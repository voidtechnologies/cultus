# High Priority:

- Maximum Structure Limit not Properly implemented.
- Permissions should be added by default.
- CivManageMenu has no way to pop-up other than by debug command.

# TODO:

- Re-design the Structure & StructureType dynamic (E.G. separate Structure from its schematic)
    - Make Structure.java an Object that represent a built structure in the physical world that can be interacted with
	- Make another file that is a static file that can be used for creating/loading/manipulating/pasting/destroying/checking specific Structures in the world (schematic)
	- Fully integrate the ConstructionSite class into Cultus.
	
- ~~Permission System, Permission management commands for Nations.~~ **(Semi-done, debating whether or not this should be an entire separate modular system)**
- Permission System, Permission management commands for Nations.
- Nation player management (kick, invite, title, etc.) (Should be finished, but clean up / double check that it is)
- ~~Nation player management (kick, invite, title, etc.)~~ **(Basically done, you can kick,invite, and set the permissions of players within a Nation)**
- ~~Remove Camp Structure when the Camp is upgraded into a Nation~~ **(The camp is removed from the flat file and memory so, no benefits may be gained from it other than destroying it for raw materials)**

- Create more Custom Events around Cultus specific situations (EX: Structure Damaged, Technology Researched, Civilization Created... etc)


- Add hooks/abilities/features of War into existing classes (I.E. add a damage value to Structures, so that they can be damaged, add control points to Nation capitol building, etc)
- War System
	- Allow Civilizations to declare hostility / alliance to other Civs.
	- Create the interactions that occur between hostile Civs (able to attack each other in their own land)
	- Create a system that allows players within Civs to attack / destroy structures during alotted times.

- Expand recipe / item config. creation abilities (description, colors)

- Allow Structures when being built to be rotated / previewed
- Create a progress indicator within the Civ (physical made with blocks or something) of the current technology being researched.

- Clean up all existing code

# IDEAS:
- Create system that will handle allocating resources to a civ (hammers, beakers, influence, luxury res, gold, etc) based on environment
- Make biome generate certain luxury resources / strategic resources
- Give civilizations hammers / beakers based upon the biome types within their civ borders (EX: +2 Prod. Per mountain +4. per extreme hills)
- Borders expand based on influence every X amt of ticks
- Make it so there are specific war time allowed users, probably via some forge mod, that way it ensures they cannot be running a modded client, other than ones we approve of
- Add more technologies + structures

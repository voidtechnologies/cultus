package com.voidtechnologies.cultus.generator;

import com.voidtechnologies.cultus.managers.LuxuryManager;
import com.voidtechnologies.cultus.structures.Position2d;
import java.util.Random;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Biome;
import org.bukkit.block.BlockState;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.ItemFrame;
import org.bukkit.generator.BlockPopulator;
import org.bukkit.generator.LimitedRegion;
import org.bukkit.generator.WorldInfo;
import org.bukkit.inventory.ItemStack;

/**
 *
 * @author Dylan Aug 24, 2021
 */
public class LuxuryPopulator extends BlockPopulator {

    private static final int height = 7;

    @Override
    public void populate(WorldInfo worldInfo, Random random, int x, int z, LimitedRegion region) {
        super.populate(worldInfo, random, x, z, region);
        int xc = x * 16;
        int zc = z * 16;
        Biome biome = region.getBiome(xc, 1, zc);
        String bioStr = biome.toString();
        if (bioStr.contains("OCEAN") || bioStr.contains("DESERT")) {
            return;
        }
        if (random.nextDouble() * 100.0 <= 0.8) {

            int yc = getHighestBlock(xc, zc, region, worldInfo.getMaxHeight());

            for (int i = 1; i <= height; i++) {
                region.setType(xc, yc + i, zc, Material.BEDROCK);
            }
            Location loc = region.getBlockState(xc, yc + height, zc + 1).getLocation();
            ItemFrame itemFrame = (ItemFrame) region.spawnEntity(loc, EntityType.ITEM_FRAME);
            Material mat = LuxuryManager.getRandomLuxury(biome);
            itemFrame.setItem(new ItemStack(mat));

            Position2d pos = Position2d.convertChunkToPosition(region.getBlockState(xc, yc, zc).getChunk());

            LuxuryManager.addResource(mat, pos);
        }
    }

    public int getHighestBlock(int x, int z, LimitedRegion region, int high) {
        for (int y = high - 1; y >= 0; y--) {
            BlockState bs = region.getBlockState(x, y, z);
            Material mat = bs.getType();
            if (mat.isSolid()) {
                return y;
            }
        }
        return 200;
    }
}

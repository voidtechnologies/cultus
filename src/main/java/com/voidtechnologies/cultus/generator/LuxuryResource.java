
package com.voidtechnologies.cultus.generator;

import com.voidtechnologies.cultus.structures.Position2d;
import org.bukkit.Material;

/**
 *
 * @author Dylan
 * Sep 2, 2021
 */
public class LuxuryResource {
    
    private Material mat;
    private Position2d location;
    
    
    public LuxuryResource(Material mat, Position2d loc) {
        this.mat = mat;
        this.location = loc;
    }

    public Material getMaterial() {
        return mat;
    }

    public Position2d getLocation() {
        return location;
    }
}

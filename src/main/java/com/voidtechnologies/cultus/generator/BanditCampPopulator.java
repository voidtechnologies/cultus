package com.voidtechnologies.cultus.generator;

import java.util.Random;
import org.bukkit.generator.BlockPopulator;
import org.bukkit.generator.LimitedRegion;
import org.bukkit.generator.WorldInfo;

/**
 *
 * @author Dylan Aug 28, 2021
 */
public class BanditCampPopulator extends BlockPopulator {

    @Override
    public void populate(WorldInfo worldInfo, Random random, int x, int z, LimitedRegion region) {
        super.populate(worldInfo, random, x, z, region);
    }
}

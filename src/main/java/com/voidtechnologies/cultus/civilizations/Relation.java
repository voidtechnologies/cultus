
package com.voidtechnologies.cultus.civilizations;

/**
 *
 * @author Dylan
 * Apr 19, 2019
 */
public enum Relation {
    NEUTRAL,
    HOSTILE,
    ALLY;
}

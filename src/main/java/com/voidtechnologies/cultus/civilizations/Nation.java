package com.voidtechnologies.cultus.civilizations;

import com.voidtechnologies.cultus.Cultus;
import com.voidtechnologies.cultus.generator.LuxuryResource;
import com.voidtechnologies.cultus.players.CPlayer;
import com.voidtechnologies.cultus.runnables.ConstructionRunnable;
import com.voidtechnologies.cultus.runnables.TechResearchRunnable;
import com.voidtechnologies.cultus.structures.Position2d;
import com.voidtechnologies.cultusschematics.Schematic;
import com.voidtechnologies.cultus.structures.Structure;
import com.voidtechnologies.cultus.structures.StructureType;
import com.voidtechnologies.cultus.structures.civil.NationStructure;
import com.voidtechnologies.cultus.structures.civil.TradePost;
import com.voidtechnologies.cultus.technology.Technologies;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.Bukkit;
import org.bukkit.Chunk;
import org.bukkit.Location;
import org.bukkit.entity.Player;

/**
 * @author Dylan Holmes
 * @date Feb 12, 2018
 */
public class Nation extends Civilization {

    public static float BASE_HAMMERS = 40;
    public static float BASE_BEAKERS = 25;

    private HashMap<ConstructionRunnable, Integer> buildTasks;
    private TechResearchRunnable currentResearch = null;
    private int currentResearchTaskId, happiness;
    private double hammers, beakers, money;
    private float influence;
    private List<Technologies> technologies;
    private NationType nationType;
    private List<UUID> members;

    public Nation(String civName, String tag, Chunk center, List<UUID> members, List<Structure> structs,
            List<Technologies> techs, List<Position2d> territory, short influence, double money, int happiness, NationType type, HashMap<String, Relation> relations) {
        super(civName, tag, CivilizationType.NATION, center, structs, territory, relations);
        this.technologies = techs;
        this.influence = influence;
        this.nationType = type;
        this.buildTasks = new HashMap<>();
        this.members = new ArrayList<>();
        for (UUID id : members) {
            this.addMember(id);
        }
        this.money = money;

        this.updateResourcesFromChunks();
    }

    /**
     * Cancels the research the Nation is currently conducting Refunds 75% of the cost, and
     * announces to the members it has been canceled.
     */
    public void cancelCurrentResearch() {
        Bukkit.getScheduler().cancelTask(currentResearchTaskId);
        this.sendMessageToMembers("Cancelled research on " + currentResearch.getTech().getName() + "!");
        this.addMoney(currentResearch.getTech().getCost() * .75);

        this.currentResearchTaskId = -1;
        this.currentResearch = null;
    }

    /**
     * Gets the influence of a nation (Currently not used in anything)
     *
     * @return amount of influence
     */
    public float getInfluence() {
        return influence;
    }

    /**
     * Sets nations influence
     *
     * @param influence
     */
    public void setInfluence(float influence) {
        this.influence = influence;
    }

    /**
     * Gets current Build Tasks
     *
     * @return map of build tasks
     */
    public HashMap<ConstructionRunnable, Integer> getBuildTasks() {
        return buildTasks;
    }

    /**
     * Gets the type of Nation this nation is
     *
     * @return type of nation
     */
    public NationType getNationType() {
        return nationType;
    }

    /**
     *
     * Structure section
     *
     */
    public int getMaxAmtOfStructures() {
        return this.getNationType().getBuildSlots();
    }

    @Override
    public boolean hasStructure(StructureType o) {
        return structures.stream().anyMatch(struct -> (struct.getType().equals(o)));
    }

    public boolean canBuildStructure(StructureType o) {
        return containsTechnology(o.getRequirement());
    }

    /**
     * Starts the building of a structure Will create a new SBR and start it, Will add the SBR to
     * the BuildTask map Create Announcement to the civilization it has started a building deduct
     * the cost of the structure from the civilization
     *
     * @param player
     * @param structure to build
     * @param loc where to build
     * @return Message to announce
     */
    public String startBuildOfStructure(CPlayer player, StructureType structure, Location loc) {
        if (!player.hasPermission("cultus.nation.building.build")) {
            return ChatColor.RED + "You do not have permission to build structures in this Civilization!";
        }

        if (!hasFunds(structure.getCost())) {
            return ChatColor.RED + "Your civilization does not have enough funds to build this structure!";
        }

        if (getMaxAmtOfStructures() <= getStructures().size() + buildTasks.keySet().size()) {
            return ChatColor.RED + "Your civilization has reached maximum amount of structures for your civilization's size!";
        }

        if (!getTechnologies().contains(structure.getRequirement())) {
            return ChatColor.RED + "Your civilization has not yet researched " + structure.getRequirement().getName() + "! Which is required to build " + structure.getName() + ".";
        }
        String res = structure.getSchematic().checkArea(loc, this, structure);
        if (!res.equals("")) {
            return res;
        }

        ConstructionRunnable sbr = new ConstructionRunnable(this, structure, loc);
        int taskId = Bukkit.getScheduler().scheduleSyncRepeatingTask(Cultus.getInstance(), sbr, 20L, StructureType.BUILD_DELAY);
        buildTasks.put(sbr, taskId);
        this.deductMoney(structure.getCost());
        return ChatColor.GREEN + "Your civilization has started to build " + structure.getName() + " for $" + structure.getCost() + "!";
    }

    /**
     * Performs the clean up of when the structure finishes
     *
     * @param sbr the finished structure task
     * @param struct the finished structure
     */
    public void finishStructure(ConstructionRunnable sbr, Structure struct) {
        structures.add(struct);
        Bukkit.getScheduler().cancelTask(buildTasks.get(sbr));
        buildTasks.remove(sbr);
        struct.onBuild(this);
    }

    public Set<ConstructionRunnable> getInProgressBuilds() {
        return buildTasks.keySet();
    }

    public int getHappiness() {
        return happiness;
    }

    /**
     * Loads a list of SBRs into the build tasks map and starts them Used when starting up the
     * server to convert SBRs from Flat-file to memory
     *
     * @param ret
     */
    public void loadCurrentBuilds(List<ConstructionRunnable> ret) {
        ret.forEach(sbr -> {
            int taskId = Bukkit.getScheduler().scheduleSyncRepeatingTask(Cultus.getInstance(), sbr, 20L, StructureType.BUILD_DELAY);
            buildTasks.put(sbr, taskId);
        });
    }

    /**
     *
     * Technology section
     *
     * @return
     */
    public List<Technologies> getTechnologies() {
        return technologies;
    }

    public boolean containsTechnology(Technologies o) {
        return technologies.contains(o);
    }

    public boolean addTechnology(Technologies e) {
        return technologies.add(e);
    }

    public boolean removeTechnology(Technologies o) {
        return technologies.remove(o);
    }

    public void clearTechnologies() {
        technologies.clear();
    }

    public int getCurrentResearchTaskId() {
        return currentResearchTaskId;
    }

    public void setCurrentResearchTaskId(int currentResearchTaskId) {
        this.currentResearchTaskId = currentResearchTaskId;
    }

    public TechResearchRunnable getCurrentResearch() {
        return currentResearch;
    }

    public void setCurrentResearch(TechResearchRunnable currentResearch) {
        this.currentResearch = currentResearch;
    }

    public void loadCurrentResearch(TechResearchRunnable research) {
        if (research == null) {
            return;
        }
        double modifier = 1.0;
        currentResearchTaskId = Bukkit.getScheduler().scheduleSyncRepeatingTask(Cultus.getInstance(), research, 20L, (long) (Technologies.RESEARCH_DELAY * modifier));
        currentResearch = research;
    }

    public boolean hasRequirementsForTech(Technologies tech) {
        for (Technologies techs : tech.getRequirements()) {
            if (!technologies.contains(techs)) {
                return false;
            }
        }
        return true;
    }

    public String startResearch(Technologies tech) {
        TechResearchRunnable trr = new TechResearchRunnable(tech, this);
        double modifier = 1.0;
        currentResearchTaskId = Bukkit.getScheduler().scheduleSyncRepeatingTask(Cultus.getInstance(), trr, 20L, (long) (Technologies.RESEARCH_DELAY * modifier));
        currentResearch = trr;
        deductMoney(tech.getCost());
        return ChatColor.AQUA + "Successfully started research on " + tech.getName() + " for $" + tech.getCost() + "!";
    }

    public void finishCurrentResearch() {
        sendMessageToMembers("Your civilization has unlocked or partially unlocked the following techs: ");

        for (Technologies tech : getCurrentResearch().getTech().getTechUnlocks()) {
            sendMessageToMembers(tech.getName());
        }
        technologies.add(getCurrentResearch().getTech());
        Bukkit.getScheduler().cancelTask(currentResearchTaskId);
        currentResearchTaskId = -1;
        setCurrentResearch(null);
    }

    public void setup(int y) {
        Location location = getCenter().toChunk().getBlock(0, y - 1, 0).getLocation();
        NationStructure nationS = new NationStructure(location);
        nationS.getType().getSchematic().clearArea(location, false);
        nationS.getType().getSchematic().paste(location);

        for (String supplement : nationS.getType().getSupplements()) {
            new Schematic(supplement).paste(location);
        }

        Bukkit.getScheduler().scheduleSyncDelayedTask(Cultus.getInstance(), () -> {
            nationS.onBuild(Nation.this);
        });

        this.addStructure(nationS);
    }

    public double getHammers() {
        return hammers * Math.max(1, (Math.log(getHappiness()) / Math.log(2)) / 2.0);
    }

    public void setHammers(double hammers) {
        this.hammers = hammers;
    }

    public double getBeakers() {
        return beakers * Math.max(1, (Math.log(getHappiness()) / Math.log(2)) / 2.0);
    }

    public void setBeakers(double beakers) {
        this.beakers = beakers;
    }

    public void updateResourcesFromChunks() {
        float hamms = 0, beaks = 0;
        for (Position2d p2d : this.getTerritories()) {
            Chunk c = p2d.toChunk();
            BiomeValue.ResourcePair rVal = BiomeValue.getResourceValues(c);

            hamms += rVal.getHammers();
            beaks += rVal.getBeakers();
        }

        setBeakers(beaks + BASE_BEAKERS);
        setHammers(hamms + BASE_HAMMERS);
    }

    @Override
    public void sendMessageToMembers(String message) {
        members.stream().map(id -> Bukkit.getPlayer(id)).filter(player -> !(player == null || !player.isOnline())).forEachOrdered(player -> {
            player.sendMessage(message);
        });
    }

    @Override
    public double getMoney() {
        return money;
    }

    @Override
    public void setMoney(double money) {
        this.money = money;
    }

    @Override
    public void addMoney(double money) {
        this.money += money;
    }

    @Override
    public void deductMoney(double money) {
        this.money -= money;
    }

    @Override
    public int getAmountOfMembers() {
        return members.size();
    }

    @Override
    public int getAmountOfOnlineMembers() {
        int ret = 0;
        for (UUID id : members) {
            Player player = Bukkit.getPlayer(id);
            if (player == null || !player.isOnline()) {
                continue;
            }
            ret++;
        }
        return ret;
    }

    @Override
    public boolean containsMember(UUID key) {
        return members.contains(key);
    }

    @Override
    public void addMember(UUID player) {
        members.add(player);
    }

    @Override
    public void removeMember(UUID key) {
        members.remove(key);
    }

    @Override
    public Set<UUID> members() {
        return new HashSet<>(members);
    }

    public void updateNationType() {
        NationType newType = NationType.fromInfluence(getInfluence());

        if (!newType.equals(getNationType())) {
            sendMessageToMembers("Your nation has developed into a " + newType.getName() + " from a " + getNationType().getName() + "!");
            setNationType(newType);
            claimLand(newType.getLandRadius());
        }
    }

    public void setNationType(NationType newType) {
        this.nationType = newType;
    }

    public void updateHappiness() {
        int numOfLr = 0;
        numOfLr = getStructures().stream().filter(struct -> !(!(struct instanceof TradePost))).map(_item -> 1).reduce(numOfLr, Integer::sum);

        this.happiness = numOfLr * 2 + 2;
    }
}

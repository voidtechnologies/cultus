package com.voidtechnologies.cultus.civilizations;

import java.util.HashMap;
import org.bukkit.Chunk;
import org.bukkit.block.Biome;

/**
 *
 * @author Dylan Aug 17, 2021
 */
public class BiomeValue {

    public static class ResourcePair {

        private float hammers, beakers;

        public ResourcePair(float hammers, float beakers) {
            this.hammers = hammers;
            this.beakers = beakers;
        }

        public float getHammers() {
            return hammers;
        }

        public float getBeakers() {
            return beakers;
        }
    }

    public static final HashMap<String, ResourcePair> BIOME_VALUES = new HashMap<String, ResourcePair>() {
        {
            put("PLAINS", new ResourcePair(2.5f, 0));
            put("SAVANNA", new ResourcePair(2.25f, 0));
            put("HILLS", new ResourcePair(6.5f, 0));
            put("PLATEAU", new ResourcePair(5.25f, 0));
            put("SNOWY", new ResourcePair(0, 2));
            put("FOREST", new ResourcePair(5, 1));
            put("JUNGLE", new ResourcePair(2.5f, 5));
            put("BAMBOO", new ResourcePair(1.5f, 7.5f));
            put("RIVER", new ResourcePair(8f, 2.5f));
            put("SWAMP", new ResourcePair(5, 5));
            put("BEACH", new ResourcePair(1.5f, 1.5f));
            put("BEACH", new ResourcePair(1.5f, 1.5f));
            put("OCEAN", new ResourcePair(.75f, 3.0f));
            put("MOUNTAIN", new ResourcePair(6.5f, 1.5f));
        }
    };

    public static ResourcePair getResourceValues(Chunk chunk) {
        float hammerValue = 0;
        float beakerValue = 0;

        Biome biome = chunk.getBlock(0, 0, 0).getBiome();
        String[] strBio = biome.toString().split("_");

        for (String str : strBio) {
            ResourcePair value = BIOME_VALUES.get(str);

            if (value == null) {
                continue;
            }

            hammerValue += value.getHammers();
            beakerValue += value.getBeakers();
        }

        return new ResourcePair(hammerValue, beakerValue);
    }
}
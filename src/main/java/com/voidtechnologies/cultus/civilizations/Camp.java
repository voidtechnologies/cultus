package com.voidtechnologies.cultus.civilizations;

import com.voidtechnologies.cultus.Cultus;
import com.voidtechnologies.cultus.managers.PlayerManager;
import com.voidtechnologies.cultus.players.CPlayer;
import com.voidtechnologies.cultus.players.Rank;
import com.voidtechnologies.cultus.players.Party;
import com.voidtechnologies.cultus.structures.Position2d;
import com.voidtechnologies.cultus.structures.Structure;
import com.voidtechnologies.cultus.structures.StructureType;
import static com.voidtechnologies.cultus.structures.StructureType.NATION_COST;
import com.voidtechnologies.cultus.structures.camp.CampStructure;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.Bukkit;
import org.bukkit.Chunk;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.Player;

/**
 * @author Dylan Holmes
 * @date Feb 8, 2018 Camp is a child of Civilization. Camp has different implementation in claiming
 * land and what it can build from a Nation
 */
public class Camp extends Civilization {

    private Party party;

    public Camp(String civName, String tag, Chunk center, Party party, List<Structure> campStructs, List<Position2d> territory, HashMap<String, Relation> relations) {
        super(civName, tag, CivilizationType.CAMP, center, campStructs, territory, relations);
        this.party = party;
    }

    /**
     * Gets an amount of chunks in a square pattern
     *
     * @param location origin
     * @param radius radius around origin to get
     * @return A list of chunks that were gotten
     */
    public static LinkedList<Chunk> getChunksInSquareArea(Location location, int radius) {
        Chunk center = location.getChunk();
        World world = center.getWorld();
        LinkedList<Chunk> chunks = new LinkedList<>();

        for (int x = -radius; x <= radius; x++) {
            for (int z = -radius; z <= radius; z++) {
                Chunk chunk = world.getChunkAt(center.getX() + x, center.getZ() + z);
                chunks.add(chunk);
            }
        }
        return chunks;
    }

    /**
     * Claims chunks within a radius and adds them to the camp's territory And builds the official
     * camp structure
     *
     * @param radius of the chunks to claim
     * @param y the plane to build the camp structure on
     */
    public void setupCamp(int radius, int y) {
        Camp.getChunksInSquareArea(getCenter().toChunk().getBlock(7, 0, 7).getLocation(), radius).forEach((chunk) -> {
            addTerritory(Position2d.convertChunkToPosition(chunk));
        });

        Location location = getCenter().toChunk().getWorld().getChunkAt(getCenter().getX() - 1, getCenter().getZ() - 1).getBlock(0, y - 3, 0).getLocation();
        CampStructure campS = new CampStructure(location);
        campS.getType().getSchematic().clearArea(location, false);
        campS.getType().getSchematic().paste(location);
        Bukkit.getScheduler().scheduleSyncDelayedTask(Cultus.getInstance(), () -> {
            campS.onBuild(Camp.this);
        });

        this.addStructure(campS);
    }

    /**
     * Unlocks an upgrade (Structure) fro the camp Camp only supports specific type of structures
     * (sifter / garden)
     *
     * @param player who is unlocking the upgrade
     * @param type the structure to unlock
     * @param loc
     */
    public void unlockUpgrade(CPlayer player, StructureType type, Location loc) {

        if (hasStructure(type)) {
            player.sendMessage(ChatColor.RED + "Your camp already has this upgrade unlocked!");
            return;
        }

        if (!hasPermission(player.getUUID(), Rank.MOD)) {
            player.sendMessage(ChatColor.RED + "You do not have permission to unlock upgrades!");
            return;
        }

        if (!hasFunds(type.getCost())) {
            player.sendMessage(ChatColor.RED + "Your camp does not have enough funds to unlock this upgrade!");
            return;
        }

        if (type.equals(StructureType.SIFTER)) {
            if (loc == null) {
                loc = this.getCampStructure().getBlocks().get(7346).toBlock().getLocation();
            }
            loc.subtract(0, 0, 1);
        } else {
            if (loc == null) {
                loc = this.getCampStructure().getBlocks().get(4833).toBlock().getLocation();
            }
            loc.subtract(0, 0, 2);
        }

        Structure struct = Structure.createStructure(loc, type.getStructureClass());
        struct.getType().getSchematic().paste(loc);

        struct.onBuild(null);
        addStructure(struct);

        deductMoney(type.getCost());

        sendMessageToMembers(ChatColor.GREEN + "Your camp has unlocked the " + type.getName() + " upgrade!");
    }

    /**
     * Gets the structure that represents the camp (NOT upgrades)
     *
     * @return Structure representing the camp
     */
    public Structure getCampStructure() {
        for (Structure struct : structures) {
            if (struct.getType().equals(StructureType.CAMP)) {
                return struct;
            }
        }
        return null;
    }

    public boolean albeToUpgradeToNation(HumanEntity whoClicked) {
        CPlayer player = PlayerManager.getPlayerByHandle((Player) whoClicked);

        if (!hasPermission(player.getUUID(), Rank.OWNER)) {
            player.sendMessage(ChatColor.RED + "You do not have permission to upgrade your camp to a nation!\nThe owner of the camp must place this block.");
            return false;
        }

        if (!hasFunds(NATION_COST)) {
            player.sendMessage(ChatColor.RED + "You do not have sufficient funds ($" + player.getParty().getMoney() + ") to upgrade your camp to a nation(" + NATION_COST + ")!");
            return false;
        }

        if (!checkClaim(Position2d.convertChunkToPosition(player.getHandle().getLocation().getChunk()))) {
            player.sendMessage(ChatColor.RED + "Your nation cannot be placed within 8 chunks of another nation!");
            return false;
        }

        return true;
    }

    @Override
    public void sendMessageToMembers(String message) {
        party.sendMessageToPartyMembers(message);
    }

    @Override
    public double getMoney() {
        return party.getMoney();
    }

    @Override
    public void setMoney(double money) {
        party.setMoney(money);
    }

    @Override
    public void addMoney(double money) {
        System.out.println("Adding money to the party");
        party.addMoney(money);
    }

    @Override
    public void deductMoney(double money) {
        party.subMoney(money);
    }

    @Override
    public int getAmountOfMembers() {
        return party.getMembers().size();
    }

    @Override
    public int getAmountOfOnlineMembers() {
        return party.getOnlineMembers().size();
    }

    /**
     * Returns the party that owns this Civilization
     *
     * @return party owner
     */
    public Party getParty() {
        return party;
    }

    public Rank getRankOfMember(UUID key) {
        return party.getRankOfMember(key);
    }

    @Override
    public boolean containsMember(UUID key) {
        return party.getRankOfMember(key) != null;
    }

    @Override
    public void addMember(UUID player) {
        party.addMember(player);
    }

    @Override
    public void removeMember(UUID key) {
        party.removeMember(key);
    }

    @Override
    public Set<UUID> members() {
        return party.getMembers();
    }

    public boolean hasPermission(UUID player, Rank expected) {
        return party.getRankOfMember(player).getValue() >= expected.getValue();
    }
}
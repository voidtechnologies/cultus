package com.voidtechnologies.cultus.civilizations;

/**
 *
 * @author dylan
 */
public enum NationType {
    
    HAMLET("Hamlet", 10, -1, 3),
    VILLAGE("Village", 18, 15, 5),
    TOWN("Town", 30, 40, 6),
    BURG("Burg", 42, 75, 7),
    CITY("City", 60, 130, 9),
    METROPOLIS("Metropolis", 60, 200, 10),
    MEGALOPOLIS("Megalopolis", 85, 300, 12);

    public static NationType fromValue(String civ) {
        return valueOf(civ.toUpperCase());
    }

    private int allowedStructures, landRadius;
    private float minimumInfluence;
    private String name;

    private NationType(String name, int allowedStructures, int minimumInfluence, int landRadius) {
        this.allowedStructures = allowedStructures;
        this.minimumInfluence = minimumInfluence;
        this.landRadius = landRadius;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public int getBuildSlots() {
        return allowedStructures;
    }
    
    public float getMinimumInfluence() {
        return minimumInfluence;
    }
    
    public int getLandRadius() {
        return landRadius;
    }
    
    public static NationType fromInfluence(float influence) {
        for (int i = 0; i < values().length; i++) {
            NationType type = values()[i];
            
            if (type.getMinimumInfluence() > influence) {
                return values()[i-1];
            }
        }
        return NationType.MEGALOPOLIS;
    }

    public static NationType fromName(String name) {
        for (NationType type : values()) {
            if (type.getName().equalsIgnoreCase(name)) {
                return type;
            }
        }
        return null;
    }
}
package com.voidtechnologies.cultus.civilizations;

/**
 *
 * @author dylan
 */
public enum CivilizationType {
    CAMP,
    NATION;

    public static CivilizationType fromValue(String civ) {
        return valueOf(civ.toUpperCase());
    }

    public CivilizationType fromName(String name) {
        for (CivilizationType type : values()) {
            if (type.toString().equalsIgnoreCase(name)) {
                return type;
            }
        }
        return null;
    }
}
package com.voidtechnologies.cultus.civilizations;

import com.voidtechnologies.cultus.managers.CivilizationManager;
import com.voidtechnologies.cultus.structures.Position2d;
import com.voidtechnologies.cultus.structures.Structure;
import com.voidtechnologies.cultus.structures.StructureType;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import org.bukkit.Chunk;
import org.bukkit.Location;
import org.bukkit.World;

/**
 * @author Dylan Holmes
 * @date Jan 3, 2018 Abstract class that represents all types of Civilizations Contains basic
 * declaration, methods, and bodies that all Civilizations will share.
 */
public abstract class Civilization {

    protected Position2d center;
    protected CivilizationType civType;
    protected String civName, tag;
    protected List<Structure> structures;
    protected List<Position2d> territory;
    protected HashMap<String, Relation> relations;

    public Civilization(String civName, String tag, CivilizationType civType, Chunk center, List<Structure> structs, List<Position2d> territory, HashMap<String, Relation> relations) {
        this.center = Position2d.convertChunkToPosition(center);
        this.structures = structs;
        this.civType = civType;
        this.relations = relations;
        this.tag = tag;
        this.territory = territory;
        this.civName = civName;
        loadStructures(structs);
    }

    /**
     * Adds a structure to the structure list
     *
     * @param e structure
     * @return t/f if succeeded or failed
     */
    public boolean addStructure(Structure e) {
        return structures.add(e);
    }

    /**
     * Checks whether the structure list contains the type of structure
     *
     * @param st type of structure
     * @return true or false if it found or not
     */
    public boolean hasStructure(StructureType st) {
        for (Structure structure : structures) {
            if (structure.getType().equals(st)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Removes a structure from the structure list
     *
     * @param o structure to be removed
     * @return
     */
    public boolean removeStructure(Structure o) {
        return structures.remove(o);
    }

    /**
     * Removes all structures from the structures list
     */
    public void clearStructures() {
        structures.clear();
    }

    /**
     * Retrieves all structures
     *
     * @return structures
     */
    public List<Structure> getStructures() {
        return structures;
    }

    /**
     * Sends a message to all online party members of the civilization
     *
     * @param message
     */
    public abstract void sendMessageToMembers(String message);

    /**
     * Gets the amount of money the civilization has
     *
     * @return Double
     */
    public abstract double getMoney();

    /**
     * Sets the amount of money the civilization has
     *
     * @param money
     */
    public abstract void setMoney(double money);

    /**
     * Adds an amount of money to the civilization
     *
     * @param money
     */
    public abstract void addMoney(double money);

    /**
     * Deducts an amount of money from the civilization
     *
     * @param money
     */
    public abstract void deductMoney(double money);

    /**
     * Sets the civilization's tag. Used in messages they type in chat, and as a prefix before
     * notifications
     *
     * @param tag
     */
    public void setTag(String tag) {
        this.tag = tag;
    }

    /**
     * Gets the civilization's tag
     *
     * @return Tag
     */
    public String getTag() {
        return tag;
    }

    /**
     * Gets the Civilization's center, represented by a Position2d
     *
     * @return
     */
    public Position2d getCenter() {
        return center;
    }

    /**
     * Gets the type of civilization this is. TYPES: - CAMP - NATION
     *
     * @return the type of civ
     */
    public CivilizationType getCivType() {
        return civType;
    }

    /**
     * Gets the civilizations Name
     *
     * @return name
     */
    public String getCivName() {
        return civName;
    }

    /**
     * Sets the civilization's type
     *
     * @param civType
     */
    public void setCivType(CivilizationType civType) {
        this.civType = civType;
    }

    /**
     * Sets the civilization's name
     *
     * @param civName
     */
    public void setCivName(String civName) {
        this.civName = civName;
    }

    /**
     *
     * Territory section
     *
     */
    public boolean containsTerritory(Position2d o) {
        return territory.contains(o);
    }

    public boolean addTerritory(Position2d e) {
        return territory.add(e);
    }

    public boolean removeTerritory(Position2d o) {
        return territory.remove(o);
    }

    public List<Position2d> getTerritories() {
        return territory;
    }

    /**
     *
     * Member section
     *
     */
    public abstract int getAmountOfMembers();

    public abstract int getAmountOfOnlineMembers();

    public abstract boolean containsMember(UUID key);

    public abstract void addMember(UUID player);

    public abstract void removeMember(UUID key);

    public abstract Set<UUID> members();

    public boolean hasFunds(double cost) {
        return getMoney() >= cost;
    }

    /**
     * Generic method for claiming land Adds chunks to the civilization within the @radius
     *
     * @param radius
     */
    public void claimLand(int radius) {
        Civilization.getChunksInArea(getCenter().toChunk().getBlock(7, 0, 7).getLocation(), radius).forEach((Chunk chunk) -> {
            Position2d p2d = Position2d.convertChunkToPosition(chunk);

            Civilization owner = CivilizationManager.getLocationOwner(p2d);
            if (owner == null) {
                addTerritory(p2d);
            }
        });
    }

    /**
     * Static Method Area
     */
    /**
     * Gets a list of chunks within a @radius of a @location
     *
     * @param location
     * @param radius
     * @return a list of chunks
     */
    public static LinkedList<Chunk> getChunksInArea(Location location, int radius) {
        Chunk center = location.getChunk();
        location = center.getBlock(7, 0, 7).getLocation();
        World world = center.getWorld();
        LinkedList<Chunk> chunks = new LinkedList<>();
        for (int x = -radius; x <= radius; x++) {
            for (int z = -radius; z <= radius; z++) {
                Chunk chunk = world.getChunkAt(center.getX() + x, center.getZ() + z);
                Location newLocation = chunk.getBlock(7, 0, 7).getLocation();
                if (location.distance(newLocation) <= radius * 16) {
                    chunks.add(chunk);
                }
            }
        }
        return chunks;
    }

    /**
     * Checks if a claim did occur at the @p2d, would it be valid based on distance to other Nations
     *
     * @param p2d
     * @return
     */
    public static boolean checkClaim(Position2d p2d) {
        for (Civilization civ : CivilizationManager.nations()) {
            if (civ.getCenter().distance(p2d) <= 8) {
                return false;
            }
        }
        return true;
    }

    /**
     * Loads a specific structure of a civilization
     *
     * @param structs list of structures to load
     */
    private void loadStructures(List<Structure> structs) {
        for (Structure struct : structs) {
            struct.onLoad(this);
        }
    }

    public Relation getRelationship(String o) {
        if (o == null) {
            return Relation.NEUTRAL;
        }
        return relations.get(o.toLowerCase());
    }

    public Relation getRelationship(Civilization o) {
        if (o == null) {
            return Relation.NEUTRAL;
        }
        return relations.get(o.getCivName().toLowerCase());
    }

    public Relation setRelationship(String k, Relation v) {
        return relations.put(k.toLowerCase(), v);
    }

    public Relation setRelationship(Civilization k, Relation v) {
        return relations.put(k.getCivName().toLowerCase(), v);
    }

    public Relation removeRelationship(String o) {
        return relations.remove(o);
    }

    public Relation removeRelationship(Civilization o) {
        return relations.remove(o.getCivName().toLowerCase());
    }
}

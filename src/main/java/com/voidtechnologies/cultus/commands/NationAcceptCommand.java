package com.voidtechnologies.cultus.commands;

import com.voidtechnologies.cultus.civilizations.Nation;
import com.voidtechnologies.cultus.managers.CivilizationManager;
import com.voidtechnologies.cultus.managers.PlayerManager;
import com.voidtechnologies.cultus.players.CPlayer;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 *
 * @author Dylan Apr 19, 2019
 */
public class NationAcceptCommand extends CultusCommand {

    public NationAcceptCommand() {
        super("nationjoin");
    }

    @Override
    public boolean onCommand(CommandSender cs, Command cmnd, String string, String[] strings) {
        if (!(cs instanceof Player)) {
            return false;
        }
        String ret = processNationAccept(strings[0], PlayerManager.getPlayerByHandle(((Player) cs)));
        cs.sendMessage(ret);
        return true;
    }

    private String processNationAccept(String name, CPlayer player) {
        if (player.getCiv() != null) {
            return ChatColor.RED + "You are already in a Nation! Leave this one first before joining another one!";
        }

        Nation invite = (Nation) CivilizationManager.getInvite(player.getUUID(), name);

        if (invite == null) {
            return ChatColor.RED + "You have not been invited to a nation by that name!";
        }

        invite.addMember(player.getUUID());
        invite.sendMessageToMembers(player.getName() + " has joined the Nation!");
        CivilizationManager.removeInvite(player.getUUID());
        player.setCiv(invite);
        return ChatColor.GREEN + "Successfully joined the Nation!";
    }

}

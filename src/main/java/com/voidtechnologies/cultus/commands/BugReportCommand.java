package com.voidtechnologies.cultus.commands;

import com.voidtechnologies.cultus.Cultus;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

/**
 * @author Dylan Holmes
 * @date Jan 29, 2018
 */
public class BugReportCommand extends CultusCommand {

    private final String BUG_REPORT_FILE = "bug_reports.yml";

    public BugReportCommand() {
        super("bug");
    }

    @Override
    public boolean onCommand(CommandSender cs, Command cmnd, String string, String[] strings) {
        try {
            if (strings.length < 2) {
                cs.sendMessage(ChatColor.RED + "/bug <SEVERITY:[MINOR,LOW,MEDIUM,HIGH,SEVERE]> <description>");
                return true;
            }
            
            String description = String.join(" ", Arrays.copyOfRange(strings, 1, strings.length));
            
            File file = new File(Cultus.getInstance().getDataFolder().getAbsolutePath() + "/" + BUG_REPORT_FILE);
            boolean newFile = false;
            if (!file.exists()) {
                newFile = true;
                try {
                    file.createNewFile();
                } catch (IOException ex) {
                    Logger.getLogger(BugReportCommand.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            if (newFile) {
                Files.write(Paths.get(Cultus.getInstance().getDataFolder().getAbsolutePath() + "\\" + BUG_REPORT_FILE), "description,severity\n".getBytes(), StandardOpenOption.APPEND);
            }
            Files.write(Paths.get(Cultus.getInstance().getDataFolder().getAbsolutePath() + "\\" + BUG_REPORT_FILE), (description.replace(",", ".") + "," + strings[0] + "\n").getBytes(), StandardOpenOption.APPEND);
            return true;
        } catch (IOException ex) {
            Logger.getLogger(BugReportCommand.class.getName()).log(Level.SEVERE, null, ex);
        }
        return true;
    }
}

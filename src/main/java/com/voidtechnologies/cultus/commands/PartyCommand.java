package com.voidtechnologies.cultus.commands;

import com.voidtechnologies.cultus.managers.PartyManager;
import com.voidtechnologies.cultus.managers.PlayerManager;
import com.voidtechnologies.cultus.players.CPlayer;
import com.voidtechnologies.cultus.players.Rank;
import com.voidtechnologies.cultus.players.Party;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * @author Dylan Holmes
 * @date Jan 28, 2018
 */
public class PartyCommand extends CultusCommand {

    public PartyCommand() {
        super("party");
    }

    @Override
    public boolean onCommand(CommandSender cs, Command cmnd, String string, String[] strings) {
        int options = strings.length;

        if (!(cs instanceof Player)) {
            cs.sendMessage(ChatColor.RED + "This command must be executed by a player in-game.");
            return true;
        }

        if (options < 1) {
            cs.sendMessage(ChatColor.RED + "Party Command Usages:");
            cs.sendMessage(ChatColor.YELLOW + "/party create <party_name>");
            cs.sendMessage(ChatColor.YELLOW + "/party invite <player_name>");
            cs.sendMessage(ChatColor.YELLOW + "/party kick <player_name>");
            cs.sendMessage(ChatColor.YELLOW + "/party rank <player_name> <rank_name>");
            cs.sendMessage(ChatColor.YELLOW + "/party message <message>");
            cs.sendMessage(ChatColor.YELLOW + "/party balance");
            cs.sendMessage(ChatColor.YELLOW + "/party deposit");
            cs.sendMessage(ChatColor.YELLOW + "/party withdraw");
            return true;
        }

        Player player = (Player) cs;
        String result = "";
        switch (string.toLowerCase()) {
            case "p":
            case "party":
                result = processParty(strings, options, player);
                break;
            case "pm":
            case "partychat":
                result = processPartyChat(strings, player);
                break;
            case "pc":
            case "partycreate":
                result = processPartyCreate(strings[0], player);
                break;
            case "pi":
            case "partyinvite":
                result = processPartyInvite(strings[0], player);
                break;
            case "pk":
            case "partykick":
                result = processPartyKick(strings[0], player);
                break;
            case "pr":
            case "partyrank":
                if (options > 1) {
                    result = processRank(strings[0], strings[1], player);
                } else {
                    result = ChatColor.RED + "You must enter a rank to set the member to (MEMBER, MOD, OWNER)!";
                }
                break;
            case "pa":
            case "partyaccept":
                result = processPartyAccept(strings[0], player);
                break;
            case "pw":
            case "partywithdraw":
                result = processPartyWithdraw(strings[0], player);
                break;
            case "pd":
            case "partydeposit":
                result = processPartyDeposit(strings[0], player);
                break;
            case "pb":
            case "partybalance":
                result = processPartyBalancee(player);
                break;
        }
        if (result != null) {
            player.sendMessage(result);
        }
        return true;
    }

    private String processParty(String[] args, int amount, Player player) {

        switch (args[0]) {
            case "m":
            case "message":
            case "chat":
                return this.processPartyChat(args, player);
            case "k":
            case "kick":
                if (amount < 2) {
                    return ChatColor.RED + "You need to specify a player to kick!";
                }
                return this.processPartyKick(args[1], player);
            case "r":
            case "rank":
                if (amount < 3) {
                    return ChatColor.RED + "You must enter a rank to set the member to (MEMBER, MOD, OWNER)!";
                }
                return this.processRank(args[1], args[2], player);
            case "c":
            case "create":
                if (amount < 2) {
                    return ChatColor.RED + "You need to specify a party name!";
                }
                return this.processPartyCreate(args[1], player);
            case "i":
            case "invite":
                if (amount < 2) {
                    return ChatColor.RED + "You need to specify a player to invite!";
                }
                return this.processPartyInvite(args[1], player);
            case "a":
            case "accept":
                if (amount < 2) {
                    return ChatColor.RED + "You need to specify a party to join!";
                }
                return this.processPartyAccept(args[1], player);
            case "deposit":
                if (amount < 1) {
                    return ChatColor.RED + "You need to specify an amount!";
                }
                return this.processPartyDeposit(args[1], player);
            case "withdraw":
                if (amount < 1) {
                    return ChatColor.RED + "You need to specify an amount!";
                }
                return this.processPartyWithdraw(args[1], player);
            case "balance":
                return this.processPartyBalancee(player);
            default:
                return ChatColor.RED + "Unrecognized option! Options are: create, invite, accept, kick, rank!";
        }
    }

    private String processPartyCreate(String name, Player executor) {
        if (!name.matches("^[a-zA-Z]+$")) {
            return ChatColor.RED + "A party name must only contain letters (no spaces, special chars, or symbols)!";
        }
        CPlayer player = PlayerManager.getPlayerByHandle(executor);

        if (PartyManager.getPartyFromPlayer(player) != null) {
            return ChatColor.RED + "You are already in a party, you must leave this one first!";
        }

        if (PartyManager.containsPartyName(name)) {
            return ChatColor.RED + "A party already exists with this name!";
        }

        Party party = new Party(name);
        party.setMember(player.getUUID(), Rank.OWNER);
        PartyManager.addParty(party);
        player.setParty(party);

        return ChatColor.GREEN + "You have successfully created the party " + ChatColor.BLUE + name + ChatColor.GREEN + ".";
    }

    private String processPartyInvite(String name, Player executor) {
        CPlayer cExecutor = PlayerManager.getPlayerByHandle(executor);
        Party party = PartyManager.getPartyFromPlayer(cExecutor);

        if (party == null) {
            return ChatColor.RED + "You are not in a party!";
        }

        if (party.getRankOfMember(cExecutor.getUUID()).getValue() < 1) {
            return ChatColor.RED + "You do not have permission to invite members to this party!";
        }

        Player player = Bukkit.getPlayer(name);

        if (player == null) {
            return ChatColor.RED + "This player is not online! A player must be online in order to be invited to a party.";
        }

        CPlayer cPlayer = PlayerManager.getPlayerByUUID(player.getUniqueId());

        if (PartyManager.getPartyFromPlayer(cPlayer) != null) {
            return ChatColor.RED + "That player is already apart of another party!";
        }

        String pName = party.getName();

        cPlayer.sendMessage(ChatColor.BLUE + "You have been invited to the " + ChatColor.YELLOW + ChatColor.BLUE + " party!");
        cPlayer.sendMessage(ChatColor.BLUE + "Type /pa " + pName + ", /partyaccept " + pName + ", or /party accept " + pName + " to accept!");

        PartyManager.addInvite(player.getUniqueId(), party.getName());

        return ChatColor.GREEN + "Successfully invited player to party!";
    }

    private String processPartyAccept(String name, Player executor) {
        CPlayer player = PlayerManager.getPlayerByHandle(executor);

        if (PartyManager.getPartyFromPlayer(player) != null) {
            return ChatColor.RED + "You are already in a party! Leave this one first before joining another one!";
        }

        Party invite = PartyManager.getInvite(player.getUUID(), name);

        if (invite == null) {
            return ChatColor.RED + "You have not been invited to a party by that name!";
        }

        invite.addMember(player.getUUID());
        invite.sendMessageToPartyMembers(player.getName() + " has joined the party!");
        PartyManager.removeInvite(player.getUUID());
        player.setParty(invite);
        return ChatColor.GREEN + "Successfully joined the party!";
    }

    private String processPartyKick(String name, Player executor) {
        CPlayer cExecutor = PlayerManager.getPlayerByHandle(executor);
        Party party = PartyManager.getPartyFromPlayer(cExecutor);

        if (party == null) {
            return ChatColor.RED + "You are not in a party!";
        }
        Integer execRank = party.getRankOfMember(cExecutor.getUUID()).getValue();
        if (execRank < 1) {
            return ChatColor.RED + "You do not have permission to kick members from this party!";
        }
        CPlayer cPlayer = PlayerManager.getPlayerByUUID(Bukkit.getOfflinePlayer(name).getUniqueId());

        if (!party.isPlayerInParty(cPlayer)) {
            return ChatColor.RED + "That player is not in your party!";
        }

        if (party.getRankOfMember(cPlayer.getUUID()).getValue() >= execRank) {
            return ChatColor.RED + "You cannot kick someone with a greater than or equal rank to yourself!";
        }

        cPlayer.sendMessage(ChatColor.RED + "You have been kicked from the party " + party.getName() + "!");
        party.removeMember(cPlayer.getUUID());
        party.sendMessageToPartyMembers("Player " + cPlayer.getName() + " has been kicked from the party!");
        cPlayer.setParty(null);
        return ChatColor.GREEN + "Successfully kicked player from party!";
    }

    private String processRank(String name, String rank, Player executor) {
        CPlayer cExecutor = PlayerManager.getPlayerByHandle(executor);
        Party party = PartyManager.getPartyFromPlayer(cExecutor);
        if (party == null) {
            return ChatColor.RED + "You are not in a party!";
        }
        Integer execRank = party.getRankOfMember(cExecutor.getUUID()).getValue();
        if (execRank.shortValue() < 2) {
            System.out.println("No perm: " + execRank);
            return ChatColor.RED + "You do not have permission to change ranks of players!";
        }
        CPlayer cPlayer = PlayerManager.getPlayerByUUID(Bukkit.getOfflinePlayer(name).getUniqueId());

        if (!party.isPlayerInParty(cPlayer)) {
            return ChatColor.RED + "That player is not in your party!";
        }

        Rank pRank = Rank.valueOf(rank.toUpperCase());

        if (pRank == null) {
            return ChatColor.RED + "You must enter a valid rank (MEMBER, MOD, or OWNER)!";
        }

        if (party.getRankOfMember(cPlayer.getUUID()).getValue() == execRank) {
            return ChatColor.RED + "This command cannot target people of the same rank!";
        }
        party.setMember(cPlayer.getUUID(), pRank);
        party.sendMessageToPartyMembers(cPlayer.getName() + " has been set to " + pRank.toString() + "!");
        return ChatColor.GREEN + "Successfully set the rank of the player!";
    }

    private String processPartyChat(String[] strings, Player player) {
        CPlayer exec = PlayerManager.getPlayerByHandle(player);
        String msg = String.join(" ", strings);
        Party party = PartyManager.getPartyFromPlayer(exec);

        if (party == null) {
            return ChatColor.RED + "You are not in a party!";
        }

        party.sendMessageToPartyMembers(ChatColor.BLUE + player.getName() + ": " + ChatColor.YELLOW + msg);
        return null;
    }

    private String processPartyWithdraw(String amount, Player player) {
        CPlayer cExecutor = PlayerManager.getPlayerByHandle(player);
        Party party = PartyManager.getPartyFromPlayer(cExecutor);

        if (party == null) {
            return ChatColor.RED + "You are not in a party!";
        }
        Integer execRank = party.getRankOfMember(cExecutor.getUUID()).getValue();
        if (execRank < 1) {
            return ChatColor.RED + "You do not have permission to withdraw money from this party!";
        }
        Double dAmount = Double.valueOf(amount);

        if (dAmount == null || dAmount == 0) {
            return ChatColor.RED + "Must enter a valid amount to withdraw!";
        }

        if (party.getMoney() < dAmount) {
            return ChatColor.RED + "Your party does not have enough money to withdraw that much!";
        }

        party.subMoney(dAmount);
        cExecutor.addMoney(dAmount);
        return ChatColor.GREEN + "Successfully withdrew $" + dAmount + " from the party!";
    }

    private String processPartyDeposit(String amount, Player player) {
        CPlayer cExecutor = PlayerManager.getPlayerByHandle(player);
        Party party = PartyManager.getPartyFromPlayer(cExecutor);

        if (party == null) {
            return ChatColor.RED + "You are not in a party!";
        }
        Double dAmount = Double.valueOf(amount);

        if (dAmount == null || dAmount == 0) {
            return ChatColor.RED + "Must enter a valid amount to withdraw!";
        }

        if (cExecutor.getMoney() < dAmount) {
            return ChatColor.RED + "You do not have enough money to deposit that much!";
        }

        party.addMoney(dAmount);
        cExecutor.deductMoney(dAmount);
        return ChatColor.GREEN + "Successfully deposited $" + dAmount + " to the party!";
    }

    private String processPartyBalancee(Player player) {
        CPlayer cExecutor = PlayerManager.getPlayerByHandle(player);
        Party party = PartyManager.getPartyFromPlayer(cExecutor);

        if (party == null) {
            return ChatColor.RED + "You are not in a party!";
        }

        return ChatColor.GREEN + "Your party currently has $" + party.getMoney() + "!";
    }
}

package com.voidtechnologies.cultus.commands;

import com.voidtechnologies.cultus.Cultus;
import java.util.HashMap;
import java.util.Map.Entry;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.PluginCommand;

/**
 * @author Dylan Holmes
 * @date Jan 17, 2018
 */
public class CultusCommand implements CommandExecutor {

    private static HashMap<String, CultusCommand> commands = new HashMap<>();

    public CultusCommand(String name) {
        registerCommand(name);
    }

    @Override
    public boolean onCommand(CommandSender cs, Command cmnd, String string, String[] strings) {
        return true;
    }

    public static void registerAll(HashMap<String, CultusCommand> comms) {
        commands.putAll(comms);
    }

    protected void registerCommand(String name) {
        commands.put(name, this);
    }

    public static void registerAllCommands() {
        for (Entry<String, CultusCommand> entry : commands.entrySet()) {
            String cmdName = entry.getKey();
            System.out.println(cmdName);
            CultusCommand cmdExec = entry.getValue();
            PluginCommand command = Cultus.getInstance().getCommand(cmdName);
            System.out.println(command.getName());

            command.setExecutor(cmdExec);
        }
    }
}

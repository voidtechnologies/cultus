package com.voidtechnologies.cultus.commands;

import com.voidtechnologies.cultus.managers.PartyManager;
import com.voidtechnologies.cultus.managers.PlayerManager;
import com.voidtechnologies.cultus.players.CPlayer;
import com.voidtechnologies.cultus.players.Party;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * @author Dylan Holmes
 * @date Feb 5, 2018
 */
public class PlayerMoneyCommand extends CultusCommand {

    public PlayerMoneyCommand() {
        super("money");
    }

    @Override
    public boolean onCommand(CommandSender cs, Command cmnd, String string, String[] strings) {
        int options = strings.length;

        if (!(cs instanceof Player)) {
            cs.sendMessage(ChatColor.RED + "This command must be executed by a player in-game.");
            return true;
        }
        Player player = (Player) cs;

        if (string.toLowerCase().equals("money") && options < 1) {
            player.sendMessage(ChatColor.RED + "Money Commands:");
            player.sendMessage(ChatColor.YELLOW + "/balance");
            player.sendMessage(ChatColor.YELLOW + "/send <player_name> <amount>");
            return true;
        }

        String result = "";

        switch (string.toLowerCase()) {
            case "b":
            case "balance":
                result = processBalance(player);
                break;
            case "s":
            case "send":
                result = processPlayerSend(strings, player);
                break;
        }
        if (result != null) {
            player.sendMessage(result);
        }
        return true;
    }

    private String processPlayerSend(String[] args, Player player) {
        CPlayer cExecutor = PlayerManager.getPlayerByHandle(player);

        if (args.length < 2) {
            cExecutor.sendMessage(ChatColor.RED + "/money send <player_name> <amount>");
            return ChatColor.RED + "You must enter a valid player name, and amount";
        }

        Player oPlayer = Bukkit.getPlayer(args[0]);

        if (oPlayer == null) {
            return ChatColor.RED + "Player must be online to receive money!";
        }

        Double dAmount = Double.valueOf(args[1]);

        if (dAmount == null || dAmount == 0) {
            return ChatColor.RED + "Must enter a valid amount to withdraw!";
        }

        if (cExecutor.getMoney() < dAmount) {
            return ChatColor.RED + "You do not have enough money to send that much!";
        }

        cExecutor.deductMoney(dAmount);
        CPlayer cOPlayer = PlayerManager.getPlayerByHandle(oPlayer);
        cOPlayer.addMoney(dAmount);
        cOPlayer.sendMessage(ChatColor.GREEN + "You have received " + ChatColor.YELLOW
                + "$" + dAmount + ChatColor.GREEN + " from "
                + ChatColor.BLUE + cExecutor.getName() + ChatColor.GREEN + "!");
        return ChatColor.GREEN + "Successfully sent $" + dAmount + " to " + oPlayer.getDisplayName() + "!";
    }

    private String processBalance(Player player) {
        CPlayer cExecutor = PlayerManager.getPlayerByHandle(player);
        return ChatColor.GREEN + "You have $" + cExecutor.getMoney() + "!";
    }
}

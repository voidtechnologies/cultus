package com.voidtechnologies.cultus.commands.ui;

import com.voidtechnologies.cultus.commands.ui.buttons.GenericCloseMenuButton;
import com.voidtechnologies.cultus.managers.MenuManager;
import java.util.HashMap;
import org.bukkit.Bukkit;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;

/**
 * @author Dylan Holmes
 * @date Jan 18, 2018
 */
public abstract class Menu {

    private String title;
    private MenuSize size;
    private Menu sender;
    private HashMap<Integer, MenuItem> buttons = new HashMap<>();

    public Menu(String title, MenuSize size, Menu sender) {
        this.title = title;
        this.size = size;
        this.sender = sender;
    }

    public Inventory createInventory() {
        generateButtons();
        Inventory inventory = Bukkit.createInventory(null, size.toInt() * 9, title);
        
        buttons.put(inventory.getSize() - 1, new GenericCloseMenuButton());
        buttons.entrySet().forEach((entry) -> {
            inventory.setItem(entry.getKey(), entry.getValue().getItemStack());
        });
        
        MenuManager.registerMenu(inventory, this);
        return inventory;
    }

    public abstract void generateButtons();

    public HashMap<Integer, MenuItem> getButtons() {
        return buttons;
    }

    public Menu getSender() {
        return sender;
    }

    public void processInvClick(InventoryClickEvent context, int slot) {
        MenuItem btn = buttons.get(slot);
        if (btn == null) {
            return;
        }
        btn.processClick(this, context);
    }
}

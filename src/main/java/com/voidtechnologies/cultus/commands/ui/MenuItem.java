package com.voidtechnologies.cultus.commands.ui;

import java.util.Arrays;
import java.util.LinkedList;
import org.bukkit.Material;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

/**
 * @author Dylan Holmes
 * @date Jan 19, 2018
 */
public abstract class MenuItem {

    private int stackAmt;
    private String desc;
    private String name;
    private Material item;
    private ItemStack itemStack;

    public MenuItem(String name, String desc, Material item, int stackAmt) {
        this.name = name;
        this.item = item;
        this.stackAmt = stackAmt;
        this.desc = desc;

        createItemStack();
    }

    public MenuItem(ItemStack item) {
        this.name = item.getItemMeta().getDisplayName();
        this.itemStack = item;
        this.stackAmt = item.getAmount();
        this.item = item.getType();
    }

    private void createItemStack() {
        itemStack = new ItemStack(item, stackAmt);

        ItemMeta im = itemStack.getItemMeta();

        im.setDisplayName(name);

        String[] split = desc.split("\n");
        im.setLore(new LinkedList<>());
        im.setLore(Arrays.asList(split));

        itemStack.setItemMeta(im);
    }

    public String getName() {
        return name;
    }

    public ItemStack getItemStack() {
        return itemStack;
    }

    public abstract boolean processClick(Menu menu, InventoryClickEvent event);

}

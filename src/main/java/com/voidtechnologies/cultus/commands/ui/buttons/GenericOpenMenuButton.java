package com.voidtechnologies.cultus.commands.ui.buttons;

import com.voidtechnologies.cultus.commands.ui.Menu;
import com.voidtechnologies.cultus.commands.ui.MenuItem;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;

/**
 * @author Dylan Holmes
 * @date Jan 25, 2018
 */
public class GenericOpenMenuButton extends MenuItem {

    private Menu openMenu;

    public GenericOpenMenuButton(String name, String desc, Material item, Menu menu) {
        super(name, desc, item, 1);
        this.openMenu = menu;
    }

    @Override
    public boolean processClick(Menu menu, InventoryClickEvent event) {
        if (menu.getSender() != null) {
            event.getWhoClicked().closeInventory();
        }
        Player player = (Player) event.getWhoClicked();
        Inventory inv = openMenu.createInventory();
        player.openInventory(inv);
        return true;
    }
}

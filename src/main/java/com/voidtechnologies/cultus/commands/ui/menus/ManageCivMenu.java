package com.voidtechnologies.cultus.commands.ui.menus;

import com.voidtechnologies.cultus.civilizations.Nation;
import com.voidtechnologies.cultus.commands.ui.Menu;
import com.voidtechnologies.cultus.commands.ui.MenuSize;
import com.voidtechnologies.cultus.commands.ui.buttons.GenericOpenMenuButton;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.Material;

/**
 * @author Dylan Holmes
 * @date Jan 21, 2018
 */
public class ManageCivMenu extends Menu {

    private Nation civ;

    public ManageCivMenu(Nation civ, Menu sender) {
        super("Manage Civilization", MenuSize.SMALL, sender);
        this.civ = civ;
    }

    @Override
    public void generateButtons() {
        getButtons().put(3, new GenericOpenMenuButton("View Civ Technologies",
                "View your civilization's tech. tree.", Material.REDSTONE, new ViewTechnologyMenu(civ, this)));

        getButtons().put(2, new GenericOpenMenuButton("Structures",
                "View your civilization's\nbuildable structures. And build them.",
                Material.WOODEN_AXE, new ViewStructuresMenu(civ, this)));

        getButtons().put(1, new GenericOpenMenuButton(civ.getCivName(),
                ChatColor.RED + civ.getCivName() + "'s general information!",
                Material.COBBLESTONE_WALL, new ViewCivInfoMenu(civ, this)));
        //@TODO: getButtons().put(1, null /* Relations */);

        //getButtons().put(0, null /* Place Holder */);
    }
}

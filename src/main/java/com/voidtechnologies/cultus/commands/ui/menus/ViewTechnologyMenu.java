package com.voidtechnologies.cultus.commands.ui.menus;

import com.voidtechnologies.cultus.civilizations.Nation;
import com.voidtechnologies.cultus.commands.ui.Menu;
import com.voidtechnologies.cultus.commands.ui.MenuItem;
import com.voidtechnologies.cultus.commands.ui.MenuSize;
import com.voidtechnologies.cultus.commands.ui.buttons.CancelResearchButton;
import com.voidtechnologies.cultus.commands.ui.buttons.TechnologyButton;
import com.voidtechnologies.cultus.technology.Technologies;
import java.util.Arrays;
import java.util.HashMap;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

/**
 * @author Dylan Holmes
 * @date Jan 22, 2018
 */
public class ViewTechnologyMenu extends Menu {

    private Nation civ;

    public ViewTechnologyMenu(Nation civ, Menu sender) {
        super("Technologies", MenuSize.LARGEST, sender);
        this.civ = civ;
    }

    @Override
    public void generateButtons() {

        int i = 0;
        HashMap<Integer, MenuItem> buttons = getButtons();
        String researched = ChatColor.GREEN + "Researched!";
        for (Technologies tech : civ.getTechnologies()) {
            ItemStack stack = new ItemStack(Material.GREEN_WOOL);

            ItemMeta im = stack.getItemMeta();
            im.setDisplayName(ChatColor.AQUA + tech.getName());
            String lore = tech.getDesc() + "\n" + researched;
            im.setLore(Arrays.asList(lore.split("\n")));

            stack.setItemMeta(im);

            buttons.put(i++, new TechnologyButton(stack, civ, tech));
        }
        i--;
        researched = ChatColor.YELLOW + "Able to be researched!";
        i = ((i / 9) + 2) * 9;
        for (Technologies tech : Technologies.values()) {
            if (tech.equals(Technologies.UNKNOWN)) {
                continue;
            }

            if (!civ.hasRequirementsForTech(tech) || civ.containsTechnology(tech)) {
                continue;
            }
            ItemStack stack = new ItemStack(Material.YELLOW_WOOL);

            ItemMeta im = stack.getItemMeta();
            im.setDisplayName(ChatColor.AQUA + tech.getName());
            String lore = tech.getDesc() + "\n" + researched;
            im.setLore(Arrays.asList(lore.split("\n")));

            stack.setItemMeta(im);

            buttons.put(i++, new TechnologyButton(stack, civ, tech));
        }
        i--;
        researched = ChatColor.YELLOW + "Locked!";
        i = ((i / 9) + 2) * 9;
        for (Technologies tech : Technologies.values()) {
            if (tech.equals(Technologies.UNKNOWN)) {
                continue;
            }

            if (civ.hasRequirementsForTech(tech)) {
                continue;
            }
            ItemStack stack = new ItemStack(Material.RED_WOOL);

            ItemMeta im = stack.getItemMeta();
            im.setDisplayName(ChatColor.AQUA + tech.getName());
            String lore = tech.getDesc() + "\n" + researched;
            im.setLore(Arrays.asList(lore.split("\n")));

            stack.setItemMeta(im);

            buttons.put(i++, new TechnologyButton(stack, civ, tech));
        }

        buttons.put(++i, new CancelResearchButton(civ));
    }
}

package com.voidtechnologies.cultus.commands.ui;

/**
 * @author Dylan Holmes 
 * @date Jan 19, 2018
 */
public enum MenuSize {

    SMALLEST(1),
    SMALL(2),
    MEDIUM(3),
    LARGE(4),
    LARGEST(6);
    
    private int size;
    
    private MenuSize(int i) {
        this.size = i;
    }
    
    
    public int toInt() {
        return size;
    }
}
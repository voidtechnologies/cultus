package com.voidtechnologies.cultus.commands.ui.menus;

import com.voidtechnologies.cultus.civilizations.Nation;
import com.voidtechnologies.cultus.commands.ui.Menu;
import com.voidtechnologies.cultus.commands.ui.MenuSize;
import com.voidtechnologies.cultus.commands.ui.buttons.RemoveStructureButton;
import com.voidtechnologies.cultus.commands.ui.buttons.StructureButton;
import com.voidtechnologies.cultus.structures.StructureType;
import java.util.LinkedList;

/**
 * @author Dylan Holmes
 * @date Jan 22, 2018
 */
public class ViewStructuresMenu extends Menu {

    private Nation civ;

    public ViewStructuresMenu(Nation civ, Menu sender) {
        super("Build Structures", MenuSize.LARGE, sender);
        this.civ = civ;
    }

    @Override
    public void generateButtons() {
        LinkedList<StructureType> structures = new LinkedList<>();
        for (StructureType struct : StructureType.values()) {
            if (civ.canBuildStructure(struct)) {
                structures.add(struct);
            }
        }
        int i = 0;
        for (StructureType struct : structures) {
            getButtons().put(i++, new StructureButton(civ, struct));
        }
        getButtons().put(++i, new RemoveStructureButton());
    }
}

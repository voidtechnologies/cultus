package com.voidtechnologies.cultus.commands.ui.menus;

import com.voidtechnologies.cultus.civilizations.Camp;
import com.voidtechnologies.cultus.commands.ui.Menu;
import com.voidtechnologies.cultus.commands.ui.MenuSize;
import com.voidtechnologies.cultus.commands.ui.buttons.GenericOpenMenuButton;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.Material;

/**
 * @author Dylan Holmes
 * @date Feb 9, 2018
 */
public class ManageCampMenu extends Menu {

    private Camp camp;

    public ManageCampMenu(Camp camp, Menu sender) {
        super("Manage Camp", MenuSize.SMALLEST, sender);
        this.camp = camp;
    }

    @Override
    public void generateButtons() {
        System.out.println(camp);
        System.out.println(camp.getCivName());
        getButtons().put(1, new GenericOpenMenuButton("Camp Upgrades",
                "View your civilization's tech. tree.", Material.REDSTONE, new CampUpgradesMenu(camp, this)));
        getButtons().put(0, new GenericOpenMenuButton("Camp Information", ChatColor.RED + camp.getCivName() + "'s general information!", Material.COBBLESTONE_WALL, new ViewCampInfoMenu(camp, this)));
    }
}
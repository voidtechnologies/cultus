package com.voidtechnologies.cultus.commands.ui.buttons;

import com.voidtechnologies.cultus.civilizations.Nation;
import com.voidtechnologies.cultus.commands.ui.Menu;
import com.voidtechnologies.cultus.commands.ui.MenuItem;
import com.voidtechnologies.cultus.players.CPlayer;
import com.voidtechnologies.cultus.managers.PlayerManager;
import com.voidtechnologies.cultus.technology.Technologies;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;

/**
 * @author Dylan Holmes
 * @date Jan 22, 2018
 */
public class TechnologyButton extends MenuItem {

    private Nation civ;
    private Technologies tech;


    public TechnologyButton(ItemStack stack, Nation civ, Technologies tech) {
        super(stack);
        this.civ = civ;
        this.tech = tech;
    }

    @Override
    public boolean processClick(Menu menu, InventoryClickEvent event) {
        Player player = (Player) event.getWhoClicked();
        CPlayer cp = PlayerManager.getPlayerByHandle(player);
        String msg = "";

        for (Technologies techs : tech.getRequirements()) {
            if (!civ.getTechnologies().contains(techs)) {
                msg = ChatColor.RED + "Unable to start research of " + tech.getName() + "! You must research " + techs.getName() + " first!";
            }
        }
        if (msg.equals("")) {
            if (!civ.members().contains(cp.getUUID())) {
                msg = org.bukkit.ChatColor.RED + "You are not apart of this Civilization!";
            } else if (!cp.hasPermission("cultus.nation.research.start")) {
                msg = org.bukkit.ChatColor.RED + "You do not have permission to research technologies in this civilization!"; 
            } else if (civ.getTechnologies().contains(tech)) {
                msg = ChatColor.YELLOW + "Your civilization already has this technology researched!";
            } else if (tech.getCost() > civ.getMoney()) {
                msg = ChatColor.RED + "Your civilization does not have enough money ($" + civ.getMoney() + ") to research this technology ($" + tech.getCost() + ")!";
            } else if (civ.getCurrentResearch() != null) {
                msg = ChatColor.RED + "Your civilization is still researching another technology!";
            } else {
                msg = civ.startResearch(tech);
            }
        }
        player.closeInventory();
        player.sendMessage(msg);
        return true;
    }
}
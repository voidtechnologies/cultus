package com.voidtechnologies.cultus.commands.ui.menus;

import com.voidtechnologies.cultus.civilizations.Nation;
import com.voidtechnologies.cultus.commands.ui.Menu;
import com.voidtechnologies.cultus.commands.ui.MenuItem;
import com.voidtechnologies.cultus.commands.ui.MenuSize;
import com.voidtechnologies.cultus.commands.ui.buttons.GenericDisplayButton;
import java.util.HashMap;
import java.util.LinkedList;
import org.bukkit.ChatColor;
import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.PotionMeta;

/**
 * @author Dylan Holmes
 * @date Jan 25, 2018
 */
public class ViewCivInfoMenu extends Menu {

    private Nation civ;

    public ViewCivInfoMenu(Nation civ, Menu sender) {
        super("Civilization Info", MenuSize.SMALL, sender);
        this.civ = civ;
    }

    @Override
    public void generateButtons() {
        HashMap<Integer, MenuItem> btns = getButtons();
        btns.put(0, new GenericDisplayButton(civ.getNationType().getName(), "Your civilization is a " + civ.getCivType().name(), Material.COMPASS, 1));
        btns.put(1, new GenericDisplayButton("Members", ChatColor.DARK_PURPLE + "There are " + civ.getAmountOfOnlineMembers() + "/" + civ.getAmountOfMembers() + " online!", Material.PLAYER_HEAD, civ.getAmountOfMembers()));
        btns.put(2, new GenericDisplayButton("Production", "" + ChatColor.GOLD + civ.getHammers() + " hammers!", Material.ANVIL, 1));

        ItemStack sciencePot = new ItemStack(Material.POTION, 1);
        PotionMeta pm = (PotionMeta) sciencePot.getItemMeta();
        LinkedList<String> lore = new LinkedList<>();

        lore.add("" + ChatColor.AQUA + civ.getBeakers() + " beakers!");

        pm.setLore(lore);
        pm.setDisplayName("Science");
        pm.setColor(Color.TEAL);

        sciencePot.setItemMeta(pm);

        btns.put(3, new GenericDisplayButton(sciencePot));
        btns.put(4, new GenericDisplayButton("Money", ChatColor.YELLOW + "$" + civ.getMoney(), Material.GOLD_INGOT, 1));
        btns.put(5, new GenericDisplayButton("Influence", "" + ChatColor.LIGHT_PURPLE + civ.getInfluence() + " influence!", Material.GLOWSTONE, 1));
        btns.put(6, new GenericDisplayButton("Structures", "" + ChatColor.RED + civ.getStructures().size() + "/" + civ.getMaxAmtOfStructures() + " structure slots used!", Material.ACACIA_DOOR, 1));

    }
}

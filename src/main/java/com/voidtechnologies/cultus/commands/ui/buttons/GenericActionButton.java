package com.voidtechnologies.cultus.commands.ui.buttons;

import com.voidtechnologies.cultus.commands.ui.Menu;
import com.voidtechnologies.cultus.commands.ui.MenuItem;
import org.bukkit.Material;
import org.bukkit.event.inventory.InventoryClickEvent;

/**
 * @author Dylan Holmes 
 * @date Feb 9, 2018
 */
public class GenericActionButton extends MenuItem {

    private Runnable action;
    
    public GenericActionButton(String name, String desc, Material item, Runnable runnable) {
        super(name, desc, item, 1);
        this.action = runnable;
    }

    @Override
    public boolean processClick(Menu menu, InventoryClickEvent event) {
        action.run();
        return true;
    }
}
package com.voidtechnologies.cultus.commands.ui.menus;

import com.voidtechnologies.cultus.civilizations.Camp;
import com.voidtechnologies.cultus.commands.ui.Menu;
import com.voidtechnologies.cultus.commands.ui.MenuItem;
import com.voidtechnologies.cultus.commands.ui.MenuSize;
import com.voidtechnologies.cultus.commands.ui.buttons.GenericDisplayButton;
import java.util.HashMap;
import org.bukkit.ChatColor;
import org.bukkit.Material;

/**
 * @author Dylan Holmes
 * @date Feb 9, 2018
 */
public class ViewCampInfoMenu extends Menu {

    private Camp camp;

    public ViewCampInfoMenu(Camp camp, Menu sender) {
        super("Camp Info", MenuSize.SMALLEST, sender);
        this.camp = camp;
    }

    @Override
    public void generateButtons() {
        HashMap<Integer, MenuItem> btns = getButtons();
        btns.put(1, new GenericDisplayButton("Members", ChatColor.DARK_PURPLE + "There are " + camp.getAmountOfOnlineMembers() + "/" + camp.getAmountOfMembers() + " online!", Material.PLAYER_HEAD, camp.getAmountOfMembers()));
        btns.put(2, new GenericDisplayButton("Money", ChatColor.YELLOW + "$" + camp.getMoney(), Material.GOLD_INGOT, 1));

    }
}

package com.voidtechnologies.cultus.commands.ui.buttons;

import com.voidtechnologies.cultus.commands.ui.Menu;
import com.voidtechnologies.cultus.commands.ui.MenuItem;
import org.bukkit.Material;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;

/**
 * @author Dylan Holmes
 * @date Jan 27, 2018
 */
public class GenericCloseMenuButton extends MenuItem {

    public GenericCloseMenuButton() {
        super("Back", "Takes you to the previous menu", Material.BARRIER, 1);
    }

    @Override
    public boolean processClick(Menu menu, InventoryClickEvent event) {
        Menu sender = menu.getSender();
        event.getWhoClicked().closeInventory();
        if (sender != null) {
            Inventory inv = sender.createInventory();
            event.getWhoClicked().openInventory(inv);
        }

        return true;
    }
}

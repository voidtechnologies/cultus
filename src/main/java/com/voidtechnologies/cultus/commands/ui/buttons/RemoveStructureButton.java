package com.voidtechnologies.cultus.commands.ui.buttons;

import com.voidtechnologies.cultus.commands.ui.Menu;
import com.voidtechnologies.cultus.commands.ui.MenuItem;
import com.voidtechnologies.cultus.listeners.CivListener;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;

/**
 * @author Dylan Holmes 
 * @date Jan 28, 2018
 */
public class RemoveStructureButton extends MenuItem {

    public RemoveStructureButton() {
        super("Remove Structure", "Tries to remove the next structure you hit.", Material.TNT, 1);
    }

    @Override
    public boolean processClick(Menu menu, InventoryClickEvent event) {
        Player player = (Player) event.getWhoClicked();
        player.closeInventory();
        
        player.sendMessage(ChatColor.RED + "Warning the next structure you hit will be removed!");
        CivListener.removeStructure.add(player);
        return true;
    }
}
package com.voidtechnologies.cultus.commands.ui.buttons;

import com.voidtechnologies.cultus.commands.ui.Menu;
import com.voidtechnologies.cultus.commands.ui.MenuItem;
import org.bukkit.Material;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;

/**
 * @author Dylan Holmes
 * @date Jan 22, 2018
 */
public class GenericDisplayButton extends MenuItem {

    public GenericDisplayButton(String name, String desc, Material item, int stackAmt) {
        super(name, desc, item, stackAmt);
    }

    public GenericDisplayButton(ItemStack item) {
        super(item);
    }

    @Override
    public boolean processClick(Menu menu, InventoryClickEvent event) {
        return true;
    }

}

package com.voidtechnologies.cultus.commands.ui.menus;

import com.voidtechnologies.cultus.civilizations.Camp;
import com.voidtechnologies.cultus.commands.ui.Menu;
import com.voidtechnologies.cultus.commands.ui.MenuSize;
import com.voidtechnologies.cultus.commands.ui.buttons.GenericActionButton;
import com.voidtechnologies.cultus.commands.ui.buttons.GenericCloseMenuButton;
import com.voidtechnologies.cultus.managers.PlayerManager;
import com.voidtechnologies.cultus.players.CPlayer;
import com.voidtechnologies.cultus.structures.StructureType;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;

/**
 * @author Dylan Holmes
 * @date Feb 9, 2018
 */
public class CampUpgradesMenu extends Menu {

    private Camp camp;

    public CampUpgradesMenu(Camp camp, Menu sender) {
        super("Camp Upgrades", MenuSize.SMALL, sender);
        this.camp = camp;
    }

    @Override
    public void generateButtons() {
        getButtons().put(1, new GenericActionButton("Garden", "Grows wheat more efficiently.\n$" + StructureType.GARDEN.getCost(), Material.TALL_GRASS, () -> {
        }));
        getButtons().put(0, new GenericActionButton("Sifter", "Sifts cobblestone for minerals.\n$" + StructureType.SIFTER.getCost(), Material.COBBLESTONE, () -> {
        }));
    }

    @Override
    public void processInvClick(InventoryClickEvent context, int slot) {
        if (getButtons().get(slot) instanceof GenericCloseMenuButton) {
            super.processInvClick(context, slot);
            return;
        }

        CPlayer clicker = PlayerManager.getPlayerByHandle((Player) context.getWhoClicked());
        
        if (!camp.containsMember(clicker.getUUID())) {
            clicker.sendMessage(ChatColor.RED + "You are not apart of this camp!");
            context.setCancelled(true);
            return;
        }

        StructureType st = slot == 0 ? StructureType.SIFTER : StructureType.GARDEN;

        camp.unlockUpgrade(clicker, st, null);
        super.processInvClick(context, slot);
    }
}

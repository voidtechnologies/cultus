package com.voidtechnologies.cultus.commands.ui.buttons;

import com.voidtechnologies.cultus.civilizations.Nation;
import com.voidtechnologies.cultus.commands.ui.Menu;
import com.voidtechnologies.cultus.commands.ui.MenuItem;
import com.voidtechnologies.cultus.managers.PlayerManager;
import com.voidtechnologies.cultus.players.CPlayer;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;

/**
 * @author Dylan Holmes
 * @date Jan 29, 2018
 */
public class CancelResearchButton extends MenuItem {

    private Nation civ;

    public CancelResearchButton(Nation civ) {
        super("Cancel Current Research", "Cancels your progress on your current research\nrefunding 75% of the cost($).", Material.MUSIC_DISC_11, 1);
        this.civ = civ;
    }

    @Override
    public boolean processClick(Menu menu, InventoryClickEvent event) {
        CPlayer player = PlayerManager.getPlayerByHandle((Player) event.getWhoClicked());
        String res = "";
        if (!civ.containsMember(player.getUUID())) {
            res = ChatColor.RED + "You are not in this civilization!";
        } else if (player.hasPermission("cultus.nation.research.cancel")) {
            res = ChatColor.RED + "You do not have permission to cancel research in this civilization!";
        } else if (civ.getCurrentResearchTaskId() == -1) {
            res = ChatColor.RED + "Your civilization does not have a technology currently being researched!";
        } else {
            res = ChatColor.GREEN + "Successfully cancelled research!";
        }
        civ.cancelCurrentResearch();
        player.sendMessage(res);
        return true;
    }
}

package com.voidtechnologies.cultus.commands.ui.buttons;

import com.voidtechnologies.cultus.civilizations.Nation;
import com.voidtechnologies.cultus.commands.ui.Menu;
import com.voidtechnologies.cultus.commands.ui.MenuItem;
import com.voidtechnologies.cultus.players.CPlayer;
import com.voidtechnologies.cultus.managers.PlayerManager;
import com.voidtechnologies.cultus.structures.StructureType;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;

/**
 * @author Dylan Holmes
 * @date Jan 22, 2018
 */
public class StructureButton extends MenuItem {

    private Nation civ;
    private StructureType struct;

    public StructureButton(Nation civ, StructureType struct) {
        super(struct.getName(), struct.getDesc(), struct.getMat(), 1);
        this.civ = civ;
        this.struct = struct;
    }

    @Override
    public boolean processClick(Menu menu, InventoryClickEvent event) {
        String result = "";
        Player player = ((Player) event.getWhoClicked());
        CPlayer cp = PlayerManager.getPlayerByHandle(player);

        if (!civ.containsMember(cp.getUUID())) {
            result = ChatColor.RED + "You are not apart of this Civilization!";
        } else {
            result = civ.startBuildOfStructure(cp, struct, player.getLocation().clone());
        }

        player.closeInventory();
        player.sendMessage(result);
        return true;
    }

}

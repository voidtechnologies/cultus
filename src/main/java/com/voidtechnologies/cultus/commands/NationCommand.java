package com.voidtechnologies.cultus.commands;

import com.voidtechnologies.cultus.civilizations.Civilization;
import com.voidtechnologies.cultus.civilizations.Nation;
import com.voidtechnologies.cultus.managers.CivilizationManager;
import com.voidtechnologies.cultus.managers.PlayerManager;
import com.voidtechnologies.cultus.players.CPlayer;
import java.util.UUID;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * @author Dylan Holmes
 * @date Jan 29, 2018
 */
public class NationCommand extends CultusCommand {

    public NationCommand() {
        super("nation");
    }

    @Override
    public boolean onCommand(CommandSender cs, Command cmnd, String string, String[] strings) {
        int options = strings.length;

        if (!(cs instanceof Player)) {
            cs.sendMessage(ChatColor.RED + "This command must be executed by a player in-game.");
            return true;
        }

        if (options < 1) {
            cs.sendMessage(ChatColor.RED + "Nation Command Usages:");
            cs.sendMessage(ChatColor.YELLOW + "/nation permlist");
            cs.sendMessage(ChatColor.YELLOW + "/nation setperm <permission> <player_name> [true|false]");
            cs.sendMessage(ChatColor.YELLOW + "/nation message <message>");
            cs.sendMessage(ChatColor.YELLOW + "/nation balance");
            cs.sendMessage(ChatColor.YELLOW + "/nation deposit <amount>");
            cs.sendMessage(ChatColor.YELLOW + "/nation withdraw <amount>");
            cs.sendMessage(ChatColor.YELLOW + "/nation invite <player_name>");
            cs.sendMessage(ChatColor.YELLOW + "/nation leave");
            cs.sendMessage(ChatColor.YELLOW + "/nation kick <player_name>");
            cs.sendMessage(ChatColor.YELLOW + "/nation invite <player_name>");
            return true;
        }
        Player player = (Player) cs;
        CPlayer cPlayer = PlayerManager.getPlayerByHandle(player);

        Civilization nation = cPlayer.getCiv();

        if (nation == null || !(nation instanceof Nation)) {
            cs.sendMessage(ChatColor.RED + "You must be apart of a nation to use this command!");
            return true;
        }

        String result = "";

        switch (string.toLowerCase()) {
            case "n":
            case "nation":
                result = processNationCommand(cPlayer, strings, options);
                break;
        }
        if (!result.equals("")) {
            player.sendMessage(result);
        }
        return true;
    }

    public String processNationCommand(CPlayer player, String[] args, int argAmt) {
        String output;
        Nation nat = (Nation) player.getCiv();
        if (nat == null) {
            return ChatColor.RED + "You are not apart of a Nation!";
        }
        switch (args[0].toLowerCase()) {
            case "permlist":
                return permListMessage();
            case "setperm":
                return processNationSetPerm(player, args, nat);
            case "message":
                return processNationChat(args, player, nat);
            case "balance":
                return processNationBalance(player, nat);
            case "leave":
                return processNationLeave(player, nat);
            case "invite":
                return processNationInvite(player, args[1], nat);
            case "kick":
                return processNationKick(args[1], player, nat);
            case "deposit":
                return processNationDeposit(args[1], player, nat);
            case "withdraw":
                return processNationWithdraw(args[1], player, nat);
            default:
                player.sendMessage(ChatColor.RED + "Nation Command Usages:");
                player.sendMessage(ChatColor.YELLOW + "/nation permlist");
                player.sendMessage(ChatColor.YELLOW + "/nation setperm <permission> <player_name> [true|false]");
                player.sendMessage(ChatColor.YELLOW + "/nation message <message>");
                player.sendMessage(ChatColor.YELLOW + "/nation balance");
                player.sendMessage(ChatColor.YELLOW + "/nation deposit <amount>");
                player.sendMessage(ChatColor.YELLOW + "/nation withdraw <amount>");
                player.sendMessage(ChatColor.YELLOW + "/nation invite <player_name>");
                player.sendMessage(ChatColor.YELLOW + "/nation leave");
                player.sendMessage(ChatColor.YELLOW + "/nation kick <player_name>");
                player.sendMessage(ChatColor.YELLOW + "/nation invite <player_name>");
                break;
        }
        return "";
    }

    public String processNationSetPerm(CPlayer executor, String[] arguments, Nation nation) {

        if (arguments.length < 3) {
            return ChatColor.RED + "You need a permission, a player's name, and true or false to set a player's permissions!";
        }

        if (!executor.hasPermission("cultus.nation.setperm")) {
            return ChatColor.RED + "You do not have permission to set the permissions of other players!";
        }

        String perm = "cultus.nation." + arguments[1];
        String name = arguments[2];
        boolean allowed = Boolean.valueOf(arguments[3]);
        CPlayer cPlayer = PlayerManager.getPlayerByUUID(Bukkit.getOfflinePlayer(name).getUniqueId());

        if (!nation.containsMember(cPlayer.getUUID())) {
            return ChatColor.RED + "That player is not in your Nation!";
        }

        if (cPlayer.hasPermission("cultus.nation.kick") && !executor.hasPermission("cultus.nation.owner")) {
            return ChatColor.RED + "You cannot set the perms of someone who also has the permission to set perms, unless you're the owner of the nation!";
        }

        if (allowed) {
            cPlayer.addPermission(perm);
        } else {
            cPlayer.removePermission(perm);
        }

        return ChatColor.GREEN + "Successfully set the permission, " + perm + ",  of player, " + executor + " to " + allowed + ".";
    }

    public String processNationInvite(CPlayer player, String other, Nation nation) {

        UUID id = player.getUUID();
        if (player.hasPermission("cultus.nation.invite")) {
            return ChatColor.RED + "You do not have permission to invite members to this Nation!";
        }

        Player invited = Bukkit.getPlayer(other);

        if (invited == null) {
            return ChatColor.RED + "This player is not online! A player must be online in order to be invited to a Nation.";
        }

        CPlayer cPlayer = PlayerManager.getPlayerByUUID(invited.getUniqueId());

        if (cPlayer.getCiv() != null) {
            return ChatColor.RED + "That player is already apart of another Civilization (Camp or Nation)!";
        }

        String pName = nation.getCivName();

        cPlayer.sendMessage(ChatColor.BLUE + "You have been invited to the " + ChatColor.YELLOW + ChatColor.BLUE + " nation!");
        cPlayer.sendMessage(ChatColor.BLUE + "Type /na " + pName + ", /nationaccept " + pName + ", or /nation accept " + pName + " to accept!");

        CivilizationManager.addInvite(invited.getUniqueId(), pName);

        return ChatColor.GREEN + "Successfully invited player to Nation!";
    }

    private String processNationKick(String name, CPlayer executor, Nation nation) {
        if (!executor.hasPermission("cultus.nation.kick")) {
            return ChatColor.RED + "You do not have permission to kick in this Nation!";
        }

        CPlayer cPlayer = PlayerManager.getPlayerByUUID(Bukkit.getOfflinePlayer(name).getUniqueId());

        if (!nation.containsMember(cPlayer.getUUID())) {
            return ChatColor.RED + "That player is not in your Nation!";
        }

        if (cPlayer.hasPermission("cultus.nation.kick") && !executor.hasPermission("cultus.nation.owner")) {
            return ChatColor.RED + "You cannot kick someone who also has the permission to kick, unless you're the owner of the nation!";
        }

        cPlayer.sendMessage(ChatColor.RED + "You have been kicked from the Nation " + nation.getCivName() + "!");
        nation.removeMember(cPlayer.getUUID());
        nation.sendMessageToMembers("Player " + cPlayer.getName() + " has been kicked from the Nation!");
        cPlayer.setCiv(null);
        return ChatColor.GREEN + "Successfully kicked player from the Nation!";
    }

    private String processNationChat(String[] strings, CPlayer player, Nation nation) {
        String msg = String.join(" ", strings);

        nation.sendMessageToMembers(ChatColor.BLUE + player.getName() + ": " + ChatColor.YELLOW + msg);
        return null;
    }

    private String processNationWithdraw(String amount, CPlayer player, Nation nation) {
        if (nation == null) {
            return ChatColor.RED + "You are not in a party!";
        }
        if (!player.hasPermission("cultus.nation.withdraw")) {
            return ChatColor.RED + "You do not have permission to withdraw money from this Nation!";
        }

        Double dAmount = Double.valueOf(amount);

        if (dAmount == null || dAmount == 0) {
            return ChatColor.RED + "Must enter a valid amount to withdraw!";
        }

        if (nation.getMoney() < dAmount) {
            return ChatColor.RED + "Your Nation does not have enough money to withdraw that much!";
        }

        nation.deductMoney(dAmount);
        player.addMoney(dAmount);
        return ChatColor.GREEN + "Successfully withdrew $" + dAmount + " from the Nation!";
    }

    private String processNationDeposit(String amount, CPlayer player, Nation nation) {
        Double dAmount = Double.valueOf(amount);

        if (dAmount == null || dAmount == 0) {
            return ChatColor.RED + "Must enter a valid amount to withdraw!";
        }

        if (player.getMoney() < dAmount) {
            return ChatColor.RED + "You do not have enough money to deposit that much!";
        }

        nation.addMoney(dAmount);
        player.deductMoney(dAmount);
        return ChatColor.GREEN + "Successfully deposited $" + dAmount + " to the Nation!";
    }

    private String processNationBalance(CPlayer player, Nation nat) {
        return ChatColor.GREEN + "Your nation currently has $" + nat.getMoney() + "!";
    }

    private String permListMessage() {
        return "building.build\nbuilding.destroy\nsetperm\nowner\nresearch.start\nresearch.cancel\nwithdraw\nkick\ninvite\n";
    }

    private String processNationLeave(CPlayer executor, Nation nat) {
        if (executor.hasPermission("cultus.nation.owner")) {
            return ChatColor.RED + "You must set someone else to be the owner before leaving!";
        }
        nat.removeMember(executor.getUUID());
        return ChatColor.GREEN + "Successfuly remove yourself from the nation!";
    }
}

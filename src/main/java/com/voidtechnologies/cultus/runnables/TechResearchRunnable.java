package com.voidtechnologies.cultus.runnables;

import com.voidtechnologies.cultus.civilizations.Civilization;
import com.voidtechnologies.cultus.civilizations.Nation;
import com.voidtechnologies.cultus.structures.StructureType;
import com.voidtechnologies.cultus.technology.Technologies;
import org.bukkit.ChatColor;

/**
 * @author Dylan Holmes
 * @date Jan 8, 2018
 */
public class TechResearchRunnable implements Runnable {

    private Technologies tech;
    private double cost, progress, prevProgress;
    private Nation civ;

    public TechResearchRunnable(Technologies tech, Nation civ) {
        this.tech = tech;
        this.civ = civ;
        this.cost = tech.getCost();
        this.progress = 0;
    }

    public TechResearchRunnable(Nation civ, Technologies tech, double progress, double cost) {
        this.cost = cost;
        this.civ = civ;
        this.tech = tech;
        this.progress = progress;
        this.prevProgress = progress;
    }

    @Override
    public void run() {
        double beaks = civ.getBeakers();
        if (civ.hasStructure(StructureType.LAB)) {
            cost -= beaks * 1.25;
        } else {
            cost -= beaks;
        }
        if (cost < 0) {
            cost = 0;
        }
        prevProgress = progress;
        progress = (1.0 - (cost / tech.getCost())) * 100.0;
        double threshold = round(prevProgress, 10);

        if (progress >= 100.0) {
            finish();
            return;
        }
        if (progress > threshold) {
            updateResearchProgress();
        }
    }

    public Technologies getTech() {
        return tech;
    }

    public double getProgress() {
        return progress;
    }

    public double getCost() {
        return cost;
    }

    public Civilization getCiv() {
        return civ;
    }

    public void finish() {
        civ.sendMessageToMembers(ChatColor.AQUA + "Your civilization has finished researching " + tech.getName() + "!");
        civ.finishCurrentResearch();
    }

    public void updateResearchProgress() {
        civ.sendMessageToMembers(ChatColor.GREEN + "Your research of " + tech.getName() + " is now " + Math.round(progress + 0.5) + "% finished!");
    }

    private double round(double num, int multipleOf) {
        return Math.ceil((num) / multipleOf) * multipleOf;
    }
}

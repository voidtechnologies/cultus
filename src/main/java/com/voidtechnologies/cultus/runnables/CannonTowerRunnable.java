package com.voidtechnologies.cultus.runnables;

import com.voidtechnologies.cultus.civilizations.Civilization;
import java.util.Collection;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Monster;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

/**
 * @author Dylan Holmes
 * @date Jan 18, 2018
 */
public class CannonTowerRunnable implements Runnable {

    private int distance;
    private Location spawnLoc;
    private Civilization civ;

    public CannonTowerRunnable(int distance, Location loc, Civilization civ) {
        this.distance = distance;
        this.spawnLoc = loc;
        this.civ = civ;
    }

    @Override
    public void run() {
        Collection<Entity> nearbyEntities = spawnLoc.getWorld().getNearbyEntities(spawnLoc, distance, distance + 30, distance);
        LivingEntity target = null;
        for (Entity ent : nearbyEntities) {
            if (!(ent instanceof Monster) && !(ent instanceof Player)) {
                continue;
            }

            if (civ.containsMember(ent.getUniqueId())) {
                continue;
            }

            if (hasLineOfSight(spawnLoc.clone(), ent.getLocation().toVector(), (int) (distance * 2.5))) {
                target = (LivingEntity) ent;
                break;
            }
        }
        if (target == null) {
            return;
        }

        double dX = spawnLoc.getX() - target.getLocation().getX();
        double dY = spawnLoc.getY() - (target.getLocation().getY() + 2.25);
        double dZ = spawnLoc.getZ() - target.getLocation().getZ();

        double yaw = Math.atan2(dZ, dX);
        double pitch = Math.atan2(Math.sqrt(dZ * dZ + dX * dX), dY) + Math.PI;

        double X = Math.sin(pitch) * Math.cos(yaw);
        double Y = Math.sin(pitch) * Math.sin(yaw);
        double Z = Math.cos(pitch);

        Vector vector = new Vector(X, Z, Y);

        Arrow spawnArrow = spawnLoc.getWorld().spawnArrow(spawnLoc.add(0, .125, 0), vector, (float) 7, (float) 0);
        spawnArrow.setCritical(true);
        spawnArrow.setCustomName("EXPLOSIVE_ARROW");
    }

    public boolean hasLineOfSight(Location loc, Vector target, int maxDistance) {
        Vector from = loc.toVector();
        double magnitude = from.distance(target);

        double xDist = (target.getX() - from.getX()) / magnitude;
        double yDist = (target.getY() - from.getY()) / magnitude;
        double zDist = (target.getZ() - from.getZ()) / magnitude;

        Location current = loc.add(0, .75, 0).clone();
        while (current.toVector().distance(target) > .8) {
            if (current.toVector().distance(from) > maxDistance) {
                return false;
            }
            if (current.getBlock().getType().equals(Material.AIR)) {
                current = current.add(xDist, yDist, zDist);
                continue;
            }
            if ((current.getBlock().getType().isSolid() || !current.getBlock().getType().isTransparent())) {
                return false;
            }
            current = current.add(xDist, yDist, zDist);
        }

        return true;
    }

    public double getDirection(double f, double t) {
        double r = f - t;
        return r < 0 ? .8 : -.8;
    }
}
package com.voidtechnologies.cultus.runnables;

import com.voidtechnologies.cultus.civilizations.Nation;
import com.voidtechnologies.cultus.structures.Constructure;
import com.voidtechnologies.cultus.structures.Structure;
import com.voidtechnologies.cultusschematics.Schematic;
import com.voidtechnologies.cultus.structures.StructureType;
import org.bukkit.ChatColor;
import org.bukkit.Location;

/**
 * @author Dylan Holmes
 * @date Jan 5, 2018
 */
public class ConstructionRunnable implements Runnable {
    private Location loc;
    private double progress, prevProgress;
    private Nation civ;
    private Constructure constructure;
    private StructureType type;

    public ConstructionRunnable(Nation civ, StructureType structure, Location loc) {
        this.progress = 0;
        this.prevProgress = 0;
        this.civ = civ;
        this.loc = loc;
        this.constructure = new Constructure(structure, loc);
    }

    public ConstructionRunnable(Nation civ, StructureType structure, Location loc, double progress) {
        this.civ = civ;
        this.progress = progress;
        this.prevProgress = progress;
        this.loc = loc;
        this.constructure = new Constructure(structure, loc);
        this.constructure.updateProgress(progress / 100.0 * constructure.getRequiredHammers());
    }
    
    public Constructure getConstructure() {
        return this.constructure;
    }
    
    @Override
    public void run() {
        double hamms = civ.getHammers();
        if (civ.hasStructure(StructureType.FORGE)) {
            hamms *= 1.35;
        }
        
        constructure.updateProgress(hamms);
        prevProgress = progress;
        progress = constructure.getCurrentHammers() / constructure.getRequiredHammers() * 100.0;
        
        constructure.getSchematic().pastePercent(loc, progress/100.0);
        
        double threshold = round(prevProgress, 10);

        if (progress >= 100.0) {
            finish();
            return;
        }
        
        if (progress > threshold) {
            updateBuildProgress();
        }
    }

    public double getProgress() {
        return progress;
    }


    public void finish() {
        civ.sendMessageToMembers(ChatColor.YELLOW + "Your civilization has finished building " + constructure.getStructureType().getName() + "!");
        civ.finishStructure(this, constructure.build());
    }

    public Location getLocation() {
        return loc;
    }

    public void updateBuildProgress() {
        civ.sendMessageToMembers(ChatColor.GOLD + "The building of " + constructure.getStructureType().getName() + " is now " + Math.round(progress + 0.5) + "% finished!");
    }

    private double round(double num, int multipleOf) {
        return Math.ceil((num) / multipleOf) * multipleOf;
    }
}

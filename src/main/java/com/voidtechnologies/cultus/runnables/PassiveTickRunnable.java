package com.voidtechnologies.cultus.runnables;

import com.voidtechnologies.cultus.civilizations.Civilization;
import com.voidtechnologies.cultus.managers.CivilizationManager;
import java.util.Collection;

/**
 * @author Dylan Holmes
 * @date Feb 7, 2018
 */
public class PassiveTickRunnable implements Runnable {

    @Override
    public void run() {
        Collection<Civilization> civilizations = CivilizationManager.civilizations();

        civilizations.forEach((Civilization civ) -> {
            civ.getStructures().forEach((struct) -> {
                struct.passiveTick(civ);
            });
        });
    }
}

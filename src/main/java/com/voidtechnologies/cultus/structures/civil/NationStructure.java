/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.voidtechnologies.cultus.structures.civil;

import com.voidtechnologies.cultus.civilizations.Civilization;
import com.voidtechnologies.cultus.civilizations.Nation;
import com.voidtechnologies.cultus.civilizations.NationType;
import com.voidtechnologies.cultus.structures.Position2d;
import com.voidtechnologies.cultus.structures.Position3d;
import com.voidtechnologies.cultus.structures.Structure;
import com.voidtechnologies.cultus.structures.StructureType;
import java.util.List;
import org.bukkit.Location;

/**
 *
 * @author Dylan
 */
public class NationStructure extends Structure {

    public NationStructure(Location loc, List<Position2d> chunks, List<Position3d> blocks) {
        super(StructureType.NATION, loc, chunks, blocks);
    }

    public NationStructure(Location loc) {
        super(StructureType.NATION, loc);
    }

    @Override
    public void onLoad(Civilization civ) {
        super.onLoad(civ);
    }

    @Override
    public void onBuild(Civilization civ) {
        super.onBuild(civ);
    }

    @Override
    public double upkeepTick(Civilization civ) {
        Nation nat = (Nation) civ;
        nat.updateResourcesFromChunks();
        
        nat.updateNationType();
        nat.updateHappiness();
        return super.upkeepTick(civ);
    }
}

package com.voidtechnologies.cultus.structures.civil;

import com.voidtechnologies.cultus.civilizations.Civilization;
import com.voidtechnologies.cultus.generator.LuxuryResource;
import com.voidtechnologies.cultus.managers.LuxuryManager;
import com.voidtechnologies.cultus.structures.Position2d;
import com.voidtechnologies.cultus.structures.Position3d;
import com.voidtechnologies.cultus.structures.Structure;
import com.voidtechnologies.cultus.structures.StructureType;
import java.util.List;
import org.bukkit.Location;

/**
 *
 * @author Dylan Sep 2, 2021
 */
public class TradePost extends Structure {

    private LuxuryResource res;

    public TradePost(Location loc, List<Position2d> chunks, List<Position3d> blocks) {
        super(StructureType.TRADE_POST, loc, chunks, blocks);
    }

    public TradePost(Location loc) {
        super(StructureType.TRADE_POST, loc);
    }

    @Override
    public void onLoad(Civilization civ) {
        super.onLoad(civ); //To change body of generated methods, choose Tools | Templates.
        setupPost();
    }

    @Override
    public void onDestroy(Civilization civ) {
        super.onDestroy(civ); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void onBuild(Civilization civ) {
        super.onBuild(civ); //To change body of generated methods, choose Tools | Templates.
        setupPost();
    }

    @Override
    public void passiveTick(Civilization civ) {
        super.passiveTick(civ); //To change body of generated methods, choose Tools | Templates.

        civ.addMoney(250);
        civ.sendMessageToMembers("$250 has been earned from the Trade Post harvesting " + res.getMaterial().name() + "!");
    }

    private void setupPost() {
        Position2d pos = Position2d.convertChunkToPosition(getLocation().getChunk());

        LuxuryResource lr = LuxuryManager.getResource(pos);

        if (lr == null) {
            System.out.println("Unable to load Luxury Resource @ " + pos.toString());
        }
        res = lr;
    }

    public LuxuryResource getResource() {
        return res;
    }
}

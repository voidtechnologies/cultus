package com.voidtechnologies.cultus.structures.civil;

import static com.voidtechnologies.cultus.Utility.removeMaterial;
import com.voidtechnologies.cultus.civilizations.Civilization;
import com.voidtechnologies.cultus.structures.Position2d;
import com.voidtechnologies.cultus.structures.Position3d;
import com.voidtechnologies.cultus.structures.Structure;
import com.voidtechnologies.cultus.structures.StructureType;
import java.util.List;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Chest;
import org.bukkit.block.DoubleChest;

/**
 * @author Dylan Holmes
 * @date Feb 17, 2018
 */
public class Granary extends Structure {

    private DoubleChest inputChest;

    public Granary(Location loc, List<Position2d> chunks, List<Position3d> blocks) {
        super(StructureType.GRANARY, loc, chunks, blocks);
    }

    public Granary(Location loc) {
        super(StructureType.GRANARY, loc);
    }

    @Override
    public double upkeepTick(Civilization civ) {
        if (inputChest != null) {
            int bread = removeMaterial(inputChest.getInventory(), Material.BREAD, 32);
            int carrot = removeMaterial(inputChest.getInventory(), Material.CARROT, 32);
            int potatoe = removeMaterial(inputChest.getInventory(), Material.BAKED_POTATO, 32);

            processFoods(civ, bread, carrot, potatoe);
        } else {
            loadChests();
        }

        return super.upkeepTick(civ);
    }

    @Override
    public void onBuild(Civilization civ) {
        loadChests();
        super.onBuild(civ);
    }

    @Override
    public void onLoad(Civilization civ) {
        loadChests();
    }

    private void loadChests() {
        System.out.println("Loading Granary chests...");
        try {
            inputChest = (DoubleChest) ((Chest) this.getBlocks().get(67).toBlock().getState()).getInventory().getHolder();
        } catch (ClassCastException ex) {
            System.out.println("failed");
        }
    }

    private void processFoods(Civilization civ, int bread, int carrot, int potatoe) {
        int reward = 15 * bread;
        reward += 10 * carrot;
        reward += 30 * potatoe;

        if (reward > 0) {
            civ.addMoney(reward);
            civ.sendMessageToMembers(ChatColor.GREEN + "A granary generated $" + (reward) + "!");
        }
    }
}


package com.voidtechnologies.cultus.structures.civil;

import com.voidtechnologies.cultus.civilizations.Civilization;
import com.voidtechnologies.cultus.civilizations.Nation;
import com.voidtechnologies.cultus.structures.Position2d;
import com.voidtechnologies.cultus.structures.Position3d;
import com.voidtechnologies.cultus.structures.Structure;
import com.voidtechnologies.cultus.structures.StructureType;
import java.util.List;
import org.bukkit.Location;

/**
 *
 * @author Dylan
 * Aug 18, 2021
 */
public class Obelisk extends Structure {

    public Obelisk(Location loc, List<Position2d> chunks, List<Position3d> blocks) {
        super(StructureType.OBELISK, loc, chunks, blocks);
    }

    public Obelisk(Location loc) {
        super(StructureType.OBELISK, loc);
    }

    @Override
    public double upkeepTick(Civilization civ) {
        Nation nat = (Nation) civ;
        nat.setInfluence(nat.getInfluence() + 0.25f);
        return super.upkeepTick(civ);
    }
}
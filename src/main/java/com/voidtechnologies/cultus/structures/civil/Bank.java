package com.voidtechnologies.cultus.structures.civil;

import com.voidtechnologies.cultus.structures.Position2d;
import com.voidtechnologies.cultus.structures.Position3d;
import com.voidtechnologies.cultus.structures.Structure;
import com.voidtechnologies.cultus.structures.StructureType;
import java.util.List;
import org.bukkit.Location;

/**
 * @author Dylan Holmes
 * @date Feb 17, 2018
 */
public class Bank extends Structure {

    public Bank(Location loc, List<Position2d> chunks, List<Position3d> blocks) {
        super(StructureType.BANK, loc, chunks, blocks);
    }

    public Bank(Location loc) {
        super(StructureType.BANK, loc);
    }

}
package com.voidtechnologies.cultus.structures.camp;

import com.voidtechnologies.cultus.civilizations.Civilization;
import com.voidtechnologies.cultus.structures.Position2d;
import com.voidtechnologies.cultus.structures.Position3d;
import com.voidtechnologies.cultus.structures.Structure;
import com.voidtechnologies.cultus.structures.StructureType;
import java.util.List;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Chest;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

/**
 * @author Dylan Holmes
 * @date Feb 8, 2018
 */
public class CampStructure extends Structure {

    private Chest inputChest;

    public CampStructure(Location loc, List<Position2d> chunks, List<Position3d> blocks) {
        super(StructureType.CAMP, loc, chunks, blocks);
    }

    public CampStructure(Location loc) {
        super(StructureType.CAMP, loc);
    }

    @Override
    public void onLoad(Civilization civ) {
        loadChests();
    }

    @Override
    public void onBuild(Civilization civ) {
        super.onBuild(civ);
        loadChests();
    }

    @Override
    public double upkeepTick(Civilization civ) {
        if (inputChest != null) {
            int amount = removeMaterial(inputChest.getBlockInventory(), Material.BREAD, 32);
            if (amount != 0) {
                processBread(civ, amount);
            }
        } else {
            System.out.println("Camp chest is null");
            loadChests();
        }

        return super.upkeepTick(civ);
    }

    private int removeMaterial(Inventory inv, Material mat, int max) {
        int removed = 0;
        for (ItemStack is : inv.getContents()) {
            if (is == null) {
                continue;
            }
            if (!is.getType().equals(mat)) {
                continue;
            }

            if (is.getAmount() >= max) {
                is.setAmount(is.getAmount() - max);
                return max;
            }

            removed += is.getAmount();
            is.setAmount(0);
            if (removed == max) {
                return max;
            }
        }
        return removed;
    }

    private void loadChests() {
        try {
            inputChest = (Chest) this.getBlocks().get(6977).toBlock().getState();
        } catch (ClassCastException ex) {
        }
    }

    private void processBread(Civilization civ, int amount) {
        civ.addMoney(amount * 10);
        civ.sendMessageToMembers(ChatColor.GREEN + "Generated " + (amount * 10) + " money from upkeep!");
    }
}

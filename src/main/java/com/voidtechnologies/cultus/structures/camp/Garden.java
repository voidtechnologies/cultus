package com.voidtechnologies.cultus.structures.camp;

import static com.voidtechnologies.cultus.Utility.removeMaterial;
import com.voidtechnologies.cultus.civilizations.Civilization;
import com.voidtechnologies.cultus.structures.Position2d;
import com.voidtechnologies.cultus.structures.Position3d;
import com.voidtechnologies.cultus.structures.Structure;
import com.voidtechnologies.cultus.structures.StructureType;
import java.util.List;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Chest;
import org.bukkit.inventory.ItemStack;

/**
 * @author Dylan Holmes
 * @date Feb 7, 2018
 */
public class Garden extends Structure {

    private Chest inputChest, outputChest;

    public Garden(Location loc, List<Position2d> chunks, List<Position3d> blocks) {
        super(StructureType.GARDEN, loc, chunks, blocks);
    }

    public Garden(Location loc) {
        super(StructureType.GARDEN, loc);
    }

    @Override
    public double upkeepTick(Civilization civ) {
        if (inputChest != null) {
            int amount = removeMaterial(inputChest.getBlockInventory(), Material.WHEAT_SEEDS, 32);
            if (amount != 0) {
                processCrops(amount);
            }
        } else {
            loadChests();
        }

        return super.upkeepTick(civ);
    }

    @Override
    public void onBuild(Civilization civ) {
        loadChests();
        super.onBuild(civ);
    }

    @Override
    public void onLoad(Civilization civ) {
        loadChests();
    }

    private void loadChests() {
        System.out.println("Loading garden chests...");
        try {
            inputChest = (Chest) this.getBlocks().get(3).toBlock().getState();
            outputChest = (Chest) this.getBlocks().get(43).toBlock().getState();
        } catch (ClassCastException ex) {
            System.out.println("failed");
        }
    }

    private void processCrops(int amount) {
        ItemStack reward = new ItemStack(Material.WHEAT, 3 * amount);
        outputChest.getBlockInventory().addItem(reward);
    }
}

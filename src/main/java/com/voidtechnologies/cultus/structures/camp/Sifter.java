package com.voidtechnologies.cultus.structures.camp;

import com.voidtechnologies.cultus.civilizations.Civilization;
import com.voidtechnologies.cultus.structures.Position2d;
import com.voidtechnologies.cultus.structures.Position3d;
import com.voidtechnologies.cultus.structures.Structure;
import com.voidtechnologies.cultus.structures.StructureType;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Chest;
import org.bukkit.inventory.ItemStack;

/**
 * @author Dylan Holmes
 * @date Feb 7, 2018
 */
public class Sifter extends Structure {

    private Chest inputChest, outputChest;

    public Sifter(Location loc, List<Position2d> chunks, List<Position3d> blocks) {
        super(StructureType.SIFTER, loc, chunks, blocks);
    }

    public Sifter(Location loc) {
        super(StructureType.SIFTER, loc);
    }

    @Override
    public void passiveTick(Civilization civ) {
        if (inputChest == null) {
            loadChests();
            return;
        }
        
        ItemStack[] contents = inputChest.getBlockInventory().getContents();
        
        for (ItemStack content : contents) {
            if (content != null) {
                if (!content.getType().equals(Material.COBBLESTONE)) {
                    continue;
                }
                // HERE
                processCobblestone(content);
                break;
            }
        }
    }

    @Override
    public void onBuild(Civilization civ) {
        super.onBuild(civ);
        loadChests();
    }

    @Override
    public void onLoad(Civilization civ) {
        loadChests();
    }

    private void loadChests() {
        System.out.println("Loading sifter chests...");
        try {
            inputChest = (Chest) this.getBlocks().get(38).toBlock().getState();
            outputChest = (Chest) this.getBlocks().get(2).toBlock().getState();
        } catch (ClassCastException ex) {
            System.out.println("Failed");
        }
    }

    private void processCobblestone(ItemStack cobble) {
        cobble.setAmount(cobble.getAmount() - 1);
        double rn = ThreadLocalRandom.current().nextDouble(1.1) * 100.0;

        ItemStack reward = new ItemStack(Material.GRAVEL);
        
        if (rn <= 87) {
        } else if (rn <= 93) {
            reward.setType(Material.COAL);
        } else if (rn <= 97) {
            reward.setType(Material.GOLD_NUGGET);
        } else if (rn <= 99) {
            reward.setType(Material.IRON_INGOT);
        } else if (rn <= 100) {
            reward.setType(Material.DIAMOND);
        }
        outputChest.getBlockInventory().addItem(reward);
    }
}
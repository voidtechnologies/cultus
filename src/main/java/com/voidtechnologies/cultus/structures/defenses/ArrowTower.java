package com.voidtechnologies.cultus.structures.defenses;

import com.voidtechnologies.cultus.Cultus;
import com.voidtechnologies.cultus.civilizations.Civilization;
import com.voidtechnologies.cultus.runnables.ArrowTowerRunnable;
import com.voidtechnologies.cultus.structures.Position2d;
import com.voidtechnologies.cultus.structures.Position3d;
import com.voidtechnologies.cultus.structures.Structure;
import com.voidtechnologies.cultus.structures.StructureType;
import java.util.List;
import org.bukkit.Bukkit;
import org.bukkit.Location;

/**
 * @author Dylan Holmes
 * @date Jan 26, 2018
 */
public class ArrowTower extends Structure {

    private int taskId;

    public ArrowTower(Location loc, List<Position2d> chunks, List<Position3d> blocks) {
        super(StructureType.ARROW_TOWER, loc, chunks, blocks);
    }

    public ArrowTower(Location loc) {
        super(StructureType.ARROW_TOWER, loc);
    }

    @Override
    public void onBuild(Civilization civ) {
        setupShooter(civ);
    }

    private void setupShooter(Civilization civ) {
        System.out.println("Setting up shooter");
        ArrowTowerRunnable atr = new ArrowTowerRunnable(45, getLocation().clone().add(6, 16, 6), civ);
        taskId = Bukkit.getScheduler().scheduleSyncRepeatingTask(Cultus.getInstance(), atr, 40L, 25L);
    }

    @Override
    public void onDestroy(Civilization civ) {
        Bukkit.getScheduler().cancelTask(taskId);
        taskId = -1;
        super.onDestroy(civ);
    }

    @Override
    public void onLoad(Civilization civ) {
        setupShooter(civ);
    }
}

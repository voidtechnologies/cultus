package com.voidtechnologies.cultus.structures;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import org.bukkit.Bukkit;
import org.bukkit.Chunk;
import org.bukkit.block.Block;

/**
 * @author Dylan Holmes
 * @date Jan 14, 2018
 */
public class Position2d {

    private int x, z;
    private String world;

    public Position2d(String world, int x, int z) {
        this.x = x;
        this.z = z;
        this.world = world;
    }

    public String getWorld() {
        return world;
    }

    public void setWorld(String world) {
        this.world = world;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getZ() {
        return z;
    }

    public void setZ(int y) {
        this.z = y;
    }

    public static Position2d convertChunkToPosition(Chunk chunk) {
        return new Position2d(chunk.getWorld().getName(), chunk.getX(), chunk.getZ());
    }

    public static List<Position2d> convertBlocksToChunks(List<Block> pastedBlocks) {
        List<Position2d> ret = new ArrayList<>();

        for (Block block : pastedBlocks) {
            Position2d p2d = Position2d.convertChunkToPosition(block.getChunk());
            if (ret.contains(p2d)) {
                continue;
            }
            ret.add(p2d);
        }
        return ret;
    }

    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Position2d)) {
            return false;
        }
        Position2d pos = (Position2d) other;
        return pos.getX() == getX() && pos.getZ() == getZ();
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 53 * hash + this.x;
        hash = 53 * hash + this.z;
        return hash;
    }

    public double distance(Position2d other) {
        return Math.sqrt(Math.pow(other.getX() - getX(), 2) + Math.pow(other.getZ() - getZ(), 2));
    }

    public Chunk toChunk() {
        return Bukkit.getWorld(world).getChunkAt(x, z);
    }

    @Override
    public String toString() {
        return world + ";" + x + ";" + z;
    }

    public static Position2d fromString(String p2d) {
        String[] elements = p2d.split(";");
        return new Position2d(elements[0], Integer.valueOf(elements[1]), Integer.valueOf(elements[2]));
    }
}
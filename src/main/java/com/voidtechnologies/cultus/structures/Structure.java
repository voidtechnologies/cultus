package com.voidtechnologies.cultus.structures;

import com.voidtechnologies.cultus.civilizations.Civilization;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.bukkit.Location;
import org.bukkit.block.Block;

/**
 * @author Dylan Holmes
 * @date Jan 14, 2018
 */
public class Structure {

    protected List<Position2d> chunks = new ArrayList<>();
    protected List<Position3d> blocks = new ArrayList<>();
    protected Location location;
    protected StructureType type;

    public Structure(StructureType type, Location loc, List<Position2d> chunks, List<Position3d> blocks) {
        this.location = loc;
        this.blocks = blocks;
        this.type = type;
        this.chunks = chunks;
    }

    public Structure(StructureType type, Location loc) {
        this.location = loc;
        this.type = type;
        loadStructure();
    }

    public StructureType getType() {
        return this.type;
    }

    public List<Position2d> getChunks() {
        return chunks;
    }

    public void setChunks(List<Position2d> chunks) {
        this.chunks = chunks;
    }

    public List<Position3d> getBlocks() {
        return blocks;
    }

    public void setBlocks(List<Position3d> blocks) {
        this.blocks = blocks;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    protected void loadStructure() {
        List<Block> pastedBlocks = type.getSchematic().getPastedBlocks(location);
        setBlocks(Position3d.convertBlocksToPositions(pastedBlocks));
        setChunks(Position2d.convertBlocksToChunks(pastedBlocks));
    }

    public double upkeepTick(Civilization civ) {
        if (civ == null) {
            return 0.0;
        }
        double amount = type.getCost() * .025;
        civ.deductMoney(amount);
        return amount;
    }

    public void passiveTick(Civilization civ) {
    }

    public void onBuild(Civilization civ) {
    }

    public void onDestroy(Civilization civ) {
        civ.addMoney(type.getCost() * .70);
    }

    public void onLoad(Civilization civ) {
    }

    public static Structure createStructure(Location loc, Class c) {
        Structure struct = null;
        try {
            struct = (Structure) c.getConstructor(Location.class).newInstance(loc);
        } catch (IllegalAccessException | IllegalArgumentException | InstantiationException | NoSuchMethodException | SecurityException | InvocationTargetException ex) {
            Logger.getLogger(Structure.class.getName()).log(Level.SEVERE, null, ex);
        }
        return struct;
    }
}

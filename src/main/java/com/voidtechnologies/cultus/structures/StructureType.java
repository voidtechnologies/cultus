package com.voidtechnologies.cultus.structures;

import com.voidtechnologies.cultus.structures.camp.*;
import com.voidtechnologies.cultus.structures.civil.*;
import com.voidtechnologies.cultus.structures.defenses.*;
import com.voidtechnologies.cultus.technology.Technologies;
import com.voidtechnologies.cultusschematics.Schematic;

import java.util.ArrayList;
import java.util.List;
import org.bukkit.Material;

/**
 * @author Dylan Holmes
 * @date Jan 3, 2018
 */
public enum StructureType {

    // CAMP STRUCTURES
    GARDEN("Garden", "Turns seeds into wheat (2x expansion rate).", "camp/garden", Garden.class, new String[]{}, 500, Technologies.UNKNOWN, Material.WOODEN_HOE),
    SIFTER("Sifter", "Sifts through cobblestone to find rare minerals.", "camp/sifter", Sifter.class, new String[] {}, 900, Technologies.UNKNOWN, Material.AIR),
    CAMP("Camp", "", "camp/camp", CampStructure.class, new String[] {}, 0, Technologies.UNKNOWN, Material.AIR),
    // NATION STRUCTURES
    NATION("Nation", "", "civil/nation", NationStructure.class, new String[] {"civil/nation_supplement1"}, 0, Technologies.UNKNOWN, Material.AIR),
    //SMALL_FARM("Farm", "Turns seeds (seeds, carrots, potatoes)\ninto grown crops (3x expansion rate).", "civil/small_farm", "SmallFarm", 750, Technologies.POTTERY, Material.IRON_HOE),
    GRANARY("Granary", "Consumes food (bread, carrots, or cooked potatoes).", "civil/granary", Granary.class, new String[] {}, 1000, Technologies.THE_WHEEL, Material.BREAD),
    OBELISK("Obelisk", "Produces influence for your Nation on upkeep.", "civil/obelisk", Obelisk.class, new String[] {}, 1000, Technologies.POTTERY, Material.BEACON),
    TRADE_POST("Trading Post", "Harvests a Luxury Resource. Producing benefits for your nation.", "civil/obelisk", TradePost.class, new String[] {}, 1000, Technologies.CURRENCY, Material.BELL),
    ARROW_TOWER("Arrow Tower", "Shoots player's from other civilizations.", "defenses/arrow_tower", ArrowTower.class, new String[] {}, 2000, Technologies.ARCHERY, Material.ARROW),
    CANNON_TOWER("Cannon Tower", "Shoots player's from other civilizations.", "defenses/arrow_tower", CannonTower.class, new String[] {}, 2000, Technologies.ARCHERY, Material.ARROW),
    LAB("Laboratory", "Increases beakers.", "civil/lab", Lab.class, new String[] {}, 1500, Technologies.MATHEMATICS, Material.GLASS_BOTTLE),
    FORGE("Forge", "Increases hammers.", "civil/forge", Forge.class, new String[] {}, 1500, Technologies.CONSTRUCTION, Material.GLASS_BOTTLE),
    BANK("Bank", "Enables exchanging minerals for money", "civil/bank", Bank.class, new String[] {}, 2000, Technologies.CURRENCY, Material.GOLD_INGOT);

    public static final long BUILD_DELAY = 200;
    public static final double NATION_COST = 5;

    protected Schematic schematic;
    protected String name, desc;
    protected double cost;
    protected Material mat;
    protected Technologies requirement;
    protected String[] supplements;
    protected Class cls;
    
    private StructureType(String name, String desc, String schem, Class c, String[] supplements, double cost, Technologies requirement, Material mat) {
        this.name = name;
        this.cost = cost;
        this.desc = desc;
        this.mat = mat;
        this.supplements = supplements;
        this.requirement = requirement;
        this.cls = c;
        this.schematic = new Schematic(schem);
    }

    public Schematic getSchematic() {
        return schematic;
    }
    
    public Class getStructureClass() {
        return this.cls;
    }

    public String getName() {
        return name;
    }

    public double getCost() {
        return cost;
    }

    public String getDesc() {
        return desc;
    }

    public Material getMat() {
        return mat;
    }

    public Technologies getRequirement() {
        return requirement;
    }

    public static List<String> getParsableList(List<StructureType> structs) {
        List<String> retList = new ArrayList<>();
        structs.forEach((tech) -> {
            retList.add(tech.getName());
        });

        return retList;
    }

    @Override
    public String toString() {
        return getName();
    }

    public static StructureType fromName(String name) {
        for (StructureType tech : values()) {
            if (tech.getName().equalsIgnoreCase(name)) {
                return tech;
            }
        }
        return null;
    }

    public static List<StructureType> getStructuresFromList(List<String> structs) {
        List<StructureType> retList = new ArrayList<>();

        for (String tech : structs) {
            retList.add(fromName(tech));
        }

        return retList;
    }

    public String[] getSupplements() {
        return supplements;
    }
}
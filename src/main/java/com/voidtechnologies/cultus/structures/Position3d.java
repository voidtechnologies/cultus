package com.voidtechnologies.cultus.structures;

import java.util.LinkedList;
import java.util.List;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.block.Block;

/**
 * @author Dylan Holmes
 * @date Jan 14, 2018
 */
public class Position3d {

    private int x, y, z;
    private String world;

    public Position3d(String world, int x, int y, int z) {
        this.x = x;
        this.y = y;
        this.z = z;
        this.world = world;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public String getWorld() {
        return world;
    }

    public void setWorld(String world) {
        this.world = world;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getZ() {
        return z;
    }

    public void setZ(int z) {
        this.z = z;
    }

    public static Position3d convertBlockToPosition(Block block) {
        return new Position3d(block.getWorld().getName(), block.getX(), block.getY(), block.getZ());
    }

    public static LinkedList<Position3d> convertBlocksToPositions(List<Block> blocks) {
        LinkedList<Position3d> pos = new LinkedList<>();
        for (Block block : blocks) {
            pos.add(convertBlockToPosition(block));
        }
        return pos;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 41 * hash + this.x;
        hash = 41 * hash + this.y;
        hash = 41 * hash + this.z;
        return hash;
    }

    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Position3d)) {
            return false;
        }
        Position3d pos = (Position3d) other;
        return x == pos.getX() && y == pos.getY() && z == pos.getZ() && pos.getWorld().equals(world);
    }
    
    public Block toBlock() {
        return new Location(Bukkit.getWorld(world), x, y, z).getBlock();
    }
}

package com.voidtechnologies.cultus.structures;

import com.voidtechnologies.cultusschematics.Schematic;
import java.util.ArrayList;
import java.util.List;
import org.bukkit.Location;
import org.bukkit.block.Block;

/**
 *
 * @author Dylan Jul 30, 2019
 */
public class Constructure {

    private StructureType type;
    private double cHammers;
    private Location loc;
    private List<Position3d> blocks;
    private List<Position2d> chunks;
    
    public Constructure(StructureType type, Location loc) {
        this.type = type;
        this.loc = loc;
        List<Block> pasted = type.getSchematic().getPastedBlocks(loc);
        blocks = new ArrayList<>(Position3d.convertBlocksToPositions(pasted));
        chunks = new ArrayList<>(Position2d.convertBlocksToChunks(pasted));
        type.getSchematic().clearArea(loc, true);
        this.type = type;

    }

    public void updateProgress(double hammers) {
        this.cHammers += hammers;
    }

    public Structure build() {
        getSchematic().removeConstructionBoundaries(loc);
        getSchematic().paste(loc);
        for (String supplement : getStructureType().getSupplements()) {
            new Schematic(supplement).paste(loc);
        }
        return Structure.createStructure(getLocation(), getStructureType().getStructureClass());
    }

    public Schematic getSchematic() {
        return type.getSchematic();
    }
    
    public StructureType getStructureType() {
        return type;
    }

    public double getCurrentHammers() {
        return cHammers;
    }
    
    public double getRequiredHammers(){
        return type.getCost();
    }
    
    public Location getLocation() {
        return this.loc;
    }
    
    public List<Position2d> getChunks() {
        return this.chunks;
    }
    
    public List<Position3d> getBlocks() {
        return this.blocks;
    }
}

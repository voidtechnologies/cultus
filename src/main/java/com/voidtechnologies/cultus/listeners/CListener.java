package com.voidtechnologies.cultus.listeners;

import com.voidtechnologies.cultus.Cultus;
import com.voidtechnologies.cultus.Utility;
import com.voidtechnologies.cultus.commands.ui.Menu;
import com.voidtechnologies.cultus.managers.MenuManager;
import com.voidtechnologies.cultus.managers.PlayerManager;
import com.voidtechnologies.cultus.players.CPlayer;
import com.voidtechnologies.cultus.generator.LuxuryPopulator;
import java.util.HashMap;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.Sign;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.event.inventory.InventoryAction;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.world.WorldInitEvent;
import org.bukkit.plugin.PluginManager;

/**
 * @author Dylan Holmes
 * @date Jan 14, 2018
 */
public class CListener implements Listener {

    public static HashMap<Player, HashMap<Integer, String>> signLines = new HashMap<>();

    public CListener() {
        PluginManager pm = null;
        Cultus.getInstance().getServer().getPluginManager().registerEvents(this, Cultus.getInstance());
    }

//    @EventHandler
//    public void arrowHit(ProjectileHitEvent event) {
//        event.getEntity().getLocation().getWorld().createExplosion(event.getEntity().getLocation(), 1000F);
//    }
    @EventHandler(priority = EventPriority.NORMAL)
    public void playerJoin(PlayerJoinEvent event) {
        Player player = event.getPlayer();
        PlayerManager.getPlayerByHandle(player);
    }
    
    @EventHandler(priority = EventPriority.HIGHEST
    )
    public void onWorldInit(WorldInitEvent event) {
        if (event.getWorld().getName().equals("test_world")) {
            event.getWorld().getPopulators().add(new LuxuryPopulator());
        }
    }
    @EventHandler(priority = EventPriority.NORMAL)
    public void playerClickSign(PlayerInteractEvent event) {

        if (!event.getAction().equals(Action.RIGHT_CLICK_BLOCK)) {
            return;
        }
        System.out.println("Right clicked a block");
        Block block = event.getClickedBlock();
        if (!block.getType().equals(Material.OAK_SIGN) && !block.getType().equals(Material.OAK_WALL_SIGN)) {
            return;
        }
        System.out.println("Right clicked a sign");

        Sign sign = (Sign) block.getState();

        String id = sign.getLine(0);
        if (!id.startsWith(Utility.SIGN_ID) || !id.contains("[Bank]")) {
//            System.out.println(id + " --- " + Utility.SIGN_ID);
//            System.out.println("Right clicked a nonn-cultus sign");
//
//            HashMap<Integer, String> lines = signLines.get(event.getPlayer());
//
//            for (Entry<Integer, String> entry : lines.entrySet()) {
//                sign.setLine(entry.getKey(), entry.getValue());
//            }
//            sign.update();
            return;
        }

        String item = sign.getLine(1).substring(2, sign.getLine(1).length()).replace(" ", "");
        System.out.println(item);
        Material mat = null;
        double multiplier = 0;
        switch (item) {
            case "GOLD":
                mat = Material.GOLD_INGOT;
                multiplier = 15;
                break;
            case "IRON":
                mat = Material.IRON_INGOT;
                multiplier = 20;
                break;
            case "DIAMOND":
                mat = Material.DIAMOND;
                multiplier = 60;
                break;
        }
        int amt = 1;
        if (event.getPlayer().isSneaking()) {
            amt = 5;
        }
        System.out.println("Amt is " + amt);
        int removeMaterial = Utility.removeMaterial(event.getPlayer().getInventory(), mat, amt);
        System.out.println("Removed " + removeMaterial);
        if (removeMaterial <= 0) {
            return;
        }
        CPlayer cp = PlayerManager.getPlayerByHandle(event.getPlayer());

        cp.addMoney(multiplier * removeMaterial);
        cp.sendMessage(ChatColor.GREEN + "Received $" + (multiplier * removeMaterial) + " for depositing your mineral!");
    }
    
    @EventHandler
    public void onExplosiveArrowHit(ProjectileHitEvent event) {
        Projectile proj = event.getEntity();
        
        if (!(proj instanceof Arrow)) {
            return;
        }
        String name = proj.getCustomName();
        
        if (name == null || !name.equals("EXPLOSIVE_ARROW")) {
            return;
        }
        
        proj.getWorld().createExplosion(proj.getLocation(), 8, false, false);
    }
    
    @EventHandler
    public void handleMenuClick(InventoryClickEvent event) {
        Menu menu = MenuManager.getMenu(event.getInventory());
        if (menu == null) {
            return;
        }
        event.setCancelled(true);
        if (!event.getAction().equals(InventoryAction.PICKUP_ALL)) {
            return;
        }
        menu.processInvClick(event, event.getSlot());
    }

    @EventHandler
    public void handleMenuClose(InventoryCloseEvent event) {
        MenuManager.deleteMenu(event.getInventory());
    }
}

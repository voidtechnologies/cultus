package com.voidtechnologies.cultus.listeners.events;

import com.voidtechnologies.cultus.players.CPlayer;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import org.bukkit.event.entity.EntityDamageByEntityEvent;

/**
 *
 * @author Dylan Jul 11, 2019
 */
public class AttackedInTerritoryEvent extends Event implements Cancellable {

    private EntityDamageByEntityEvent event;
    private CPlayer player;
    private CPlayer attacker;
    private static final HandlerList handlers = new HandlerList();

    public AttackedInTerritoryEvent(EntityDamageByEntityEvent event, CPlayer player, CPlayer attacker) {
        this.event = event;
        this.player = player;
        this.attacker = attacker;
    }
    

    @Override
    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }

    @Override
    public void setCancelled(boolean bln) {
        event.setCancelled(true);
    }

    public EntityDamageByEntityEvent getParentEvent() {
        return event;
    }

    public CPlayer getPlayer() {
        return player;
    }

    public CPlayer getAttacker() {
        return attacker;
    }

    @Override
    public boolean isCancelled() {
        return event.isCancelled();
    }
}
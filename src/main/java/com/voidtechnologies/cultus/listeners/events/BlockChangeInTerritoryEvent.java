package com.voidtechnologies.cultus.listeners.events;

import com.voidtechnologies.cultus.civilizations.Civilization;
import com.voidtechnologies.cultus.players.CPlayer;
import com.voidtechnologies.cultus.structures.Position3d;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

/**
 *
 * @author Dylan Apr 22, 2019
 */
public class BlockChangeInTerritoryEvent extends Event implements Cancellable {

    private Event event;
    private EventType type;
    private Position3d block;
    private CPlayer player;
    private Civilization civ;
    private static final HandlerList handlers = new HandlerList();

    public BlockChangeInTerritoryEvent(Event base, EventType type, Position3d block, CPlayer player, Civilization civ) {
        this.event = base;
        this.type = type;
        this.civ = civ;
        this.player = player;
        this.block = block;
    }

    @Override
    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }

    public Civilization getCiv() {
        return civ;
    }

    public CPlayer getPlayer() {
        return player;
    }

    public Event getEvent() {
        return event;
    }

    public EventType getType() {
        return type;
    }

    public Position3d getBlock() {
        return block;
    }

    @Override
    public boolean isCancelled() {
        return ((Cancellable) event).isCancelled();
    }

    @Override
    public void setCancelled(boolean bln) {
        ((Cancellable) event).setCancelled(bln);
    }
}
package com.voidtechnologies.cultus.listeners.events;

import org.bukkit.event.Event;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.player.PlayerBucketEmptyEvent;
import org.bukkit.event.player.PlayerBucketFillEvent;
import org.bukkit.event.player.PlayerInteractEvent;

/**
 *
 * @author Dylan Apr 22, 2019
 */
public enum EventType {
    BREAK,
    INTERACT,
    BUCKET_FILL,
    BUCKET_EMPTY;

    public static EventType fromEvent(Event e) {
        if (e instanceof PlayerInteractEvent) {
            return INTERACT;
        } else if (e instanceof BlockBreakEvent) {
            return BREAK;
        } else if (e instanceof PlayerBucketFillEvent) {
            return BUCKET_FILL;
        } else if (e instanceof PlayerBucketEmptyEvent) {
            return BUCKET_EMPTY;
        }
        return null;
    }
}
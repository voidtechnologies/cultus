package com.voidtechnologies.cultus.listeners.events;

import com.voidtechnologies.cultus.civilizations.Civilization;
import com.voidtechnologies.cultus.players.CPlayer;
import com.voidtechnologies.cultus.structures.Structure;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import org.bukkit.event.block.BlockBreakEvent;

/**
 *
 * @author Dylan Apr 22, 2019
 */
public class StructureDamageEvent extends Event implements Cancellable {

    private BlockBreakEvent base;
    private CPlayer player;
    private Civilization civ;
    private Structure struct;
    private static final HandlerList handlers = new HandlerList();

    public StructureDamageEvent(BlockBreakEvent e, CPlayer player, Civilization civ, Structure struct) {
        this.base = e;
        this.player = player;
        this.civ = civ;
        this.struct = struct;
    }

    public BlockBreakEvent getBaseEvent() {
        return base;
    }

    public Structure getStructure() {
        return struct;
    }

    public CPlayer getPlayer() {
        return player;
    }

    public Civilization getCiv() {
        return civ;
    }

    @Override
    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }

    @Override
    public boolean isCancelled() {
        return base.isCancelled();
    }

    @Override
    public void setCancelled(boolean bln) {
        base.setCancelled(bln);
    }
}

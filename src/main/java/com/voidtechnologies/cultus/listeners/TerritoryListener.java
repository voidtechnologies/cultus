package com.voidtechnologies.cultus.listeners;

import com.mycompany.cultuswar.CultusWar;
import com.voidtechnologies.cultus.Cultus;
import com.voidtechnologies.cultus.civilizations.Civilization;
import com.voidtechnologies.cultus.civilizations.Relation;
import com.voidtechnologies.cultus.listeners.events.AttackedInTerritoryEvent;
import com.voidtechnologies.cultus.listeners.events.BlockChangeInTerritoryEvent;
import com.voidtechnologies.cultus.listeners.events.StructureDamageEvent;
import com.voidtechnologies.cultus.players.CPlayer;
import com.voidtechnologies.cultus.structures.Position2d;
import org.bukkit.ChatColor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

/**
 *
 * @author Dylan Apr 22, 2019
 */
public class TerritoryListener implements Listener {
    
    
    public TerritoryListener() {
        Cultus.getInstance().getServer().getPluginManager().registerEvents(this, Cultus.getInstance());
    }
    @EventHandler
    public void blockChange(BlockChangeInTerritoryEvent event) {
        Civilization civ = event.getCiv();
        CPlayer player = event.getPlayer();

        if (civ.containsMember(player.getUUID())) {
            return;
        }

        event.setCancelled(true);
        player.sendMessage(ChatColor.RED + "You do not have permission to do this in another Civilization's territory!");
    }

    @EventHandler
    public void structureDamaged(StructureDamageEvent event) {
        Civilization civ = event.getCiv();
        CPlayer player = event.getPlayer();

        if (civ.containsMember(player.getUUID())) {
            System.out.println("Contains member, cancelling");
            event.setCancelled(true);
            return;
        }

        if (!CultusWar.isWar() || !civ.getRelationship(player.getCiv()).equals(Relation.HOSTILE)) {
            event.setCancelled(true);
            player.sendMessage(ChatColor.RED + "You can only damage other Civ's structure if you have a hostile relationship and it is war time!");
            return;
        }
        
        // if it gets this far, it IS war time, AND they are a hostile civilization.
        // So, have the structure take damage @TODO
    }

    @EventHandler
    public void playerAttackedInTerritory(AttackedInTerritoryEvent event) {
        CPlayer player = event.getPlayer();
        CPlayer attacker = event.getAttacker();

        Civilization dCiv = player.getCiv();
        Civilization aCiv = attacker.getCiv();
        // No Civ defender has no rights
        if (dCiv == null) {
            return;
        }
        // Can't attack fellow civ members
        if (dCiv == aCiv) {
            attacker.sendMessage(ChatColor.RED + "You cannot attack members within your own Civilization!");
            event.setCancelled(true);
            return;
        }

        Position2d pos = Position2d.convertChunkToPosition(player.getHandle().getLocation().getChunk());
        Relation relation = dCiv.getRelationship(aCiv);
        
        // Can't attack allies
        if (relation.equals(Relation.ALLY)) {
            attacker.sendMessage(ChatColor.RED + "You cannot attack allies of your Civilization!");
            event.setCancelled(true);
            return;
        }

        // Defender is on their own Soil AND the attacker is not declared hostile to the defender
        if (dCiv.getTerritories().contains(pos) && !relation.equals(Relation.HOSTILE)) {
            attacker.sendMessage(ChatColor.RED + "You cannot attack another player within their own borders, without declaring a hostile relationship first!");
            event.setCancelled(true);
        }

        // At this point, the defender, was not a member of the same civ, not an ally, not within their own terrirtory or they were and declared hostile
        // So, they take damage as normal
        //@TODO/@FUTURE Damage reduction, Damage increase locations
        //I.E. Research the "Great Walls", Civ members now take 15% reduced damage within their own territory... etc
    }
}

package com.voidtechnologies.cultus.listeners;

import com.voidtechnologies.cultus.Cultus;
import com.voidtechnologies.cultus.civilizations.Civilization;
import com.voidtechnologies.cultus.civilizations.Nation;
import com.voidtechnologies.cultus.listeners.events.AttackedInTerritoryEvent;
import com.voidtechnologies.cultus.listeners.events.BlockChangeInTerritoryEvent;
import com.voidtechnologies.cultus.listeners.events.EventType;
import com.voidtechnologies.cultus.listeners.events.StructureDamageEvent;
import com.voidtechnologies.cultus.managers.CivilizationManager;
import com.voidtechnologies.cultus.managers.PlayerManager;
import com.voidtechnologies.cultus.players.CPlayer;
import com.voidtechnologies.cultus.structures.Position2d;
import com.voidtechnologies.cultus.structures.Position3d;
import com.voidtechnologies.cultus.structures.Structure;
import java.util.ArrayList;
import java.util.List;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Chunk;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockDamageEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.player.PlayerBucketEmptyEvent;
import org.bukkit.event.player.PlayerBucketFillEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerMoveEvent;

/**
 * @author Dylan Holmes
 * @date Feb 8, 2018
 */
public class CivListener implements Listener {

    public static List<Player> removeStructure = new ArrayList<>();

    public CivListener() {
        Cultus.getInstance().getServer().getPluginManager().registerEvents(this, Cultus.getInstance());
    }

    @EventHandler(priority = EventPriority.LOW)
    public void civMoveEvent(PlayerMoveEvent event) {
        Location from = event.getFrom();
        Location to = event.getTo();
        if (from == null || to == null) {
            return;
        }

        if (from.getChunk().equals(to.getChunk())) {
            return;
        }
        Civilization fromCiv = CivilizationManager.getLocationOwner(from), toCiv = CivilizationManager.getLocationOwner(to);

        if ((fromCiv == null && toCiv == null)) {
            return;
        }

        if (fromCiv != toCiv) {
            if (fromCiv != null) {
                event.getPlayer().sendMessage(ChatColor.YELLOW + "[" + ChatColor.GREEN
                        + fromCiv.getTag() + ChatColor.YELLOW + "]" + ChatColor.GRAY
                        + " Leaving " + fromCiv.getCivName() + "'s territory!");
            }
            if (toCiv != null) {
                event.getPlayer().sendMessage(ChatColor.YELLOW
                        + "[" + ChatColor.GREEN + toCiv.getTag() + ChatColor.YELLOW + "]"
                        + ChatColor.GRAY + " Entering " + toCiv.getCivName() + "'s territory!");

            } else if (toCiv == null) {
                event.getPlayer().sendMessage(ChatColor.DARK_GREEN + "Entering Wilderness!");
            }
        }
    }

    @EventHandler(priority = EventPriority.LOW, ignoreCancelled = true)
    public void onBlockBreakInCiv(BlockBreakEvent event) {
        Block block = event.getBlock();
        Chunk chunk = block.getChunk();
        CPlayer player = PlayerManager.getPlayerByHandle(event.getPlayer());

        Civilization civ = CivilizationManager.getLocationOwner(block.getLocation());
        if (civ == null) {
            return;
        }
        
        BlockChangeInTerritoryEvent e = new BlockChangeInTerritoryEvent(event, EventType.fromEvent(event), Position3d.convertBlockToPosition(block), player, civ);
        callEvent(e);

        if (e.isCancelled()) {
            return;
        }
        for (Structure struct : civ.getStructures()) {

            if (struct.getChunks().contains(Position2d.convertChunkToPosition(chunk)) && struct.getBlocks().contains(Position3d.convertBlockToPosition(block))) {
                StructureDamageEvent sEvent = new StructureDamageEvent(event, player, civ, struct);
                callEvent(sEvent);
                
                if (sEvent.isCancelled()) {
                    event.setCancelled(true);
                }
                return;
            }
        }
    }

    @EventHandler(priority = EventPriority.LOW, ignoreCancelled = true)
    public void onBucketEmptyInCiv(PlayerBucketEmptyEvent event) {
        CPlayer player = PlayerManager.getPlayerByHandle(event.getPlayer());
        Civilization civ = CivilizationManager.getLocationOwner(event.getBlockClicked().getLocation());
        if (civ == null) {
            return;
        }

        BlockChangeInTerritoryEvent e = new BlockChangeInTerritoryEvent(event, EventType.fromEvent(event), Position3d.convertBlockToPosition(event.getBlockClicked()), player, civ);
        callEvent(e);
    }

    @EventHandler(priority = EventPriority.LOW, ignoreCancelled = true)
    public void onBucketFillInCiv(PlayerBucketFillEvent event) {
        CPlayer player = PlayerManager.getPlayerByHandle(event.getPlayer());
        Civilization civ = CivilizationManager.getLocationOwner(event.getBlockClicked().getLocation());
        if (civ == null) {
            return;
        }

        BlockChangeInTerritoryEvent e = new BlockChangeInTerritoryEvent(event, EventType.fromEvent(event), Position3d.convertBlockToPosition(event.getBlockClicked()), player, civ);
        callEvent(e);
    }

    @EventHandler(priority = EventPriority.LOW, ignoreCancelled = true)
    public void onBlockInteractInCiv(PlayerInteractEvent event) {
        Block block = event.getClickedBlock();
        Material type = block.getType();
        if (!type.isInteractable()) {
            return;
        }

        Chunk chunk = block.getChunk();
        CPlayer player = PlayerManager.getPlayerByHandle(event.getPlayer());
        Civilization civ = CivilizationManager.getLocationOwner(block.getLocation());
        if (civ == null) {
            return;
        }

        BlockChangeInTerritoryEvent e = new BlockChangeInTerritoryEvent(event, EventType.fromEvent(event), Position3d.convertBlockToPosition(block), player, civ);
        callEvent(e);
    }

    @EventHandler(priority = EventPriority.LOW, ignoreCancelled = true)
    public void onPvPInCiv(EntityDamageEvent event) {
        if (!(event instanceof EntityDamageByEntityEvent)) {
            return;
        }
        EntityDamageByEntityEvent e = (EntityDamageByEntityEvent) event;
        if (!(e.getDamager() instanceof Player) || !(e.getEntity() instanceof Player)) {
            return;
        }
        CPlayer attacker = PlayerManager.getPlayerByUUID(e.getDamager().getUniqueId());
        CPlayer defender = PlayerManager.getPlayerByUUID(e.getEntity().getUniqueId());

        AttackedInTerritoryEvent aitEvent = new AttackedInTerritoryEvent(e, defender, attacker);
        callEvent(aitEvent);
    }

    @EventHandler(priority = EventPriority.LOW, ignoreCancelled = true)
    public void onBlockPlaceInCiv(BlockPlaceEvent event) {
        Block block = event.getBlock();
        CPlayer player = PlayerManager.getPlayerByHandle(event.getPlayer());
        Civilization civ = CivilizationManager.getLocationOwner(block.getLocation());
        if (civ == null) {
            return;
        }
        BlockChangeInTerritoryEvent e = new BlockChangeInTerritoryEvent(event, EventType.fromEvent(event), Position3d.convertBlockToPosition(block), player, civ);
        callEvent(e);
    }

    @EventHandler
    public void playerHitStructure(BlockDamageEvent event) {
        Block block = event.getBlock();
        Chunk chunk = block.getChunk();
        Player player = event.getPlayer();
        CPlayer cp = PlayerManager.getPlayerByHandle(player);
        if (!removeStructure.contains(player)) {
            return;
        }

        Nation civ = (Nation) PlayerManager.getPlayerByHandle(player).getCiv();
        if (!cp.hasPermission("cultus.nation.building.destroy")) {
            player.sendMessage(ChatColor.RED + "You do not have permission to destroy building in this Nation!");
            return;
        }
        for (Structure struct : civ.getStructures()) {
            if (struct.getChunks().contains(Position2d.convertChunkToPosition(chunk)) && struct.getBlocks().contains(Position3d.convertBlockToPosition(block))) {
                event.setCancelled(true);
                player.sendMessage(ChatColor.RED + "Removing " + struct.getType().getName() + "!");
                civ.sendMessageToMembers(player.getName() + " removed a " + struct.getType().getName() + "!");
                struct.onDestroy(civ);
                civ.removeStructure(struct);
                removeStructure.remove(player);
                return;
            }
        }
    }

    private void callEvent(Event e) {
        Bukkit.getPluginManager().callEvent(e);
    }
}
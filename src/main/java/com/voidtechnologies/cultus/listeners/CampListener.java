package com.voidtechnologies.cultus.listeners;

import com.voidtechnologies.cultus.Cultus;
import com.voidtechnologies.cultus.Utility;
import com.voidtechnologies.cultus.civilizations.Camp;
import com.voidtechnologies.cultus.civilizations.Civilization;
import com.voidtechnologies.cultus.civilizations.Nation;
import com.voidtechnologies.cultus.civilizations.NationType;
import com.voidtechnologies.cultus.commands.ui.Menu;
import com.voidtechnologies.cultus.commands.ui.menus.ManageCampMenu;
import com.voidtechnologies.cultus.commands.ui.menus.ManageCivMenu;
import com.voidtechnologies.cultus.managers.CivilizationManager;
import com.voidtechnologies.cultus.managers.PartyManager;
import com.voidtechnologies.cultus.managers.PlayerManager;
import com.voidtechnologies.cultus.players.CPlayer;
import com.voidtechnologies.cultus.players.Rank;
import com.voidtechnologies.cultus.players.Party;
import com.voidtechnologies.cultus.structures.Position2d;
import com.voidtechnologies.cultus.structures.StructureType;
import com.voidtechnologies.cutlusmaterials.items.CultusItem;
import com.voidtechnologies.cutlusmaterials.managers.ItemManager;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Chunk;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.Sign;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.player.PlayerChatEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

/**
 * @author Dylan Holmes
 * @date Feb 8, 2018
 */
public class CampListener implements Listener {

    private HashMap<Player, String> promptMap = new HashMap<>();
    private HashMap<Player, LinkedList<String>> nationMap = new HashMap<>();
    private HashMap<Player, Position2d> natPlacedMap = new HashMap<>();
    
    public CampListener() {
        Cultus.getInstance().getServer().getPluginManager().registerEvents(this, Cultus.getInstance());
    }

    @EventHandler(priority = EventPriority.HIGH, ignoreCancelled = true)
    public void createCamp(BlockPlaceEvent event) {
        ItemStack is = event.getItemInHand();
        if (!is.getType().equals(Material.IRON_DOOR)) {
            return;
        }
        CultusItem ci = CultusItem.getCultusItem(is);
        if (ci == null) {
            return;
        }
        if (!ci.getName().contains("Camp")) {
            return;
        }
        Player player = event.getPlayer();
        CPlayer cPlayer = PlayerManager.getPlayerByHandle(player);

        event.setCancelled(true);
        String msg = checkValidCampArea(Position2d.convertChunkToPosition(player.getLocation().getChunk()));
        if (!msg.equals("")) {
            player.sendMessage(msg);
            return;
        }
        msg = checkValidCampParty(cPlayer);
        if (!msg.equals("")) {
            player.sendMessage(msg);
            return;
        }
        Bukkit.getScheduler().scheduleSyncDelayedTask(Cultus.getInstance(), () -> {
            is.setAmount(is.getAmount() - 1);
            ItemStack campMang = new ItemStack(Material.BOOK, 1);
            ItemManager.setCultusItem(campMang, null, ChatColor.GOLD + "Manage Camp", true, 0, true);
            player.getInventory().addItem(campMang);
            player.updateInventory();
            Party party = PartyManager.getPartyFromPlayer(cPlayer);
            Chunk pChunk = player.getLocation().getChunk();
            Camp camp = new Camp("Camp " + party.getName(), party.getName().substring(0, 3), player.getLocation().getWorld().getChunkAt(pChunk.getX() + 1, pChunk.getZ() + 1), party, new ArrayList<>(), new ArrayList<>(), new HashMap<>());
            camp.setupCamp(1, player.getLocation().getBlockY());
            party.getMembers().forEach((id) -> {
                PlayerManager.getPlayerByUUID(id).setCiv(camp);
            });
            CivilizationManager.addCivilization(camp);
        });
    }

    private String checkValidCampArea(Position2d center) {
        for (Civilization civ : CivilizationManager.nations()) {
            if (civ.getCenter().distance(center) <= 6) {
                return ChatColor.RED + "You cannot build a camp within 5 chunks of another Civilization!";
            }
        }

        for (Camp camp : CivilizationManager.camps()) {
            if (camp.getCenter().distance(center) <= 5) {
                return ChatColor.RED + "You cannot build a camp within 4 chunks of another Camp!";
            }
        }
        return "";
    }

    private String checkValidCampParty(CPlayer player) {
        Party pp = PartyManager.getPartyFromPlayer(player);
        if (pp == null) {
            return ChatColor.RED + "You must be in a party to create a camp! type /party create <name> to create one!";
        }
        for (Camp camp : CivilizationManager.camps()) {
            if (camp.getParty() == pp) {
                return ChatColor.RED + "Your party already owns a camp!";
            }
        }
        return "";
    }

    @EventHandler(priority = EventPriority.LOW)
    public void openCampManage(PlayerInteractEvent event) {
        ItemStack is = event.getItem();
        if (!event.getAction().equals(Action.RIGHT_CLICK_AIR)) {
            return;
        }
        if (!is.getType().equals(Material.BOOK)) {
            return;
        }

        CultusItem ci = CultusItem.getCultusItem(is);
        if (ci == null) {
            return;
        }

        if (!ci.getName().contains("Manage Camp") && !ci.getName().contains("Manage Nation")) {
            return;
        }

        CPlayer player = PlayerManager.getPlayerByHandle(event.getPlayer());
        Menu menu = null;
        if (player.getCiv() instanceof Camp) {
            menu = new ManageCampMenu((Camp) player.getCiv(), null);
        } else if (player.getCiv() instanceof Nation) {
            menu = new ManageCivMenu((Nation) player.getCiv(), null);
        } else {
            return;
        }

        player.displayMenu(menu);
    }

    @EventHandler(priority = EventPriority.NORMAL)
    public void onCampSignClick(PlayerInteractEvent event) {
        CPlayer player = PlayerManager.getPlayerByHandle(event.getPlayer());

        if (!event.getAction().equals(Action.RIGHT_CLICK_BLOCK)) {
            return;
        }
        Block block = event.getClickedBlock();
        if (!block.getType().equals(Material.OAK_SIGN) && !block.getType().equals(Material.OAK_WALL_SIGN)) {
            return;
        }
        Sign sign = (Sign) block.getState();
        String id = sign.getLine(0);

        if (!id.startsWith(Utility.SIGN_ID) || !id.contains("[Camp]")) {
            return;
        }

        System.out.println("Clicked a camp sign");

        Location signLoc = sign.getLocation();
        Civilization owner = CivilizationManager.getLocationOwner(signLoc);

        if (owner == null || !(owner instanceof Camp)) {
            System.out.println("Owner is null");
            return;
        }

        Camp camp = (Camp) owner;
        String action = sign.getLine(1);

        switch (action) {
            case "UPGRADE":
                Rank rank = camp.getRankOfMember(player.getUUID());
                if (rank == null || rank.getValue() < 1) {
                    player.sendMessage(ChatColor.RED + "You do not have permission to upgrade this camp!");
                    return;
                }
                StructureType struct = StructureType.valueOf(sign.getLine(2));
                if (camp.getMoney() < struct.getCost()) {
                    player.sendMessage(ChatColor.RED + "Your camp cannot afford this upgrade! It costs " + struct.getCost() + " (" + camp.getMoney() + ").");
                    return;
                }
                camp.unlockUpgrade(player, struct, event.getClickedBlock().getLocation());

                break;
        }
    }

    @EventHandler()
    public void onNationBlockPlaced(BlockPlaceEvent event) {
        ItemStack is = event.getItemInHand();
        if (!is.getType().equals(Material.BEDROCK)) {
            return;
        }
        CultusItem ci = CultusItem.getCultusItem(is);
        if (ci == null) {
            return;
        }
        if (!ci.getName().contains("Nation")) {
            return;
        }
        Player player = event.getPlayer();
        CPlayer cPlayer = PlayerManager.getPlayerByHandle(player);

        event.setCancelled(true);

        Party party = PartyManager.getPartyFromPlayer(cPlayer);
        Civilization civ = cPlayer.getCiv();

        if (civ == null || !(civ instanceof Camp)) {
            player.sendMessage(ChatColor.RED + "You must own a camp to create a Nation!");
            return;
        }
        if (party == null) {
            player.sendMessage(ChatColor.RED + "You need to be in a party to create a Nation!");
            return;
        }

        Camp camp = (Camp) civ;

        if (!camp.albeToUpgradeToNation(player)) {
            return;
        }
        player.sendMessage(ChatColor.YELLOW + "Type a name for your Nation! The name must only contain letters.");
        player.sendMessage(ChatColor.YELLOW + "Type cancel to cancel the creation!");
        natPlacedMap.put(player, Position2d.convertChunkToPosition(event.getBlock().getChunk()));
        promptMap.put(player, "NAT_NAME");
    }

    @EventHandler()
    public void playerChatEvent(PlayerChatEvent event) {
        Player player = event.getPlayer();
        CPlayer cPlayer = PlayerManager.getPlayerByHandle(player);

        String prompt = promptMap.get(player);
        if (prompt == null) {
            return;
        }
        event.setCancelled(true);
        LinkedList<String> list = nationMap.get(player);
        if (list == null) {
            list = new LinkedList<>();
        }
        String item = event.getMessage().split(" ")[0];
        Nation nation;
        if (item.equalsIgnoreCase("cancel")) {
            player.sendMessage(ChatColor.RED + "Cancelling nation creation!");
            promptMap.put(player, null);
            nationMap.put(player, null);
            natPlacedMap.put(player, null);
            return;
        }
        switch (prompt) {
            case "NAT_NAME":
                if (!item.matches("^[a-zA-Z]+$")) {
                    player.sendMessage(ChatColor.RED + "A Nation's name must only contain letters (no spaces, special chars, or symbols)!");
                    return;
                }
                nation = (Nation) CivilizationManager.getCivByName(item);

                if (nation != null) {
                    player.sendMessage(ChatColor.RED + "A Civilization already has this name!");
                    return;
                }
                list.add(0, item);

                nationMap.put(player, list);
                promptMap.put(player, "NAT_TAG");
                player.sendMessage(ChatColor.YELLOW + "Enter a Nation Tag (MAXIMUM 3 letters): ");
                break;
            case "NAT_TAG":
                if (item.equalsIgnoreCase("cancel")) {
                    player.sendMessage(ChatColor.RED + "Cancelling nation creation!");
                    promptMap.put(player, null);
                    return;
                }
                if (!item.matches("^[a-zA-Z]+$")) {
                    player.sendMessage(ChatColor.RED + "A Nation's tag must only contain letters (no spaces, special chars, or symbols)!");
                    return;
                }

                nation = CivilizationManager.getNationByTag(item);
                if (nation != null) {
                    player.sendMessage(ChatColor.RED + "A nation with that tag already exists, a tag mnust be unique!");
                    return;
                }
                list.add(1, item);
                nationMap.put(player, list);
                promptMap.put(player, "NAT_CONFIRM");
                player.sendMessage(ChatColor.YELLOW + "Your nation's name will be [" + list.get(1) + "] " + list.get(0));
                player.sendMessage(ChatColor.YELLOW + "Type \"yes\" to continue");
                break;
            case "NAT_CONFIRM":
                if (!item.equalsIgnoreCase("yes")) {
                    player.sendMessage(ChatColor.RED + "Nation creation cancelled!");
                    promptMap.put(player, null);
                    nationMap.put(player, null);
                    natPlacedMap.put(player, null);
                    return;
                }
                LinkedList<String> info = nationMap.get(player);
                CivilizationManager.removeCivilization(cPlayer.getCiv());

                Party party = cPlayer.getParty();
                Set<UUID> members = party.getMembers();
                List<UUID> memberList = new ArrayList<>();

                members.forEach((id) -> {
                    memberList.add(id);
                    CPlayer pID = PlayerManager.getPlayerByUUID(id);
                    pID.setParty(null);
                });
                System.out.println(memberList);
                System.out.println("Creating new nation...");
                nation = new Nation(info.get(0), info.get(1),
                        natPlacedMap.get(player).toChunk(), memberList,
                        new ArrayList<>(), new ArrayList<>(), new ArrayList<>(), (short) 0, party.getMoney() - 5000, 2, NationType.HAMLET, new HashMap<>());
                nation.claimLand(nation.getNationType().getLandRadius());

                nation.setup(player.getLocation().getBlockY());
                cPlayer.addPermission("cultus.nation.owner");

                System.out.println(cPlayer.getUUID().toString());
                cPlayer.setCiv(nation);
                PartyManager.removePartyByReference(party);
                
                CivilizationManager.addCivilization(nation);
                cPlayer.sendMessage(ChatColor.GREEN + "You created a nation!");
                promptMap.put(player, null);
                nationMap.put(player, null);
                natPlacedMap.put(player, null);
                
                
                ItemStack civMang = new ItemStack(Material.BOOK, 1);
                ItemManager.setCultusItem(civMang, null, ChatColor.GOLD + "Manage Nation", true, 0, true);
                break;
        }
    }
}
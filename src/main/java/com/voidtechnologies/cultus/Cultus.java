package com.voidtechnologies.cultus;

import com.voidtechnologies.cultus.civilizations.Nation;
import com.voidtechnologies.cultus.commands.BugReportCommand;
import com.voidtechnologies.cultus.commands.NationCommand;
import com.voidtechnologies.cultus.commands.CultusCommand;
import com.voidtechnologies.cultus.commands.NationAcceptCommand;
import com.voidtechnologies.cultus.commands.PartyCommand;
import com.voidtechnologies.cultus.commands.PlayerMoneyCommand;
import com.voidtechnologies.cultus.listeners.CListener;
import com.voidtechnologies.cultus.listeners.CampListener;
import com.voidtechnologies.cultus.listeners.CivListener;
import com.voidtechnologies.cultus.listeners.TerritoryListener;
import com.voidtechnologies.cultus.managers.CivilizationManager;
import com.voidtechnologies.cultus.managers.PartyManager;
import com.voidtechnologies.cultus.players.CPlayer;
import com.voidtechnologies.cultus.managers.PlayerManager;
import com.voidtechnologies.cultus.recipes.RecipeLoader;
import com.voidtechnologies.cultus.runnables.PassiveTickRunnable;
import com.voidtechnologies.cultus.runnables.UpkeepTickRunnable;
import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.WorldCreator;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * @author Dylan Holmes
 * @date Jan 3, 2018
 */
public class Cultus extends JavaPlugin {

    private static Cultus instance;
    private static World world;

    /**
     * Called when the plugin is enabled
     */
    @Override
    public void onEnable() {
        instance = this;

        // Have all static instances load their data into memory
        CivilizationManager.loadCivilizations();
        PlayerManager.loadConfig();
        PartyManager.loadParties();

        RecipeLoader.loadRecipes();

        // Create and register all commands and 
        new PartyCommand();
        new NationCommand();
        new BugReportCommand();
        new PlayerMoneyCommand();
        new NationAcceptCommand();

        CultusCommand.registerAllCommands();

        // Create and register all event listeners
        new CListener();
        new CivListener();
        new CampListener();
        new TerritoryListener();

        // Debug/test command, temporary inclusion 
        getCommand("test").setExecutor(new CommandExecutor() {
            @Override
            public boolean onCommand(CommandSender cs, Command cmnd, String string, String[] strings) {
                Player player = (Player) cs;

                player.teleport(world.getSpawnLocation());

                return true;
            }
        });

        // Debug/test command, temporary inclusion 
        getCommand("cinfo").setExecutor((CommandSender cs, Command cmnd, String string, String[] strings) -> {
            Player player = (Player) cs;

            CPlayer cPlayer = PlayerManager.addPlayerFromHandle(player);
            Nation nat = (Nation) CivilizationManager.getCivByName(strings[0]);

            player.sendMessage("------- " + nat.getCivName() + " -------");
            player.sendMessage("Members: " + nat.getAmountOfOnlineMembers() + "/" + nat.getAmountOfMembers());
            player.sendMessage("Member of?: " + nat.containsMember(cPlayer.getUUID()));
            player.sendMessage("build: " + player.hasPermission("cultus.nation.building.build"));
            player.sendMessage("research: " + player.hasPermission("cultus.nation.research.start"));
            player.sendMessage("general: " + player.hasPermission("cultus.nation"));
            player.sendMessage("all: " + player.hasPermission("cultus.nation.*"));

            return true;
        });

        // Schedule our Passive and Upkeep tasks to run syncd
        Bukkit.getScheduler().scheduleSyncRepeatingTask(this, new PassiveTickRunnable(), 400L, 30L);
        Bukkit.getScheduler().scheduleSyncRepeatingTask(this, new UpkeepTickRunnable(), 200L, 120L);
        WorldCreator creator = new WorldCreator("test_world");
        creator.copy(Bukkit.getWorld("cultus-alpha-world"));

        creator.createWorld();
        world = creator.createWorld();
    }

    /**
     * Called when the plugin is disabled
     */
    @Override
    public void onDisable() {
        // Save all memory related information
        CivilizationManager.saveCivilizations();
        PlayerManager.savePlayers();
        PartyManager.saveParties();
    }

    /**
     * Gets the Cultus plugin instance
     *
     * @return an instance to the Cultus plugin
     */
    public static Cultus getInstance() {
        return instance;
    }
}

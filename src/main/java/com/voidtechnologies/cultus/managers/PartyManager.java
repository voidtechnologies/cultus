package com.voidtechnologies.cultus.managers;

import com.voidtechnologies.cultus.Cultus;
import com.voidtechnologies.cultus.players.CPlayer;
import com.voidtechnologies.cultus.players.Party;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.YamlConfiguration;

/**
 * @author Dylan Holmes
 * @date Jan 28, 2018
 */
public class PartyManager {

    private static final HashMap<String, Party> PARTIES = new HashMap<>();
    private static final HashMap<UUID, List<String>> INVITES = new HashMap<>();
    private static YamlConfiguration partyConfig;

    public static Party getParty(String key) {
        return PARTIES.get(key.toLowerCase());
    }

    public static boolean containsPartyName(String key) {
        return PARTIES.containsKey(key.toLowerCase());
    }

    public static Party addParty(Party value) {
        return PARTIES.put(value.getName().toLowerCase(), value);
    }

    public static Party removePartyByName(String key) {
        return PARTIES.remove(key.toLowerCase());
    }

    public static Party removePartyByReference(Party key) {
        partyConfig.set("parties." + key.getName(), null);
        try {
            partyConfig.save(new File(Cultus.getInstance().getDataFolder().getAbsolutePath() + "\\players\\parties.yml"));
        } catch (IOException ex) {
            Logger.getLogger(PartyManager.class.getName()).log(Level.SEVERE, null, ex);
        }

        return removePartyByName(key.getName());
    }

    public static void clear() {
        PARTIES.clear();
    }

    public static boolean containsParty(Party value) {
        return PARTIES.containsValue(value);
    }

    public static Collection<Party> parties() {
        return PARTIES.values();
    }

    public static Party getPartyFromPlayer(CPlayer player) {
        for (Party party : parties()) {
            if (party.isPlayerInParty(player)) {
                return party;
            }
        }
        return null;
    }

    public static void addInvite(UUID id, String party) {
        List<String> pInvites = INVITES.get(id);
        if (pInvites == null) {
            pInvites = new ArrayList<>();
        }
        pInvites.add(party.toLowerCase());
        INVITES.put(id, pInvites);
    }

    public static Party getInvite(UUID id, String party) {
        List<String> pInvites = INVITES.get(id);
        if (pInvites == null) {
            pInvites = new ArrayList<>();
            INVITES.put(id, pInvites);
            return null;
        }
        if (!pInvites.contains(party.toLowerCase())) {
            return null;
        }
        return PartyManager.getParty(party);
    }

    public static void removeInvite(UUID id) {
        INVITES.remove(id);
    }

    public static void saveParties() {
        for (Party party : parties()) {
            String path = "parties." + party.getName() + ".";
            partyConfig.set(path + "players", party.convertMemberMapToString());
            partyConfig.set(path + "money", party.getMoney());
        }

        try {
            partyConfig.save(new File(Cultus.getInstance().getDataFolder().getAbsolutePath() + "\\players\\parties.yml"));
        } catch (IOException ex) {
            Logger.getLogger(PartyManager.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void loadParties() {
        loadConfig();
        ConfigurationSection cs = partyConfig.getConfigurationSection("parties");

        if (cs == null) {
            return;
        }

        for (String key : cs.getKeys(false)) {
            String path = "parties." + key + ".";
            Party pp = new Party(key);

            double money = partyConfig.getDouble(path + "money");
            String players = partyConfig.getString(path + "players");

            pp.setMoney(money);
            pp.addMembersStringToMap(players);
            PARTIES.put(key.toLowerCase(), pp);
        }
    }

    public static void loadConfig() {
        File file = new File(Cultus.getInstance().getDataFolder().getAbsolutePath() + "\\players\\parties.yml");

        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException ex) {
                Logger.getLogger(PartyManager.class.getName()).log(Level.SEVERE, null, ex);
                return;
            }
        }
        partyConfig = YamlConfiguration.loadConfiguration(file);
    }
}

package com.voidtechnologies.cultus.managers;

import com.voidtechnologies.cultus.commands.ui.Menu;
import java.util.HashMap;
import org.bukkit.inventory.Inventory;

/**
 * @author Dylan Holmes 
 * @date Jan 19, 2018
 */
public class MenuManager {

    private static HashMap<Inventory, Menu> menus = new HashMap<>();
    
    public static void registerMenu(Inventory inv, Menu menu) {
        menus.put(inv, menu);
    }
    
    public static Menu getMenu(Inventory inv) {
        return menus.get(inv);
    }

    public static void deleteMenu(Inventory inventory) {
        if (!menus.containsKey(inventory)) {
            return;
        }
        menus.remove(inventory);
    }
}
package com.voidtechnologies.cultus.managers;

import com.voidtechnologies.cultus.Cultus;
import com.voidtechnologies.cultus.civilizations.Civilization;
import com.voidtechnologies.cultus.players.CPlayer;
import com.voidtechnologies.cultus.players.Party;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

/**
 * @author Dylan Holmes
 * @date Jan 5, 2018
 */
public class PlayerManager {

    private static HashMap<UUID, CPlayer> players = new HashMap<>();
    private static YamlConfiguration playerConfig;

    public static CPlayer getPlayerByHandle(Player player) {
        return getPlayerByUUID(player.getUniqueId());
    }

    public static CPlayer addPlayerFromUUID(UUID key) {
        if (containsPlayer(key)) {
            return getPlayerByUUID(key);
        }
        CPlayer value = new CPlayer(key);
        loadPlayer(value);
        players.put(key, value);
        return value;
    }
    
    public static CPlayer addPlayerFromHandle(Player player) {
        return addPlayerFromUUID(player.getUniqueId());
    }

    public static boolean containsPlayer(UUID key) {
        return players.containsKey(key);
    }

    public static CPlayer getPlayerByUUID(UUID key) {
        if (!containsPlayer(key)) {
            return addPlayerFromUUID(key);
        }
        return players.get(key);
    }

    public static CPlayer removePlayer(UUID key) {
        if (!containsPlayer(key)) {
            return null;
        }
        return players.remove(key);
    }

    public static void removeAllPlayers() {
        players.clear();
    }

    public static Collection<CPlayer> players() {
        return players.values();
    }

    public static void savePlayers() {
        String path = "";
        for (CPlayer player : players()) {
            path = "players." + player.getUUID();
            playerConfig.set(path + ".money", player.getMoney());
            if (player.getCiv() != null) {
                playerConfig.set(path + ".civ", player.getCiv().getCivName());
            }

            if (player.getParty() != null) {
                playerConfig.set(path + ".party", player.getParty().getName());
            }
            playerConfig.set(path + ".permissions", player.getPerms());
            playerConfig.set(path + ".name", player.getName().toLowerCase());
        }

        try {
            playerConfig.save(new File(Cultus.getInstance().getDataFolder() + "/players/players.yml"));
        } catch (IOException ex) {
            Logger.getLogger(PlayerManager.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void loadPlayer(CPlayer player) {
        ConfigurationSection cs = playerConfig.getConfigurationSection("players." + player.getUUID());
        if (cs == null) {
            return;
        }

        double money = cs.getDouble("money", 250);
        String civName = cs.getString("civ", "");
        System.out.println(civName);
        Civilization civ = CivilizationManager.getCivByName(civName);
        System.out.println(civ);
        Party party = PartyManager.getParty(cs.getString("party", ""));
        List<String> perms = cs.getStringList("permissions");
        player.setCiv(civ);
        player.setMoney(money);
        player.setParty(party);
        player.setPermissions(perms);
    }

    public static void loadConfig() {
        File file = new File(Cultus.getInstance().getDataFolder() + "/players/players.yml");
        if (!file.exists()) {
            try {
                Files.createDirectories(Paths.get(Cultus.getInstance().getDataFolder() + "/players/"));
                file.createNewFile();
            } catch (IOException ex) {
                System.out.println("Could not create the data file!");
            }
        }
        playerConfig = YamlConfiguration.loadConfiguration(file);
    }
}

package com.voidtechnologies.cultus.managers;

import com.voidtechnologies.cultus.Cultus;
import com.voidtechnologies.cultus.Utility;
import static com.voidtechnologies.cultus.Utility.locationFromString;
import com.voidtechnologies.cultus.civilizations.Camp;
import com.voidtechnologies.cultus.civilizations.Civilization;
import com.voidtechnologies.cultus.civilizations.CivilizationType;
import com.voidtechnologies.cultus.civilizations.Nation;
import com.voidtechnologies.cultus.civilizations.NationType;
import com.voidtechnologies.cultus.civilizations.Relation;
import com.voidtechnologies.cultus.players.Party;
import com.voidtechnologies.cultus.runnables.ConstructionRunnable;
import com.voidtechnologies.cultus.runnables.TechResearchRunnable;
import com.voidtechnologies.cultus.structures.Position2d;
import com.voidtechnologies.cultus.structures.Structure;
import com.voidtechnologies.cultus.structures.StructureType;
import com.voidtechnologies.cultus.technology.Technologies;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.bukkit.Chunk;
import org.bukkit.Location;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.YamlConfiguration;

/**
 * @author Dylan Holmes
 * @date Jan 10, 2018
 */
public class CivilizationManager {

    private static YamlConfiguration civConfig;
    private static YamlConfiguration structConfig;
    private static YamlConfiguration techConfig;
    private static YamlConfiguration campConfig;

    private static final HashMap<UUID, List<String>> INVITES = new HashMap<>();
    private static final HashMap<String, Civilization> CIVILIZATIONS = new HashMap<>();

    public static void saveConfigs() {
        try {
            File civ = new File(Cultus.getInstance().getDataFolder() + "/civilizations/civilizations.yml");
            File struct = new File(Cultus.getInstance().getDataFolder() + "/civilizations/structures.yml");
            File tech = new File(Cultus.getInstance().getDataFolder() + "/civilizations/technologies.yml");
            File camp = new File(Cultus.getInstance().getDataFolder() + "/civilizations/camps.yml");

            civConfig.save(civ);
            structConfig.save(struct);
            techConfig.save(tech);
            campConfig.save(camp);
        } catch (IOException ex) {
            Logger.getLogger(CivilizationManager.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public static void loadConfigs() {
        File civ = new File(Cultus.getInstance().getDataFolder() + "/civilizations/civilizations.yml");
        File struct = new File(Cultus.getInstance().getDataFolder() + "/civilizations/structures.yml");
        File tech = new File(Cultus.getInstance().getDataFolder() + "/civilizations/technologies.yml");
        File camp = new File(Cultus.getInstance().getDataFolder() + "/civilizations/camps.yml");

        if (!civ.exists()) {
            try {
                civ.createNewFile();
            } catch (IOException ex) {
                System.out.println("Could not create the data file!");
            }
        }

        if (!tech.exists()) {
            try {
                tech.createNewFile();
            } catch (IOException ex) {
                System.out.println("Could not create the data file!");
            }
        }
        if (!struct.exists()) {
            try {
                struct.createNewFile();
            } catch (IOException ex) {
                System.out.println("Could not create the data file!");
            }
        }

        if (!camp.exists()) {
            try {
                camp.createNewFile();
            } catch (IOException ex) {
                System.out.println("Could not create the data file!");
            }
        }

        campConfig = YamlConfiguration.loadConfiguration(camp);
        civConfig = YamlConfiguration.loadConfiguration(civ);
        techConfig = YamlConfiguration.loadConfiguration(tech);
        structConfig = YamlConfiguration.loadConfiguration(struct);
    }

    public static List<Nation> nations() {
        List<Nation> nations = new ArrayList<>();
        for (Civilization civ : CIVILIZATIONS.values()) {
            if (civ instanceof Nation) {
                nations.add((Nation) civ);
            }
        }
        return nations;
    }

    public static List<Camp> camps() {
        List<Camp> camps = new ArrayList<>();
        for (Civilization civ : CIVILIZATIONS.values()) {
            if (civ instanceof Camp) {
                camps.add((Camp) civ);
            }
        }
        return camps;
    }

    public static Collection<Civilization> civilizations() {
        return CIVILIZATIONS.values();
    }

    public static Civilization getCivByName(String civ) {
        return CIVILIZATIONS.get(civ.toLowerCase());
    }

    public static void loadCivilizations() {
        loadConfigs();
        int i = 0;

        ConfigurationSection cs = civConfig.getConfigurationSection("civs");
        if (cs != null) {

            System.out.println("[CULTUS] Loading Nations...");
            for (String civ : cs.getKeys(false)) {
                HashMap<String, Object> values = loadNationGeneral(civ);
                List<Structure> structures = loadNationStructures(civ);
                String name = (String) values.get("name");
                String tag = (String) values.get("tag");
                LinkedList<Position2d> territory = (LinkedList<Position2d>) values.get("territories");
                Chunk center = Position2d.fromString((String) values.get("center")).toChunk();

                short influence = (short) values.get("influence");
                double money = (double) values.get("money");
                int happiness = (int) values.get("happiness");
                NationType nType = (NationType) values.get("type");
                List<Technologies> technologies = loadNationTech(civ);
                String membersString = (String) values.get("members");
                String[] membersID = membersString.split(",");
                HashMap<String, Relation> relations = (HashMap<String, Relation>) values.get("relations");
                List<UUID> members = new ArrayList<>();

                for (String member : membersID) {
                    members.add(UUID.fromString(member));
                }

                Nation nation = new Nation(name, tag, center, members, structures, technologies, territory, influence, money, happiness, nType, relations);

                List<ConstructionRunnable> builds = loadNationBuilds(nation);
                TechResearchRunnable trr = loadCurrentNationTech(nation);

                nation.loadCurrentBuilds(builds);
                nation.loadCurrentResearch(trr);

                addCivilization(nation);
                i++;
            }
        }
        System.out.println("Loaded " + i + " nations!");
        cs = campConfig.getConfigurationSection("camps");
        if (cs == null) {
            return;
        }

        i = 0;
        for (String civ : cs.getKeys(false)) {
            HashMap<String, Object> values = loadCampGeneral(civ);
            List<Structure> structures = loadCampStructures(civ);
            String name = (String) values.get("name");
            String tag = (String) values.get("tag");
            LinkedList<Position2d> territory = (LinkedList<Position2d>) values.get("territories");
            CivilizationType type = (CivilizationType) values.get("type");
            Chunk center = Position2d.fromString((String) values.get("center")).toChunk();
            Party party = PartyManager.getParty((String) values.get("party"));
            HashMap<String,  Relation> relations = (HashMap<String,  Relation>) values.get("relations");
            Camp camp = new Camp(name, tag, center, party, structures, territory, relations);
            addCivilization(camp);
            i++;
        }
        System.out.println("Loaded " + i + " camps!");
    }

    private static TechResearchRunnable loadCurrentNationTech(Nation civ) {
        String path = "civs." + civ.getCivName().toLowerCase() + ".technologies.current.";
        String tech = techConfig.getString(path + "technology", "");
        double prog = techConfig.getDouble(path + "progress", 0);
        double cost = techConfig.getDouble(path + "cost", 0);
        if (tech.equalsIgnoreCase("")) {
            return null;
        }
        return new TechResearchRunnable(civ, Technologies.fromName(tech), prog, cost);
    }

    private static List<ConstructionRunnable> loadNationBuilds(Nation civ) {
        List<ConstructionRunnable> ret = new ArrayList<>();
        String path = "civs." + civ.getCivName().toLowerCase() + ".building";
        ConfigurationSection sect = structConfig.getConfigurationSection(path);
        if (sect == null) {
            return ret;
        }

        for (String key : sect.getKeys(false)) {
            StructureType type = StructureType.fromName(structConfig.getString(path + "." + key + ".type"));
            double progress = structConfig.getDouble(path + key + ".progress");
            //double cost = structConfig.getDouble(path + key + ".cost");
            Location loc = locationFromString(key);
            ret.add(new ConstructionRunnable(civ, type, loc, progress));
        }
        return ret;
    }

    private static List<Structure> loadNationStructures(String civ) {
        String path = "civs." + civ + ".";
        LinkedList<Structure> structures = new LinkedList<>();

        String sStructs = structConfig.getString(path + "structures");
        String[] splitStructs = sStructs.split(",");
        for (String struct : splitStructs) {
            if (struct.equals("")) {
                continue;
            }
            String[] elements = struct.split(";");
            structures.add(Structure.createStructure(Utility.locationFromString(struct), StructureType.fromName(elements[4]).getStructureClass()));
        }
        return structures;
    }

    private static List<Technologies> loadNationTech(String civ) {
        String path = "civs." + civ + ".technologies.";

        List<String> techList = techConfig.getStringList(path + "researched");
        return Technologies.getTechnologiesFromList(techList);
    }

    private static HashMap<String, Object> loadNationGeneral(String civ) {
        String path = "civs." + civ + ".";
        HashMap<String, Object> values = new HashMap<>();
        values.put("name", civConfig.getString(path + "name"));
        values.put("tag", civConfig.getString(path + "tag"));
        values.put("type", CivilizationType.valueOf(civConfig.getString(path + "type")));
        values.put("center", civConfig.getString(path + "center"));
        values.put("influence", (short) civConfig.getInt(path + "influence"));
        values.put("type", NationType.fromName(civConfig.getString(path + "nation.type")));
        values.put("members", civConfig.getString(path + "members"));
        values.put("money", civConfig.getDouble(path + "money"));

        String terrs = civConfig.getString(path + "territories");
        String[] splitTerr = terrs.split(",");
        LinkedList<Position2d> territories = new LinkedList<>();
        for (String terr : splitTerr) {
            String[] elements = terr.split(";");
            territories.add(new Position2d(elements[0], Integer.valueOf(elements[1]), Integer.valueOf(elements[2])));
        }
        values.put("territories", territories);

        ConfigurationSection cs = civConfig.getConfigurationSection(path + "relations");
        if (cs == null) {
            values.put("relations", null);
            return values;
        }
        HashMap<String, Relation> relations = new HashMap<>();
        for (String key : cs.getKeys(false)) {
            relations.put(key, Relation.valueOf(cs.getString(key)));
        }
        values.put("relations", relations);

        return values;
    }

    private static List<Structure> loadCampStructures(String camp) {
        String path = "camps." + camp + ".";
        LinkedList<Structure> structures = new LinkedList<>();

        String sStructs = structConfig.getString(path + "structures");
        String[] splitStructs = sStructs.split(",");
        for (String struct : splitStructs) {
            if (struct.equals("")) {
                continue;
            }
            String[] elements = struct.split(";");
            structures.add(Structure.createStructure(Utility.locationFromString(struct), StructureType.fromName(elements[4]).getStructureClass()));
        }
        return structures;
    }

    private static HashMap<String, Object> loadCampGeneral(String camp) {

        String path = "camps." + camp + ".";
        HashMap<String, Object> values = new HashMap<>();
        values.put("name", campConfig.getString(path + "name"));
        values.put("tag", campConfig.getString(path + "tag"));
        values.put("type", CivilizationType.valueOf(campConfig.getString(path + "type")));
        values.put("party", campConfig.getString(path + "party"));
        values.put("center", campConfig.getString(path + "center"));

        String terrs = campConfig.getString(path + "territories");
        String[] splitTerr = terrs.split(",");
        LinkedList<Position2d> territories = new LinkedList<>();
        for (String terr : splitTerr) {
            String[] elements = terr.split(";");
            territories.add(new Position2d(elements[0], Integer.valueOf(elements[1]), Integer.valueOf(elements[2])));
        }
        values.put("territories", territories);

        ConfigurationSection cs = campConfig.getConfigurationSection(path + "relations");
        if (cs == null) {
            values.put("relations", null);
            return values;
        }
        HashMap<String, Relation> relations = new HashMap<>();
        for (String key : cs.getKeys(false)) {
            relations.put(key, Relation.valueOf(cs.getString(key)));
        }
        values.put("relations", relations);

        return values;
    }

    public static void saveCivilizations() {
        System.out.println("Saving civilizations...");
        saveCamps();
        saveConfigs();
        for (Nation civ : nations()) {
            saveNation((Nation) civ);
        }
        saveConfigs();
        System.out.println("Saved " + CIVILIZATIONS.size() + " civilizations!");
    }

    private static void saveNationStructures(Nation nation) {
        String path = "civs." + nation.getCivName().toLowerCase() + ".";

        StringBuilder sb = new StringBuilder();
        for (Structure struct : nation.getStructures()) {
            String loc = Utility.locationToString(struct.getLocation());
            sb.append(loc).append(";").append(struct.getType().getName()).append(",");
        }
        if (nation.getStructures().size() > 0) {
            sb.replace(sb.length() - 1, sb.length(), "");
        }
        set(structConfig, path + "structures", sb.toString());

        path = "civs." + nation.getCivName().toLowerCase() + ".building.";
        Set<ConstructionRunnable> builds = nation.getInProgressBuilds();
        for (ConstructionRunnable sbr : builds) {
            path += Utility.locationToString(sbr.getLocation()) + ".";
            set(structConfig, path + "type", sbr.getConstructure().getStructureType().getName().toLowerCase());
            set(structConfig, path + "progress", sbr.getProgress());
            set(structConfig, path + "cost", sbr.getConstructure().getCurrentHammers());
        }
    }

    private static void saveNationGeneral(Nation nation) {
        String path = "civs." + nation.getCivName().toLowerCase() + ".";

        Set<UUID> members = nation.members();
        String membersString = members.toString().replace("[", "").replace("]", "").trim();
        set(civConfig, path + "name", nation.getCivName());
        set(civConfig, path + "tag", nation.getTag());
        set(civConfig, path + "type", nation.getCivType().toString());
        set(civConfig, path + "center", nation.getCenter().toString());
        set(civConfig, path + "members", membersString);

        set(civConfig, path + "hammers", nation.getHammers());
        set(civConfig, path + "beakers", nation.getBeakers());
        set(civConfig, path + "influence", nation.getInfluence());
        set(civConfig, path + "nation.type", nation.getNationType().getName());
        set(civConfig, path + "money", nation.getMoney());
        set(civConfig, path + "happiness", nation.getHappiness());

        StringBuilder sb = new StringBuilder();
        for (Position2d p2d : nation.getTerritories()) {
            sb.append(p2d.toString()).append(",");
        }
        if (nation.getTerritories().size() > 0) {
            sb.replace(sb.length() - 1, sb.length(), "");
        }
        set(civConfig, path + "territories", sb.toString());
    }

    private static void set(YamlConfiguration config, String path, Object value) {
        config.set(path, value);
    }

    private static void saveNation(Nation nation) {
        saveNationGeneral(nation);
        saveNationStructures(nation);
        saveNationTechnologies(nation);
    }

    private static void saveNationTechnologies(Nation nation) {
        String path = "civs." + nation.getCivName().toLowerCase() + ".technologies.";
        set(techConfig, path + "researched", Technologies.getParsableList(nation.getTechnologies()));

        TechResearchRunnable cr = nation.getCurrentResearch();
        if (cr == null) {
            set(techConfig, path + "current.technology", "");
            return;
        }
        set(techConfig, path + "current.technology", cr.getTech().toString());
        set(techConfig, path + "current.progress", cr.getProgress());
        set(techConfig, path + "current.cost", cr.getCost());
    }

    public static void addCivilization(Civilization civ) {
        CIVILIZATIONS.put(civ.getCivName().toLowerCase(), civ);
    }

    public static Civilization getLocationOwner(Location loc) {
        Position2d p2d = Position2d.convertChunkToPosition(loc.getChunk());
        for (Civilization civ : CIVILIZATIONS.values()) {
            if (civ.containsTerritory(p2d)) {
                return civ;
            }
        }
        return null;
    }

    public static Civilization getLocationOwner(Position2d p2d) {
        for (Civilization civ : CIVILIZATIONS.values()) {
            if (civ.containsTerritory(p2d)) {
                return civ;
            }
        }
        return null;
    }
    
    public static void removeCivilization(Civilization civ) {
        CIVILIZATIONS.remove(civ.getCivName().toLowerCase());
    }

    public static void saveCamps() {
        for (Camp camp : camps()) {
            saveCamp(camp);
        }
    }

    private static void removeCampFromFile(Civilization civ) {
        campConfig.set("camps." + civ.getCivName().toLowerCase(), null);
    }

    private static void saveCamp(Camp civ) {
        String path = "camps." + civ.getCivName().toLowerCase() + ".";
        set(campConfig, path + "name", civ.getCivName());
        set(campConfig, path + "tag", civ.getTag());
        set(campConfig, path + "type", civ.getCivType().toString());
        set(campConfig, path + "party", civ.getParty().getName().toLowerCase());
        set(campConfig, path + "center", civ.getCenter().toString());

        StringBuilder sb = new StringBuilder();
        for (Position2d p2d : civ.getTerritories()) {
            sb.append(p2d.toString()).append(",");
        }
        if (civ.getTerritories().size() > 0) {
            sb.replace(sb.length() - 1, sb.length(), "");
        }
        set(campConfig, path + "territories", sb.toString());

        path = "camps." + civ.getCivName().toLowerCase() + ".";

        sb = new StringBuilder();
        for (Structure struct : civ.getStructures()) {
            String loc = Utility.locationToString(struct.getLocation());
            sb.append(loc).append(";").append(struct.getType().getName()).append(",");
        }
        if (civ.getStructures().size() > 0) {
            sb.replace(sb.length() - 1, sb.length(), "");
        }
        set(structConfig, path + "structures", sb.toString());
    }

    public static Nation getNationByTag(String item) {
        for (Nation nat : nations()) {
            if (!nat.getTag().equalsIgnoreCase(item)) {
                continue;
            }
            return nat;
        }
        return null;
    }

    public static void addInvite(UUID id, String civ) {
        List<String> pInvites = INVITES.get(id);
        if (pInvites == null) {
            pInvites = new ArrayList<>();
        }
        pInvites.add(civ.toLowerCase());
        INVITES.put(id, pInvites);
    }

    public static Civilization getInvite(UUID id, String civ) {
        List<String> pInvites = INVITES.get(id);
        if (pInvites == null) {
            pInvites = new ArrayList<>();
            INVITES.put(id, pInvites);
            return null;
        }
        if (!pInvites.contains(civ.toLowerCase())) {
            return null;
        }
        return CivilizationManager.getCivByName(civ);
    }

    public static void removeInvite(UUID id) {
        INVITES.remove(id);
    }
}
package com.voidtechnologies.cultus.managers;

import com.voidtechnologies.cultus.generator.LuxuryResource;
import com.voidtechnologies.cultus.structures.Position2d;
import java.util.ArrayList;
import org.bukkit.Material;
import org.bukkit.block.Biome;

/**
 *
 * @author Dylan Aug 28, 2021
 */
public class LuxuryManager {

    public static ArrayList<LuxuryResource> resources = new ArrayList<>();

    public static class LuxuryPairing {

        private Material mat;
        private float chance;

        public LuxuryPairing(Material mat, float chance) {
            this.mat = mat;
            this.chance = chance;
        }

        public Material getMaterial() {
            return mat;
        }

        public float getChance() {
            return chance;
        }

    }

    private static ArrayList<LuxuryPairing> SHORE = new ArrayList<LuxuryPairing>() {
        {
            add(new LuxuryPairing(Material.SUGAR_CANE, 50));
            add(new LuxuryPairing(Material.PUFFERFISH, 50));
        }
    };

    private static ArrayList<LuxuryPairing> HILLS = new ArrayList<LuxuryPairing>() {
        {
            add(new LuxuryPairing(Material.GRANITE, 33));
            add(new LuxuryPairing(Material.BASALT, 33));
            add(new LuxuryPairing(Material.COPPER_INGOT, 33));
        }
    };

    private static ArrayList<LuxuryPairing> JUNGLE = new ArrayList<LuxuryPairing>() {
        {
            add(new LuxuryPairing(Material.COCOA, 50));
            add(new LuxuryPairing(Material.BROWN_MUSHROOM, 50));
        }
    };

    private static ArrayList<LuxuryPairing> FROZEN = new ArrayList<LuxuryPairing>() {
        {
            add(new LuxuryPairing(Material.SALMON, 100));
        }
    };
    private static ArrayList<LuxuryPairing> OTHER = new ArrayList<LuxuryPairing>() {
        {
            add(new LuxuryPairing(Material.GOLD_INGOT, 15));
            add(new LuxuryPairing(Material.DIAMOND, 5));
            add(new LuxuryPairing(Material.HONEYCOMB, 15));
            add(new LuxuryPairing(Material.RED_DYE, 25));
            add(new LuxuryPairing(Material.LAPIS_LAZULI, 10));
            add(new LuxuryPairing(Material.APPLE, 30));

        }
    };

    private static ArrayList<String> barren = new ArrayList<String>() {
        {
            add("DESERT");
            add("OCEAN");
        }
    };

    public static Material getRandomLuxury(Biome biome) {
        String bioStr = biome.toString();

        for (String bar : barren) {
            if (bioStr.contains(bar)) {
                return Material.AIR;
            }
        }

        if (bioStr.contains("BEACH")) {
            return randomEntry(SHORE);
        } else if (bioStr.contains(("JUNGLE"))) {
            return randomEntry(JUNGLE);
        } else if (bioStr.contains("FROZEN")) {
            return randomEntry(FROZEN);
        } else if (bioStr.contains("HILLS")) {
            return randomEntry(HILLS);
        } else {
            return randomEntry(OTHER);
        }
    }

    public static Material randomEntry(ArrayList<LuxuryPairing> list) {
        int r = (int) (100 * Math.random());
        int sum = 0;
        int event = 0;
        for (int i = 0; i < list.size() && sum <= r; i++) {
            sum += list.get(i).getChance();
            event = i;
        }

        return list.get(event).getMaterial();
    }

    public static void addResource(Material mat, Position2d loc) {
        resources.add(new LuxuryResource(mat, loc));
    }

    public static boolean containsResource(Position2d pos) {
        return resources.stream().anyMatch(lr -> (lr.getLocation().equals(pos)));
    }

    public static LuxuryResource getResource(Position2d pos) {
        for (LuxuryResource lr : resources) {
            if (lr.getLocation().equals(pos)) {
                return lr;
            }
        }
        return null;
    }
}

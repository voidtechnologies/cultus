package com.voidtechnologies.cultus;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

/**
 * @author Dylan Holmes
 * @date Feb 12, 2018
 */
public class Utility {
    
    public static final String SIGN_ID = "§i§0";
    
    /**
     * Converts a Spigot Location object to a string representation
     * @param loc to convert
     * @return converted string
     */
    public static String locationToString(Location loc) {
        return (loc.getWorld().getName() + ";" + loc.getBlockX() + ";" + loc.getBlockY() + ";" + loc.getBlockZ());
    }

    /**
     * Converts a string representation of a Spigot Location object into a Location object
     * @param string to convert
     * @return converted Location
     */
    public static Location locationFromString(String string) {
        String[] split = string.split(";");
        return new Location(Bukkit.getWorld(split[0]), Integer.valueOf(split[1]), Integer.valueOf(split[2]), Integer.valueOf(split[3]));
    }
    
    /**
     * Removes @mat from @inv @max amount of times
     * @param inv inventory to remove the Material from
     * @param mat Material to be removed from the inventory
     * @param max the most of amount of material that can be removed
     * @return the amount of material that was removed, <= @max
     */
    public static int removeMaterial(Inventory inv, Material mat, int max) {
        // Tracker for how much we have currently removed
        int removed = 0;
        // Loop through the contents of the inventory stack by stack
        for (ItemStack is : inv.getContents()) {
            // ignore null items
            if (is == null) {
                continue;
            }
            // ignore items that aren't the same type we are looking for
            if (!is.getType().equals(mat)) {
                continue;
            }
            
            // if we find a single itemstack that is >= the amt we are looking for, just remove @max amt and be done
            if (is.getAmount() >= max) {
                is.setAmount(is.getAmount() - max);
                return max;
            }
            // Otherwise it is < @max, so remove it entirely and increment the counter
            // Increment our counter
            removed += is.getAmount();
            // Delete ItemStack
            is.setAmount(0);
            
            // if we reached the desired amount quit loop
            if (removed == max) {
                return max;
            }
            // Loop again
        }
        return removed;
    }
}
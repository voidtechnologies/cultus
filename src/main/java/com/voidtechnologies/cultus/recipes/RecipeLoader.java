package com.voidtechnologies.cultus.recipes;

import com.voidtechnologies.cultus.Cultus;
import com.voidtechnologies.cutlusmaterials.items.ShapedRecipe;
import com.voidtechnologies.cutlusmaterials.managers.ItemManager;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import org.bukkit.Material;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.inventory.ItemStack;

/**
 * @author Dylan Holmes
 * @date Feb 7, 2018
 */
public class RecipeLoader {

    private static YamlConfiguration recipeConfig;
    private static HashMap<String, ItemStack> items = new HashMap<>();

    public static void loadRecipes() {
        loadConfig();
        ConfigurationSection itemSect = recipeConfig.getConfigurationSection("items");
        if (itemSect == null) {
            return;
        }
        System.out.println("Loading Items...");
        for (String key : itemSect.getKeys(false)) {
            String path = "items." + key + ".";
            String name = recipeConfig.getString(path + "name");
            int amount = recipeConfig.getInt(path + "amount");
            Material type = Material.valueOf(recipeConfig.getString(path + "type"));
            boolean soulBound = recipeConfig.getBoolean(path + "soulbound");
            int cTier = recipeConfig.getInt(path + "ctier");

            ItemStack item = new ItemStack(type, amount);
            ItemManager.setCultusItem(item, null, name, soulBound, cTier, true);
            items.put(name, item);
            System.out.println("Loaded Item: " + name);
        }

        System.out.println("Loading Recipes...");
        ConfigurationSection recipeSect = recipeConfig.getConfigurationSection("recipes");
        if (recipeSect == null) {
            return;
        }

        for (String key : recipeSect.getKeys(false)) {
            String path = "recipes." + key + ".";
            ItemStack result = items.get(recipeConfig.getString(path + "result"));
            String[] shape = new String[3];
            int i = 0;
            for (Object obj : recipeConfig.getStringList(path + "shape")) {
                shape[i++] = obj.toString();
            }

            ShapedRecipe recipe = new ShapedRecipe(shape, result);

            List<String> ingredients = recipeConfig.getStringList(path + "ingredients");

            for (String ing : ingredients) {
                String[] elements = ing.split(";");
                char ch = elements[0].charAt(0);
                String type = elements[1];

                if (type.equals("TYPE")) {
                    recipe.setIngredient(ch, Material.valueOf(elements[2]));
                } else {
                    ItemStack inS = items.get(elements[2]);
                    recipe.setIngredient(ch, inS);
                }
            }
            System.out.println("Loaded Recipe: " + key);
            recipe.register();
        }
    }

    public static void loadConfig() {
        File file = new File(Cultus.getInstance().getDataFolder() + "/recipes.yml");
        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException ex) {
                System.out.println("Could not create the data file!");
            }
        }
        recipeConfig = YamlConfiguration.loadConfiguration(file);
    }
}

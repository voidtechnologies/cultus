package com.voidtechnologies.cultus.players;

/**
 *
 * @author Dylan
 */
public enum Rank {
    MEMBER(0),
    MOD(1),
    OWNER(2);
    
    
    private final int value;
    
    private Rank(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }
}
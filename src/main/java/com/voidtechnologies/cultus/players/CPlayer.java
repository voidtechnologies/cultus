package com.voidtechnologies.cultus.players;

import com.voidtechnologies.cultus.civilizations.Civilization;
import com.voidtechnologies.cultus.commands.ui.Menu;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;

/**
 * @author Dylan Holmes
 * @date Jan 3, 2018
 */
public class CPlayer {

    private Player handle;
    private Civilization civ;
    private final UUID UUID;
    private double money;
    private Party party;
    private List<String> permissions = new ArrayList<>();

    public CPlayer(UUID uuid) {
        Player player = Bukkit.getPlayer(uuid);
        if (player != null) {
            handle = player;
        }
        this.UUID = uuid;
    }

    public CPlayer(Player player) {
        this.handle = player;
        this.civ = null;
        this.UUID = player.getUniqueId();
    }

    public String getName() {
        getHandle();
        if (handle == null) {
            return Bukkit.getOfflinePlayer(UUID).getName();
        }
        return handle.getName();
    }

    public Player getHandle() {
        if (handle == null) {
            Player player = Bukkit.getPlayer(UUID);
            if (player != null) {
                handle = player;
            }
        }
        return handle;
    }

    public List<String> getPerms() {
        return permissions;
    }

    public double getMoney() {
        return money;
    }

    public void setMoney(double money) {
        this.money = money;
    }

    public void addMoney(double money) {
        this.money += money;
    }

    public void deductMoney(double money) {
        addMoney(-money);
    }

    public boolean isOnline() {
        return getHandle() != null;
    }

    public void sendMessage(String message) {
        getHandle().sendMessage(message);
    }

    public UUID getUUID() {
        return UUID;
    }

    public Civilization getCiv() {
        return civ;
    }

    public void setCiv(Civilization civ) {
        this.civ = civ;
    }

    @Override
    public String toString() {
        return UUID.toString();
    }

    public Party getParty() {
        return party;
    }

    public void setParty(Party party) {
        this.party = party;
    }

    public boolean hasPermission(String perm) {
        if (permissions.contains("cultus.nation.*") || permissions.contains("cultus.nation.owner")) {
            return true;
        }
        return permissions.contains(perm);
    }

    public void addPermission(String perm) {
        if (hasPermission(perm)) {
            return;
        }
        permissions.add(perm);
    }

    public void removePermission(String perm) {
        permissions.remove(perm);
    }

    public void setPermissions(List<String> perms) {
        permissions = perms;
    }

    public void removeNationPermissions() {
        permissions.clear();
    }
    
    public void displayMenu(Menu menu) {
        Inventory inv = menu.createInventory();
        getHandle().openInventory(inv);
    }
}
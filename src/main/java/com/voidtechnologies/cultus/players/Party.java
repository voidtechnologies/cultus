package com.voidtechnologies.cultus.players;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;
import java.util.UUID;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

/**
 * @author Dylan Holmes
 * @date Jan 28, 2018
 */
public class Party {

    private HashMap<UUID, Rank> members;
    private String name;
    private double money;

    public Party(String name) {
        this.members = new HashMap<>();
        this.name = name;
    }

    public Party(String name, HashMap<UUID, Rank> members) {
        this.members = members;
        this.name = name;
    }

    public boolean isPlayerInParty(CPlayer key) {
        return members.containsKey(key.getUUID());
    }

    public double getMoney() {
        return money;
    }

    public void setMoney(double money) {
        this.money = money;
    }
    
    public Rank setMember(UUID key, Rank value) {
        return members.put(key, value);
    }
    
    public void addMember(UUID key) {
        members.put(key, Rank.MEMBER);
    }

    public Rank removeMember(UUID key) {
        return members.remove(key);
    }

    public Set<UUID> getMembers() {
        return members.keySet();
    }
    
    public List<Player> getOnlineMembers() {
        ArrayList<Player> players = new ArrayList<>();
        for (UUID id : getMembers()) {
            Player player = Bukkit.getPlayer(id);
            if (player == null || !player.isOnline()) {
                continue;
            }
            players.add(player);
        }
        return players;
    }

    public String getName() {
        return name;
    }

    public Rank getRankOfMember(UUID player) {
        return members.get(player);
    }
    
    public void sendMessageToPartyMembers(String msg) {
        for (UUID id : getMembers()) {
            Player player = Bukkit.getPlayer(id);
            if (!player.isOnline()) {
                continue;
            }
            player.sendMessage(ChatColor.BLUE + "[" + name + "]" + ChatColor.RESET + msg);
        }
    }
    
    public String convertMemberMapToString() {
        StringBuilder sb = new StringBuilder();
        for (Entry<UUID, Rank> entry : members.entrySet()) {
            sb.append(entry.getKey().toString()).append(":").append(entry.getValue().toString()).append(";");
        }
        return sb.toString();
    }
    
    public void addMembersStringToMap(String map) {
        String[] mappedElements = map.split(";");
        for (String element : mappedElements) {
            String[] keyValue = element.split(":");
            String key = keyValue[0];
            String value = keyValue[1];
            
            members.put(UUID.fromString(key), Rank.valueOf(value.toUpperCase()));
        }
    }
    
    public void addMoney(double money) {
        this.money += money;
    }
    
    public void subMoney(double money) {
        addMoney(-money);
    }
}
package com.voidtechnologies.cultus.technology;

import com.voidtechnologies.cultus.structures.StructureType;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author dylan
 */
public enum Technologies {

    POTTERY("Pottery", "Enables the building of \ngardens to gather crops faster.", TechnologyEra.ANCIENT, new Technologies[]{}, 300),
    ANIMAL_HUSBANDRY("Animal Husbandry", "Does nothing other than unlock The Wheel.", TechnologyEra.ANCIENT, new Technologies[]{}, 250),
    ARCHERY("Archery", "Enables the building of \n Arrow Towers and such.", TechnologyEra.ANCIENT, new Technologies[]{}, 450),
    MINING("Mining", "Dink dink, oooo", TechnologyEra.ANCIENT, new Technologies[]{}, 500),
    WRITING("Writing", "Partially enables the research of writing.", TechnologyEra.ANCIENT, new Technologies[]{POTTERY}, 650),
    THE_WHEEL("The Wheel", "Enables the building of\n Granaries.", TechnologyEra.ANCIENT, new Technologies[]{ANIMAL_HUSBANDRY}, 1000),
    MATHEMATICS("Mathematics", "Enables the building\n of the lab.", TechnologyEra.CLASSICAL, new Technologies[]{THE_WHEEL, WRITING}, 1300),
    CONSTRUCTION("Construction", "Enables the building\n of the forge.", TechnologyEra.CLASSICAL, new Technologies[]{MINING, THE_WHEEL}, 950),
    CURRENCY("Currency", "Enables the building\nof the bank!", TechnologyEra.CLASSICAL, new Technologies[]{MATHEMATICS}, 750),
    HORSEBACK_RIDING("Horseback Riding", "Neigh(?)", TechnologyEra.CLASSICAL, new Technologies[]{THE_WHEEL}, 750),
    UNKNOWN("Unknown", "N/A", TechnologyEra.NO_REQ, new Technologies[]{}, 333);

    public static final long RESEARCH_DELAY = 200;

    private Technologies[] requirements;

    private String name, desc;
    private StructureType[] structureUnlocks;
    private TechnologyEra era;
    private double cost;

    private Technologies(String name, String desc, TechnologyEra era, Technologies[] requirements, double cost) {
        this.requirements = requirements;
        this.name = name;
        this.era = era;
        this.cost = cost;
        this.desc = desc;
    }

    public Technologies[] getRequirements() {
        return requirements;
    }

    public String getName() {
        return name;
    }

    public StructureType[] getStructureUnlocks() {
        return structureUnlocks;
    }

    public TechnologyEra getEra() {
        return era;
    }

    public double getCost() {
        return cost;
    }

    public String getDesc() {
        return desc;
    }

    public Technologies[] getTechUnlocks() {
        LinkedList<Technologies> techList = new LinkedList<>();
        for (Technologies tech : Technologies.values()) {
            for (Technologies innerTech : tech.getRequirements()) {
                if (innerTech.equals(this)) {
                    techList.add(tech);
                    break;
                }
            }
        }
        return (Technologies[]) techList.toArray(new Technologies[techList.size()]);
    }

    @Override
    public String toString() {
        return getName();
    }

    public static List<String> getParsableList(List<Technologies> list) {
        List<String> retList = new ArrayList<>();

        for (Technologies tech : list) {
            retList.add(tech.toString());
        }

        return retList;
    }

    public static Technologies fromName(String name) {
        for (Technologies tech : values()) {
            if (tech.getName().equalsIgnoreCase(name)) {
                return tech;
            }
        }
        return null;
    }

    public static List<Technologies> getTechnologiesFromList(List<String> list) {
        List<Technologies> retList = new ArrayList<>();
        for (String tech : list) {
            retList.add(fromName(tech));
        }
        return retList;
    }
}

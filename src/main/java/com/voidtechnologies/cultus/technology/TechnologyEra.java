package com.voidtechnologies.cultus.technology;

/**
 * @author Dylan Holmes 
 * @date Jan 8, 2018
 */
public enum TechnologyEra {
    NO_REQ,
    ANCIENT,
    CLASSICAL,
    MEDIEVAL,
    RENAISSANCE,
    INDUSTRIAL,
    MODERN,
    ATOMIC,
    INFORMATION;
}